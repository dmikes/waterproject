=== Project ===
When rendering large bodies of water in real-time an efficient method is required to model
water waves. This thesis describes a method for real-time interactive generation of such
waves. We use the wave particle method to describe wave propagation in a fluid medium.
The method allows to simulate interactions of water with general shaped rigid bodies in
real-time. We present a GPU implementation of the method and show results in scenarios
such as open ocean waters or pools with water boundaries. Finally we compare the tested
result to the wave propagation in a real life.

Thesis text:
https://dl.dropboxusercontent.com/u/16760927/thesis_daniel_mikes.pdf

Video:
https://www.youtube.com/watch?v=M_FsjL9j0HY

=== Application ===
Run the program from the command line. Input arguments:
argv[0] = scene number (0-6)
argv[1] = count parameter. This parameter changes its meaning with respect to the tested scene.
argv[2] = filename of additional simulation configuration. The path to the file is relative to the project and is set to ../resources/config/water_simulation
argv[3] = boolean parameters describing whether it is testing case. 0 for testing, 1 for the run without performance testing.
If scene = 1 or 2 the parameter denotes the number of ships (1-32)
If scene = 6 the parameter denotes the max number of particles added to the system (randomly distributed to the surface). (The surface deviation can been only in a short neighbourhood around the ship - settings in ../resources/config/cameras/OrthoTopView.ini)

The path are stated relative to the exe file.
The results of performance testing (in ms) are stored in ../measurements. In order to see results, create according folder

=== Scenes ===
0 = ocean with single ship
1 = ocean with more ships. (SCE_OCEAN_SHIPS)
2 = lighthouse scene with other ships (SCE_LIGHTHOUSE)
3 = pool with the house
4 = pool
5 = pool with obstacle
6 = particle performance test (SCE_PARTICLE_TEST)
7 = mesh complexity performance test (SCE_MESH_TEST)

Example:
wave.exe 2 6 "setup_01.ini" 1
or
wave.exe 0

In ../resources/transformation/ you can change the transformation of used objects.

=== Count param ===
For SCE_LIGHTHOUSE == number of ships
For SCE_OCEAN_SHIPS == number of ships
For SCE_MESH_TEST == model number (different number of faces)
For SCE_PARTICLE_TEST == number of particles

=== Camera ===
Camera can be controlled by mouse. While holding RMB, you can rotate you view by mouse movement.

=== Keys ===
In the case of running the application without testing, the animation will not start on its own.
W,S,A,D ... camera movement (or ship movement if in Riding mode)
Q,E ... movement up and down
R ... toggle riding mode
Z ... change active floating object
I ... toggle show textures
P ... toggle pause wave particle (water animation)
H ... resets the frame counter and starts the boat animation
T ... stops the water forces
