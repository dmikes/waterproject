#version 400

// === Uniform ===
uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;
uniform mat4 u_ModelMatrix;
uniform int u_Step;


uniform float u_ObjSpeed;
uniform vec3 u_ObjDirection;
uniform vec3 u_ObjVelocity;
uniform float u_LiftCoef;
uniform float u_DragCoef;
uniform float u_ObjProjWeight;

// === Varying ===
// --- In ---
layout (location = 0) in vec4 a_Vertex; 
layout (location = 1) in vec4 a_Normal_Area; // xyz: normal, w: triangleArea
layout (location = 2) in vec4 a_Color;  
layout (location = 3) in vec2 a_MultiTexCoord0; 
// --- Out ---

// === Local ===
out block{
	vec3 normal;	// view space
	vec3 normalObj;	// object space
	vec4 position;	// clip space
	vec4 positionView;
	vec4 positionWorld;
	vec4 color; // drag force
	vec4 color1;// lift force
} Out;



void main(){
	Out.normalObj = a_Normal_Area.xyz;
	Out.normal	  = normalize(mat3(u_ModelViewMatrix) * a_Normal_Area.xyz);
	
	if (u_Step == 3){
		vec3 N = Out.normalObj;
		float absU = u_ObjSpeed;//length(u_ObjVelocity);
		vec3 U = u_ObjDirection;//normalize(u_ObjVelocity);
		//vec3 NxU = cross(N, U);

		float A = a_Normal_Area.w;
		float Aeff = mix(dot(N, U), 1, u_ObjProjWeight) * A;
		//float Aeff = A;

		// u_DragCoef ommited (applied on result on CPU), density omited (== 1)
		// u_DragCoef + u_LiftCoef used here if htey will be compunded together.
		vec3 Fdrag = -0.5 * u_DragCoef * Aeff * absU * absU * U;
		vec3 Flift = -0.5 * u_LiftCoef * Aeff * absU * absU * cross(U, cross(N, U));
		Out.color = vec4(0, 0, 0, 1);
		//if (absU > 0){ // otherwise -INF when velocity is 0 (normalization). So the if is necessary (<< handled on CPU)
		Out.color  = vec4(Fdrag, 1);
		Out.color1 = vec4(Flift, 1);
		//}
	}

	Out.positionWorld  = u_ModelMatrix * a_Vertex;
	Out.positionView  = u_ModelViewMatrix * a_Vertex;
	Out.position  = u_ProjectionMatrix * Out.positionView;
	gl_Position   = Out.position;
}
