#version 430

uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;
uniform float u_ParticleHalfSize;

layout (location = 0) in vec4 a_Pos2_Orig2;
layout (location = 1) in vec4 a_Amp_Propag_Disper; 
//layout (location = 2) in vec4 a_Color;
//layout (location = 3) in vec4 a_Info;


out block{
	vec4 v_ObjectPosition;
	vec4 v_Amp_Propag_Disper;
	vec4 v_Pos1;
	vec4 v_Pos2;
	vec4 v_Pos3;
	vec4 v_Pos4;
} Out;

#define a_Position a_Pos2_Orig2.xy
#define a_OriginDistance a_Pos2_Orig2.z
#define a_Size a_Pos2_Orig2.w

#define a_Amplitude a_Amp_Propag_Disper.x
#define a_PropagationAngle a_Amp_Propag_Disper.y
#define a_DispersionAngle a_Amp_Propag_Disper.z
#define a_Speed a_Amp_Propag_Disper.w

const vec3 sig = vec3(-1, 0, 1);

//#define USE_UNIFORM_SIZE

vec2 unpackSize(float compound){
	float brdFrames = floor(compound / 65536);
	float size = (compound - brdFrames * 65536);
	return vec2(brdFrames, size);
}

float packSize(int brdFrames, float size){
	return brdFrames * 65536 + size;
}


void main(){	
	//vec4 vertex = vec4(a_Position, (a_Pos2_Orig2.y-50)*1.5, 1); // debug purposes
	vec4 vertex = vec4(a_Position, 0, 1); // debug purposes
	//Out.v_ObjectPosition = vertex; 	
	
	mat4 MVP = u_ProjectionMatrix * u_ModelViewMatrix;

#ifdef USE_UNIFORM_SIZE
	vec4 offset = vec4(u_ParticleHalfSize, u_ParticleHalfSize, 0, 0);
#else
	vec4 offset = vec4(a_Size, a_Size, 0, 0);
#endif
	//vec2 unpack = unpackSize(a_Size);
	gl_PointSize = unpackSize(a_Size).y;//a_Size;
	/*Out.v_Pos1 = MVP * (vertex + sig.zxyy * offset); 
	Out.v_Pos2 = MVP * (vertex + sig.zzyy * offset); 
	Out.v_Pos3 = MVP * (vertex + sig.xxyy * offset); 
	Out.v_Pos4 = MVP * (vertex + sig.xzyy * offset); */

	// Depends whether to use geometry shader to make the quad or precompute the size of quad at vertex shader 
	//Out.v_WindowPosition = u_ProjectionMatrix * u_ModelViewMatrix * vec4(a_Vertex, 1); 
	Out.v_Amp_Propag_Disper = a_Amp_Propag_Disper;


	gl_Position = MVP * vertex;
}
