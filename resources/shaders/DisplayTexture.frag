#version 330 compatibility

uniform sampler1D u_InputTexture1D;
uniform sampler2D u_InputTexture2D;
uniform sampler3D u_InputTexture3D;

uniform int u_DisplayMode;
uniform int u_Dimensions;
uniform int u_LOD;
uniform int u_Depth;



in block{
	vec3 texCoord;
} In;

layout (location = 0) out vec4 v_FragColor;

#define TEXTURE_1D 1
#define TEXTURE_2D 2
#define TEXTURE_3D 3

#define KEEP_AS_IS 0
#define NEGATIVE 1
#define HIGLIGHT_COLORS 2
#define HIGLIGHT_COLORS_NEGATIVE 3

// special - start from 10
#define TEX_OBJECT_SILHOUETTE 10
#define FINAL_WAVE_PARTICLE 11

void main(){
	vec4 color;
	if (u_Dimensions == TEXTURE_1D){
		color = texture1D(u_InputTexture1D, In.texCoord.x, u_LOD);
	}
	if (u_Dimensions == TEXTURE_2D){
		color = texture2D(u_InputTexture2D, In.texCoord.xy, u_LOD);
	}
	if (u_Dimensions == TEXTURE_3D){
		color = texture3D(u_InputTexture3D, vec3(In.texCoord.xy, 0.7f), u_LOD); // TODO: u_Depth
	}
	v_FragColor = color;

	// Display Mode
	if (u_DisplayMode == KEEP_AS_IS){
		v_FragColor = color;
	}
	if (u_DisplayMode == NEGATIVE){
		v_FragColor = color + 0.5;
	}
	if (u_DisplayMode == HIGLIGHT_COLORS){
		v_FragColor = color * 100;
	}
	if (u_DisplayMode == HIGLIGHT_COLORS_NEGATIVE){
		v_FragColor = color * 100 + 0.5;
	}

	// Special
	if(u_DisplayMode == TEX_OBJECT_SILHOUETTE){
		v_FragColor = vec4(0);
		//v_FragColor.r = int(color.r > 0);
		//v_FragColor.g = int(color.g > 0.9);

		v_FragColor.g = int(color.r > 0 && color.g <= 0);
		//v_FragColor = color;
	}
	if(u_DisplayMode == FINAL_WAVE_PARTICLE){
		v_FragColor = color + 0.5;
		if(color.a > 0){
			v_FragColor = vec4(0,1,0,0);
		}

	}

}

