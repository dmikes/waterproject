#version 400

layout(points) in;
layout(points, max_vertices = 3) out; // create MAX 2 new vertices
 
uniform uint u_VertexCount;
uniform uint u_MaxParticleBound;
uniform float u_SimSpeed;

in block{
	vec4 f_Pos2_Orig_Size;
	vec4 f_Ampl_Prop_Disp_Spee;
	bvec4 v_Info;
} In[];

out feedback{
	vec4 f_Pos2_Orig_Size;
	vec4 f_Ampl_Prop_Disp_Spee;
} feedOut;

const int i = 0; // No need for loop! (points. In.length() == 1)

#define GREEN_CREATE_VERTEX 1.0f
#define GREEN_VERTEX_CREATED 0.5f
#define BLUE_CHECK_CREATION 1.0f
#define BLUE_DONT_CHECK_CREATION 0.0f

#define f_Position f_Pos2_Orig_Size.xy
#define f_OriginDistance f_Pos2_Orig_Size.z
#define f_Size f_Pos2_Orig_Size.w

#define f_Amplitude f_Ampl_Prop_Disp_Spee.x
#define f_PropagationAngle f_Ampl_Prop_Disp_Spee.y
#define f_DispersionAngle f_Ampl_Prop_Disp_Spee.z
#define f_Speed f_Ampl_Prop_Disp_Spee.w

#define ONE_THIRD 0.33333333333333333333333333
/**
* \param originDist distance from the origin (count only once for multiple uses of this function)
* \param direction left = -1 | right = 1
*/
void prepareNewParticle(float propagOffsetAngle, float disperAngle){
	feedOut.f_Pos2_Orig_Size.zw		= In[i].f_Pos2_Orig_Size.zw; // origin_distance + size
	feedOut.f_Amplitude			= In[i].f_Amplitude * ONE_THIRD;
	feedOut.f_PropagationAngle	= In[i].f_PropagationAngle + propagOffsetAngle;
	feedOut.f_DispersionAngle	= disperAngle;
	feedOut.f_Speed				= In[i].f_Speed; 
	
	feedOut.f_Pos2_Orig_Size.xy = In[i].f_Position 
		// subtract the Old(*) direction from position in order to get origin of the particle. * = Old direction is the propagation angle of the parent particle 
		- (vec2(cos(In[i].f_PropagationAngle),   sin(In[i].f_PropagationAngle))   * In[i].f_OriginDistance)  
		// add the New direction to get to position of the newly created particle
		+ (vec2(cos(feedOut.f_PropagationAngle), sin(feedOut.f_PropagationAngle)) * In[i].f_OriginDistance); 
}

#define isNAN(x) x!=x
void main()
{	
	// Do delete?
	if(In[i].v_Info.x){
		return;
	}

	feedOut.f_Pos2_Orig_Size	  = In[i].f_Pos2_Orig_Size; // position, origin_dist, size
	feedOut.f_Ampl_Prop_Disp_Spee = In[i].f_Ampl_Prop_Disp_Spee; // amp, propag, disp, speed

	// Emitt additional particles?
	bool doEmitParticle = In[i].v_Info.g;

	float angle = In[i].f_Ampl_Prop_Disp_Spee.z * ONE_THIRD; // = / 3

	if (doEmitParticle){
		feedOut.f_DispersionAngle = angle;
		feedOut.f_Amplitude = In[i].f_Amplitude * ONE_THIRD;
	}

	EmitVertex();
	EndPrimitive();


	if (doEmitParticle){
		prepareNewParticle(+angle, angle);
		EmitVertex();
		EndPrimitive();

		prepareNewParticle(-angle, angle);
		EmitVertex();
		EndPrimitive();
	}
	
}