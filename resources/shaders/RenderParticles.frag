#version 430

// ### Variables ###
// === Uniform ===
uniform int u_Mode;
uniform float u_Alpha;
uniform vec3 u_Color;
uniform sampler2D u_PointTexture;
// === Varying ===
// --- In ---
in block{
	vec2 v_TexCoord;
	vec4 v_Amp_Propag_Disper;
	//vec4 v_TexCoord_Amp_Propag;
} In;
// --- Out ---
layout (location = 0) out vec4 v_FragColor;


void main(){
	//v_FragColor = texture2D(u_PointTexture, In.v_TexCoord ) * u_Alpha;

	// Point
	v_FragColor = vec4(
		cos(In.v_Amp_Propag_Disper.y),// * In.v_Amp_Propag_Disper.w, // velocity x
		sin(In.v_Amp_Propag_Disper.y),// * In.v_Amp_Propag_Disper.w, // velocity y
		texture2D(u_PointTexture, gl_PointCoord).b * In.v_Amp_Propag_Disper.x, // 
		1
	);

	// Quad
	/*v_FragColor = vec4(
		cos(In.v_Amp_Propag_Disper.y),// * In.v_Amp_Propag_Disper.w, // velocity x
		sin(In.v_Amp_Propag_Disper.y),// * In.v_Amp_Propag_Disper.w, // velocity y
		texture2D(u_PointTexture, In.v_TexCoord.xy).b * In.v_Amp_Propag_Disper.x, // 
		1
	);*/
}


