#version 330 compatibility

uniform sampler2D permTexture;
uniform sampler1D simplexTexture;
uniform sampler2D gradTexture;
uniform float time; // Used for texture animation

uniform int u_Mode;
uniform sampler2D u_NoiseToNormal;


in vec2 v_texCoord2D;
in vec3 v_texCoord3D;
in vec4 v_color;

layout (location = 0) out vec4 v_FragColor;		// noise
layout (location = 1) out vec4 v_FragColor_1;	// noise gradient



//-------------- Noise version 3 ----------------
// Courtesy of http://www.sci.utah.edu/~leenak/IndStudy_reportfall/Perlin%20Noise%20on%20GPU.html

#define ONE 0.00390625
#define ONEHALF 0.001953125

float fade(float t) {
	//return t*t*(3.0-2.0*t); // Old fade
	return t*t*t*(t*(t*6.0-15.0)+10.0); // Improved fade
}
 
float noise(vec3 P)
{
	vec3 Pi = ONE*floor(P) + ONEHALF;

	vec3 Pf = P - floor(P);

	// Noise contributions from (x=0, y=0), z=0 and z=1
	float perm00 = texture2D(permTexture, Pi.xy).a;
	vec3  grad000 = texture2D(permTexture, vec2(perm00, Pi.z)).rgb * 4.0 - 1.0;
	float n000 = dot(grad000, Pf);
	vec3  grad001 = texture2D(permTexture, vec2(perm00, Pi.z + ONE)).rgb * 4.0 - 1.0;
	float n001 = dot(grad001, Pf - vec3(0.0, 0.0, 1.0));

	// Noise contributions from (x=0, y=1), z=0 and z=1
	float perm01 = texture2D(permTexture, Pi.xy + vec2(0.0, ONE)).a;
	vec3  grad010 = texture2D(permTexture, vec2(perm01, Pi.z)).rgb * 4.0 - 1.0;
	float n010 = dot(grad010, Pf - vec3(0.0, 1.0, 0.0));
	vec3  grad011 = texture2D(permTexture, vec2(perm01, Pi.z + ONE)).rgb * 4.0 - 1.0;
	float n011 = dot(grad011, Pf - vec3(0.0, 1.0, 1.0));

	// Noise contributions from (x=1, y=0), z=0 and z=1
	float perm10 = texture2D(permTexture, Pi.xy + vec2(ONE, 0.0)).a;
	vec3  grad100 = texture2D(permTexture, vec2(perm10, Pi.z)).rgb * 4.0 - 1.0;
	float n100 = dot(grad100, Pf - vec3(1.0, 0.0, 0.0));
	vec3  grad101 = texture2D(permTexture, vec2(perm10, Pi.z + ONE)).rgb * 4.0 - 1.0;
	float n101 = dot(grad101, Pf - vec3(1.0, 0.0, 1.0));

	// Noise contributions from (x=1, y=1), z=0 and z=1
	float perm11 = texture2D(permTexture, Pi.xy + vec2(ONE, ONE)).a;
	vec3  grad110 = texture2D(permTexture, vec2(perm11, Pi.z)).rgb * 4.0 - 1.0;
	float n110 = dot(grad110, Pf - vec3(1.0, 1.0, 0.0));
	vec3  grad111 = texture2D(permTexture, vec2(perm11, Pi.z + ONE)).rgb * 4.0 - 1.0;
	float n111 = dot(grad111, Pf - vec3(1.0, 1.0, 1.0));

	// Blend contributions along x
	vec4 n_x = mix(vec4(n000, n001, n010, n011), vec4(n100, n101, n110, n111), fade(Pf.x));

	// Blend contributions along y
	vec2 n_xy = mix(n_x.xy, n_x.zw, fade(Pf.y));

	// Blend contributions along z
	float n_xyz = mix(n_xy.x, n_xy.y, fade(Pf.z));

	return n_xyz;
}


float turbulence(int octaves, vec3 P, float lacunarity, float gain)
{	
	float sum = 0;
	float scale = 1;
	float totalgain = 1;
	for (int i = 0; i < octaves; i++){
		sum += totalgain * noise(P*scale);
		scale *= lacunarity;
		totalgain *= gain;
	}
	return abs(sum);
}


float fBm(int octaves, vec3 P, float power)
{	
	float sum = 0;
	float scale = 1;
	for (int i = 0; i < octaves; i++){
		sum += noise(P * scale);
		scale *= power;
	}
	return (sum / octaves);
}


void main( void )
{
	if (u_Mode == 1){
		vec3 coord = v_texCoord3D.xyz;
		coord += time;
		float n;
		n += noise(vec3(1.0 * coord.xyz));
		n = fBm(3, coord.xyz, 2.0);
		v_FragColor = vec4(0.5 + vec3(n, n, n), 1.0);  
		
	}
	//-----
	if (u_Mode == 2){
		vec2 coord = v_texCoord3D.xy;
		coord += time;
	
		const vec2 size = vec2(2.0, 0.0);
		const ivec3 off = ivec3(-1, 0, 1);

		float h = 30;
		float cur = texture(u_NoiseToNormal, coord).x;

		float right  = h * textureOffset(u_NoiseToNormal, coord, off.zy).x;
		float left	 = h * textureOffset(u_NoiseToNormal, coord, off.xy).x;
		float top	 = h * textureOffset(u_NoiseToNormal, coord, off.yz).x;
		float bottom = h * textureOffset(u_NoiseToNormal, coord, off.yx).x;
		
		vec3 va = normalize(vec3(size.xy, right - left));
		vec3 vb = normalize(vec3(size.yx, top   - bottom));
		vec4 normal = vec4(cross(va, vb), 1); // a = cur

		v_FragColor = normal;		
	}
}


