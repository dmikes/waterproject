#version 430

// === Uniform ===
uniform mat4 u_ModelMatrix;
uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;

uniform bool u_Refract;

// === Varying ===
// --- In ---
layout (location = 0) in vec3 a_Vertex; 
layout (location = 1) in vec3 a_Normal; 
layout (location = 2) in vec4 a_Color;  
layout (location = 3) in vec3 a_MultiTexCoord0; 
// --- Out ---

// === Local ===
out block{
	vec3 normal;
	vec4 color;
	vec3 texCoord;
	vec3 position;		// object space
	vec4 positionWorld; // world space
	vec4 positionView;	// view space
} Out;


void main(){
	Out.color = a_Color;
	Out.normal = normalize(mat3(u_ModelViewMatrix) * a_Normal); 
	Out.texCoord = a_MultiTexCoord0;

	Out.position		= a_Vertex;
	Out.positionWorld	= u_ModelMatrix * vec4(a_Vertex, 1);
	Out.positionView	= u_ModelViewMatrix * vec4(a_Vertex, 1);
	gl_Position  = u_ProjectionMatrix * Out.positionView; // if inactive geometry shader
	
}
