#version 430

// === Uniform ===
uniform sampler2D u_GradientTexture;
uniform int u_Mode;
uniform vec3 u_Color;
uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;

// === Varying ===
// --- In ---
layout (location = 0) in vec3 a_Vertex; 
layout (location = 1) in vec3 a_Normal; 
layout (location = 2) in vec4 a_Color;  
layout (location = 3) in vec2 a_MultiTexCoord0; 
// --- Out ---

out vs2tcs {
	vec3 position;
	vec3 color;
} Out;
// === Local ===


void main(){
	Out.position = a_Vertex.xyz;
	Out.color = a_Color.xyz;
	
}
