#version 400

//#extension GL_ARB_separate_shader_objects : enable
//#extension GL_ARB_enhanced_layouts : enable
//#extension GL_ARB_shader_atomic_counters : enable
//#extension GL_ARB_transform_feedback3 : enable
// ### Variables ###
// === Uniform ===
uniform sampler2D u_CollisionTexture;
uniform vec2 u_CollisionTexInvSize;
uniform vec2 u_CollisionTexShift;

uniform float u_SimSpeed;
uniform float u_Dampening;
uniform float u_ParticleHalfSize;
uniform float u_DispersionThreshold;
uniform int u_Mode;
uniform int u_StopSim;
uniform float u_CumulativeMotion;
// if u_Mode == fromTexture
uniform float u_MinDispersionAngle;
uniform bool u_UseBoundaries;

// ParticlesFromTexture
uniform sampler2D u_ParticleTexture;
uniform mat4 u_DirRotation;
uniform mat4 u_ModelMatrix;
uniform vec2 u_ParticleTexInvSize;

uniform uint u_VertexCount;
#define M_PI 3.1415926535897932384626433832795
#define MAP_254_TO_255 1.00393700787401574803149606299 // = 255 / 254.0f

// === Varying ===
// --- In ---
layout (location = 0) in vec4 a_Pos2_Orig_Size;
layout (location = 1) in vec4 a_Ampl_Prop_Disp_Spee; 

// --- Out ---
out block{
	vec4 f_Pos2_Orig_Size;
	vec4 f_Ampl_Prop_Disp_Spee;
	
	// x = do delete
	// y = do subdivide
	bvec4 v_Info; 
} Out;

#define f_Position f_Pos2_Orig_Size.xy
#define f_OriginDistance f_Pos2_Orig_Size.z
#define f_Size f_Pos2_Orig_Size.w

#define f_Amplitude f_Ampl_Prop_Disp_Spee.x
#define f_PropagationAngle f_Ampl_Prop_Disp_Spee.y
#define f_DispersionAngle f_Ampl_Prop_Disp_Spee.z
#define f_Speed f_Ampl_Prop_Disp_Spee.w

#define a_Position a_Pos2_Orig_Size.xy
#define a_Origin a_Pos2_Orig_Size.zw
#define a_OriginDistance a_Pos2_Orig_Size.z
#define a_Size a_Pos2_Orig_Size.w

#define a_Amplitude a_Ampl_Prop_Disp_Spee.x
#define a_PropagationAngle a_Ampl_Prop_Disp_Spee.y
#define a_DispersionAngle a_Ampl_Prop_Disp_Spee.z
#define a_Speed a_Ampl_Prop_Disp_Spee.w

const ivec3 off = ivec3(-1, 0, 1);
	
// declaration
void setupParticlesFromTexture();
void waveParticlePropagation();

#define USE_BOUNDARIES

// use attribute or uniform variable
//#define T_w a_Size
//#define T_w u_ParticleHalfSize
#define T_w u_DispersionThreshold


const float brdFrameIncrement = 65536;

int getBrdFrame(float compound){
	return int(floor(compound / 65536));
}

vec2 unpackSize(float compound){
	float brdFrames = floor(compound / 65536);
	float size = (compound - brdFrames * 65536);
	return vec2(brdFrames, size);
}

float packSize(int brdFrames, float size){
	return brdFrames * 65536 + size;
}




void main(){
	if (u_Mode == 2){
		setupParticlesFromTexture();
	} else {
		waveParticlePropagation();
	}
}

void waveParticlePropagation(){
	vec4 position = vec4(a_Position, 0, 1); // z coordinate is not important

	// We store the origin amplitude sign in sign of speed field
	float amplSign = sign(a_Speed);
	float absSpeed = abs(a_Speed);

	Out.f_OriginDistance = a_OriginDistance;
	Out.f_Size = a_Size;
	//
	Out.f_Ampl_Prop_Disp_Spee.x   = a_Amplitude - (absSpeed * u_Dampening * u_StopSim * amplSign);
	//Out.f_Ampl_Prop_Disp_Spee.x   = a_Amplitude * (1 - absSpeed * u_Dampening * u_StopSim);
	Out.f_Ampl_Prop_Disp_Spee.yzw = a_Ampl_Prop_Disp_Spee.yzw; // speed
	

	Out.v_Info = bvec4(false);
	// Subdivision criterion
	float w = a_DispersionAngle * a_OriginDistance;
	Out.v_Info.y = w > T_w; // Current particle will be divided

	
	// If a wave particles has low amplitude, discard it
	//Out.v_Info.x = abs(Out.f_Amplitude) < 0.01;
	// If is changes the sign delete it (-- = +, ++ = +, +- = -, 00 = 0)
	Out.v_Info.x = (Out.f_Amplitude * amplSign < 0);
	

#ifdef USE_BOUNDARIES
	if(u_UseBoundaries){
		vec4 boundary = texture2D(u_CollisionTexture, (position.xy - u_CollisionTexShift) * u_CollisionTexInvSize);

		//TODO: temporary hack - delete if long enough behind the border
		// Other solution: wait with division if approaching a border (pool) (because this particle could place new particles behind borders)
		if (boundary.r > 0 || boundary.g > 0){
			//vec2 brdFrame_Size = unpackSize(a_Pos2_Orig_Size.w);
			Out.f_Size += brdFrameIncrement;
			//Out.v_Info.r++; // if above treshold kill in geometry shader
		} else {
			Out.f_Size = 10.f; // zero the brdFrame
		}

		if(getBrdFrame(Out.f_Size) > 5){
			Out.v_Info.x = true;
		}
		// If wave particles is behind the boundary few consecutive frames
		/*if (In[i].v_Info.r > 5){
			return; // kill particle
		}*/

		//Out.f_PropagationAngle += (boundary.x + boundary.y + boundary.z);
		// Reflect wave particle
		if (boundary.b > 0.5){
			//Out.f_PropagationAngle += (boundary.x + boundary.y + boundary.z);
			// Particle is reflected
			vec2 boundaryNormal = ((boundary.rg * MAP_254_TO_255) - 0.5) * 2;
			vec2 propagDirection = vec2(cos(a_PropagationAngle), sin(a_PropagationAngle));
			vec2 reflectDirection = reflect(propagDirection, boundaryNormal);
			Out.f_PropagationAngle = atan(reflectDirection.y, reflectDirection.x);

			//float originDist = a_OriginDistance;// length(position.xy - a_Origin);
			//Out.f_Origin = position.xy + (-reflectDirection * originDist);
			vec2 newOrigin = position.xy + (-reflectDirection * a_OriginDistance);
			Out.f_OriginDistance = distance(position.xy, newOrigin);

			Out.f_DispersionAngle = a_DispersionAngle;// *(originDist / newOriginDist);
		}
	}
#endif
	// Move wave particle
	position.x += cos(Out.f_PropagationAngle) * absSpeed * u_StopSim;
	position.y += sin(Out.f_PropagationAngle) * absSpeed * u_StopSim;

	// no multiply needed. length(vec2(cos(a), sin(a))) == 1
	Out.f_OriginDistance += absSpeed * u_StopSim;
	Out.f_Position = position.xy;
}

#define isNAN(x) x!=x
//#define USE_DIRECT 

void setupParticlesFromTexture(){
	vec4 position = u_ModelMatrix * vec4(a_Position, 0, 1);
	vec2 texCoord = a_Position * u_ParticleTexInvSize; //TODO: texel fetch
	// Color from final texture from Object2Particles step
	vec4 color = texture2D(u_ParticleTexture, texCoord.xy);
	vec2 dir = mat2(u_DirRotation) * color.gb; 
	//vec2 dir = color.gb; 

	vec4 colors[8];
		
	colors[0] = textureOffset(u_ParticleTexture, texCoord.xy, off.xx); // -1, -1
	colors[1] = textureOffset(u_ParticleTexture, texCoord.xy, off.yx); //  0, -1
	colors[2] = textureOffset(u_ParticleTexture, texCoord.xy, off.zx); //  1, -1
	 
	colors[3] = textureOffset(u_ParticleTexture, texCoord.xy, off.xy); // -1,  0
	colors[4] = textureOffset(u_ParticleTexture, texCoord.xy, off.zy); //  1,  0

	colors[5] = textureOffset(u_ParticleTexture, texCoord.xy, off.xz); // -1,  1
	colors[6] = textureOffset(u_ParticleTexture, texCoord.xy, off.yz); //  0,  1
	colors[7] = textureOffset(u_ParticleTexture, texCoord.xy, off.zz); //  1, -1

	// Dispersion angle is computed as an average of neighbour direction differences
	vec4 diff1 = vec4(0);
	vec4 diff2 = vec4(0);
	int n = 0;
	#pragma unroll
	for (int j = 0; j < 8; ++j){
		if (colors[j].r > 0.01){ 
			if (n == 0) diff1 = colors[j];
			if (n == 1) diff2 = colors[j];
			n++;
		}
	}

	float propagation = atan(dir.y, dir.x);
	vec2 dir1 = (vec2(diff1.g, diff1.b));
	vec2 dir2 = (vec2(diff2.g, diff2.b));

	float cosOfDisp = dot(dir1, dir2);
	float dispersion = acos(cosOfDisp); // may be NAN
	
	if (cosOfDisp > 1){  
		dispersion = u_MinDispersionAngle;
	}
	

#ifdef USE_DIRECT
	float V = 0;// = u_CumulativeMotion * color.r;
	if(color.a > 0){
		V = u_CumulativeMotion * color.a * 10;
		propagation = 0.0; 
		dispersion = 2*M_PI;	
	}
	if(color.r > 0){
		V = u_CumulativeMotion * color.r; 
	}
#else
	// V = pi/2 * A * r^2     =>   A = (2.0 * V) / (pi * r^2)
	float V = u_CumulativeMotion * color.r; 
#endif
		
	float A = (2.0 * V) / (M_PI * u_ParticleHalfSize * u_ParticleHalfSize);
	float ampSign = 1-2*step(A, 0);

	Out.f_Pos2_Orig_Size = vec4(position.xy, 0, u_ParticleHalfSize);
	Out.f_Ampl_Prop_Disp_Spee = vec4(A, propagation, dispersion, ampSign * u_SimSpeed);
	Out.v_Info = bvec4(false);
	// Do delete
	Out.v_Info.x = isNAN(propagation);
}
