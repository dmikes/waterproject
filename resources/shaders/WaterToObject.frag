#version 400

uniform sampler2D u_WaterHeightTex;
uniform vec4 u_HeightTexToWorld; // xy = scale inverse, zw = scale * shift (MAD instruction)
uniform vec4 u_WaterPosClip;
uniform vec4 u_WaterPosWorld;
uniform int u_Step;

// For water height texture
uniform sampler2D u_WPSurfTex;
uniform sampler2D u_WPPointTex;
uniform vec2 u_WPTexScale;
uniform vec2 u_WPTexShift;
uniform float u_WP_Horizontal;
uniform float u_WP_Vertical;

#define USE_NOISE
uniform sampler3D u_Noise3D; 
uniform float u_NoiseTime;
uniform float u_NoiseStrength;
uniform vec3 u_NoiseScale; // xy = scale texture coordinates (different tex size), z = Noise scale pamater of water surface
uniform vec2 u_NoiseShift;

uniform vec2 u_SilCamPos;
uniform vec2 u_SilCamView;

uniform int u_AdjacentSize;
uniform vec4 u_AdjacentWPScale_Shift[4];
uniform sampler2D u_AdjacentWPSurfTex[4];
uniform sampler2D u_AdjacentWPPointTex[4];

in block{
	vec3 normal;	// view space
	vec3 normalObj;	// obj space
	vec4 position;	// clip space
	vec4 positionView;
	vec4 positionWorld;
	vec4 color; // drag force
	vec4 color1;// lift force
} In;

layout (location = 0) out vec4 v_FragColor;
layout (location = 1) out vec4 v_FragColor_1;

#define WATER_HEIGHT_SUM 1
#define VOLUME_CENTROIDS 2
#define VOLUME_FRAGMENTS 3
#define WATER_HOLE 4
#define DRAG_LIFT_FORCE 5

void main(){
	// Volume texture generation
	if (u_Step == 1){
		//if(In.position.z < u_WaterPosClip.z){
		//	discard;
		//}
		
		float waterHeight = texture(u_WaterHeightTex, (In.positionWorld.xy * u_HeightTexToWorld.xy) - u_HeightTexToWorld.zw ).r;
		if(In.positionWorld.z > waterHeight){
			discard;
		}

		vec3 normal = normalize(In.normalObj);
		//int isDown = (int(normal.z < 0));
		int orientation = (int(normal.z < 0) * 2) - 1; // Normal facing downward = positive depth value. Upwards = negative
		v_FragColor = vec4(orientation * (In.position.z - u_WaterPosClip.z), 0, 0, 0);
		//v_FragColor = vec4(waterHeight, 0, 0, 0);
	}
	// Bottom of the boat (water hole creation)
	if (u_Step == 2){
		v_FragColor = vec4(In.positionView.z - u_WaterPosWorld.z, 0, 0, 0);
	}

	// Drag and Lift force
	if (u_Step == 3){
		if(In.position.z < u_WaterPosClip.z){
			discard;
		}
		v_FragColor   = In.color;
		v_FragColor_1 = In.color1;
	}

	// Water height texture
	if (u_Step == 4){
		// Merge every displacement source
		vec2 texCoord = (In.position.xy * 0.5 + 0.5);
		//vec4 wpRender = texture(u_WPPointTex, (texCoord * u_WPTexScale) + u_WPTexShift);
		vec4 wpRender = vec4(0,0,0,0);
		
		float depth = 0;
		// Horizontal deviation is ommited (inverse lookup for height reduction)
		//depth += u_WP_Vertical * texture(u_WPSurfTex, (texCoord * u_WPTexScale) + u_WPTexShift).z;
#ifdef USE_NOISE
		depth += u_NoiseStrength * texture(u_Noise3D, vec3((texCoord * u_SilCamView + u_SilCamPos) * u_NoiseScale.z, u_NoiseTime)).a;
#endif
		for(int i = 0; i < u_AdjacentSize; i++){
			wpRender.xy += 			 	   texture(u_AdjacentWPPointTex[i], (texCoord * u_AdjacentWPScale_Shift[i].xy) + u_AdjacentWPScale_Shift[i].zw).xy;
			depth 		+= u_WP_Vertical * texture(u_AdjacentWPSurfTex[i],  (texCoord * u_AdjacentWPScale_Shift[i].xy) + u_AdjacentWPScale_Shift[i].zw).z;
		}

		// ambient waves?
		v_FragColor = vec4(depth, wpRender.xy, 1);
	}
}

