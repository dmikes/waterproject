#version 430

uniform vec3 u_LightPos;
uniform float u_Alpha;

uniform sampler2D u_ModelDiffuse;
uniform sampler2D u_ModelNormal;

uniform float u_WaterLevel;
uniform vec4 u_DeepWaterColor;

uniform sampler2D u_WaterHeightTex;
uniform vec4 u_HeightTexToWorld; // xy = scale inverse, zw = scale * shift (MAD instruction)

uniform bool u_Refract;
uniform float u_WaterClarity;
uniform float u_ReflectDarkening;

in block{
	vec3 normal;
	vec4 color;
	vec3 texCoord;
	vec3 position;		// object space
	vec4 positionWorld; // world space
	vec4 positionView;	// view space
} In;

layout (location = 0) out vec4 v_FragColor;

#define CUT_BY_WAVES

void main(){
#ifdef CUT_BY_WAVES
	vec2 heightCoord = (In.positionWorld.xy * u_HeightTexToWorld.xy) - u_HeightTexToWorld.zw;
	float waterFragLevel = texture(u_WaterHeightTex, heightCoord).r;
#else
	float waterFragLevel = u_WaterWindowPosition.z;
#endif

	if(In.positionWorld.z > waterFragLevel){ 
		discard;
	}

	//v_FragColor = texture2D(u_PointTexture, gl_PointCoord.st ) * u_Alpha;
	vec3 normal = normalize(In.normal);
	vec3 eye	= normalize(-In.positionView.xyz);
	vec3 lightRay		= normalize(u_LightPos - In.positionView.xyz);
	vec3 reflectedRay	= normalize(-reflect(lightRay, normal));
	
	float diffuse  = max( dot(normal, lightRay), 0.0); // must be positive
	float specular = pow( max( dot(reflectedRay, eye), 0.0), 128);

	vec4 color = texture2D(u_ModelDiffuse, In.texCoord.xy);
	v_FragColor = vec4(color.xyz * diffuse * u_ReflectDarkening, 1.0f); // reflection

	if (u_Refract){
		float depth = -In.positionWorld.z * u_WaterClarity;
		v_FragColor = mix(color * diffuse, u_DeepWaterColor, clamp(depth, 0, 1)); //refraction
	}
	//v_FragColor = vec4(normal, 1.0f);

}


