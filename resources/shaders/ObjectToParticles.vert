#version 330 compatibility

// === Uniform ===
uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;
//uniform mat4 u_MVP;
uniform sampler2D u_SilhouetteTexture;
uniform vec4 u_WaterPosScreen; // screen space
uniform int u_Step;
uniform int u_TargetMipmapLevel;

uniform vec3 u_ObjVelocity;


// === Varying ===
// --- In ---
layout (location = 0) in vec4 a_Vertex; //
layout (location = 1) in vec4 a_Normal; 
layout (location = 2) in vec4 a_Color;  
layout (location = 3) in vec2 a_MultiTexCoord0; 
// --- Out ---

// === Local ===
out block{
	vec3 normalObj;	// object space
	vec3 normal;	// view space
	vec4 position;	// clip space
	vec4 positionObj;
	vec4 color;
} Out;



void main(){
	Out.normalObj = a_Normal.xyz;
	Out.normal = normalize(mat3(u_ModelViewMatrix) * a_Normal.xyz); // normal matrix is only for uniform scales
	Out.positionObj = a_Vertex;
	Out.position = u_ProjectionMatrix * u_ModelViewMatrix * a_Vertex;
	gl_Position = Out.position; // if inactive geometry shader

	if(u_Step == 2){
		vec4 step1data = texture2D(u_SilhouetteTexture, (Out.position.xy * 0.5f) + 0.5f, 0);
		Out.color = vec4(0.0f, 0.0f, a_Normal.w, 1.0); // b = area of a triangle (strored in normal.w)

		// Out.color.z = Out.position;
		// If the N_z is positive (this means that the object is fully submerged at this pixel location)
		if(step1data.y > 0){
			// If the depth of the point is larger than the surface depth, 
			// Constant: We use a small bias so that the depth values of a face recorded in the previous step do not occlude the face itself at this step
			if((Out.position.z  - u_WaterPosScreen.z) - 0.001 > step1data.x){
				// the face is not on top of the object and the wave effect will be distributed (indirect wave effect)
				// note: visible only for non-concave objects
				Out.color.r = 1; // indirect
			} else {
				//, otherwise the face is on top of the surface and the wave effect will be direct.		
				Out.color.g = 1; // direct 
			}
		} else {
			// If the N_z is negative, the object is partially submerged at this pixel location and the wave effect is indirect
			// it will be distributed to the boundaries of the silhouette
			Out.color.r = 1; // indirect
		}
	}
}
