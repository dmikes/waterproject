#version 330 compatibility

// values 0/1
// x... for uniform color
// y... for attribude colors
// z... for textured colors
uniform ivec3 u_ColorMode; 
uniform int u_Mode;

uniform vec3 u_LightPos;
uniform vec3 u_Color;
uniform float u_Alpha;

uniform sampler2D u_ModelDiffuse;
uniform sampler2D u_ModelNormal;

uniform sampler2D u_PointTexture;
uniform samplerCube u_CubeTexture;

uniform bool u_UseWaterClip;
uniform float u_WaterLevel;

uniform vec4 u_WaterWindowPosition;

uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;

uniform bool u_Refract;

in block{
	vec3 v_Normal;
	vec4 v_Color;
	vec3 texCoord;
	vec3 position; // object space
	vec4 positionWorld;
	vec4 positionView;
} In;

layout (location = 0) out vec4 v_FragColor;



void main(){
	//v_FragColor = vec4(u_Color, 1.0f) * u_ColorMode.x + In.v_Color * u_ColorMode.y + texture2D(u_PointTexture, gl_PointCoord.st);
	
	if(u_UseWaterClip){
		if(In.positionWorld.z > (u_WaterWindowPosition.z)){ // TODO: water clip position
			discard;
		}
	}
	if(u_Refract){
		if(In.positionWorld.z > (u_WaterWindowPosition.z)){ // TODO: water clip position
			discard;
		}
	}

	switch(u_Mode){
		case 1: // Uniform Color
			v_FragColor = vec4(u_Color, 1.0f);
			break;
		case 2:	
			v_FragColor = In.v_Color;
			break;
		case 3: // texture color
			//v_FragColor = texture2D(u_PointTexture, gl_PointCoord.st ) * u_Alpha;
			vec3 normal = normalize(In.v_Normal);
			vec3 eye	= normalize(-In.positionView.xyz);
			vec3 lightRay		= normalize(u_LightPos - In.positionView.xyz);
			vec3 reflectedRay	= normalize(-reflect(lightRay, normal));
	
			float diffuse  = max( dot(normal, lightRay), 0.0); // must be positive
			float specular = pow( max( dot(reflectedRay, eye), 0.0), 128);

			vec4 color = texture2D(u_ModelDiffuse, In.texCoord.xy);
			v_FragColor = vec4(color.xyz * diffuse, 1.0f);

			if (u_Refract){
				float r = 1 + In.positionWorld.z / 100;
				v_FragColor = vec4(color.xyz * diffuse * r, 1);
			}
			//v_FragColor = vec4(normal, 1.0f);

			break;
		case 4: { // Cubemap
			vec3 cubeCoord = In.position.xzy;

			v_FragColor = texture(u_CubeTexture, (cubeCoord - 0.5)); // unpack to <0,1>
			//v_FragColor = vec4(cubeCoord, 1);
			//if(cubeCoord.z == 0){
			//	v_FragColor = vec4(1, 0, 1, 1);
			//}
			break;
		}
		case 5: { // Cubemap created
			vec3 cubeCoord = In.texCoord.xzy; // notice different coords
			v_FragColor = texture(u_CubeTexture, (cubeCoord - 0.5)); // unpack to <0,1>
			//if(cubeCoord.x == 0){
			//	v_FragColor = vec4(1, 0, 0, 1);
			//}
			//v_FragColor = vec4(cubeCoord, 1);
			break;
		}
	}

	
}

