#version 430

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;
 
uniform mat4 u_ModelViewMatrix;
uniform mat4 u_ProjectionMatrix;
uniform float u_ParticleHalfSize;

in block{
	//vec4 v_ObjectPosition;
	vec4 v_Amp_Propag_Disper;
	vec4 v_Pos1;
	vec4 v_Pos2;
	vec4 v_Pos3;
	vec4 v_Pos4;
} In[];

out block{
	vec2 v_TexCoord;
	vec4 v_Amp_Propag_Disper;
	//vec4 v_TexCoord_Amp_Propag;
} Out;
 
// TODO, vyzkouset jestli neni rychlejsi do shaderu predavat uz spocitany 4 body.
void main()
{	
	
	//mat4 MVP = u_ProjectionMatrix * u_ModelViewMatrix; //TODO: CPU

	Out.v_TexCoord = vec2(1.0f, 0.0f);
	Out.v_Amp_Propag_Disper = In[0].v_Amp_Propag_Disper;
	gl_Position = In[0].v_Pos1;
	EmitVertex();

	Out.v_TexCoord = vec2(1.0f, 1.0f);
	Out.v_Amp_Propag_Disper = In[0].v_Amp_Propag_Disper;
	gl_Position = In[0].v_Pos2;
	EmitVertex();

	Out.v_TexCoord = vec2(0.0f, 0.0f);
	Out.v_Amp_Propag_Disper = In[0].v_Amp_Propag_Disper;
	gl_Position = In[0].v_Pos3;
	EmitVertex();

	Out.v_TexCoord = vec2(0.0f, 1.0f);
	Out.v_Amp_Propag_Disper = In[0].v_Amp_Propag_Disper;
	gl_Position = In[0].v_Pos4;
	EmitVertex();

	/*Out.v_TexCoord_Amp_Propag = vec4(vec2(1.0f, 0.0f), In[0].v_Amp_Propag_Disper.xy);
	gl_Position = In[0].v_Pos1;//MVP * ( In[0].v_ObjectPosition + vec4(u_ParticleHalfSize, -u_ParticleHalfSize, 0, 0));
	EmitVertex();

	Out.v_TexCoord_Amp_Propag = vec4(vec2(1.0f, 1.0f), In[0].v_Amp_Propag_Disper.xy);
	gl_Position = In[0].v_Pos2;//MVP * ( In[0].v_ObjectPosition + vec4(u_ParticleHalfSize, u_ParticleHalfSize, 0, 0));
	EmitVertex();

	Out.v_TexCoord_Amp_Propag = vec4(vec2(0.0f, 0.0f), In[0].v_Amp_Propag_Disper.xy);
	gl_Position = In[0].v_Pos3;//MVP * ( In[0].v_ObjectPosition + vec4(-u_ParticleHalfSize, -u_ParticleHalfSize, 0, 0));
	EmitVertex();

	Out.v_TexCoord_Amp_Propag = vec4(vec2(0.0f, 1.0f), In[0].v_Amp_Propag_Disper.xy);
	gl_Position = In[0].v_Pos4;//MVP * ( In[0].v_ObjectPosition + vec4(-u_ParticleHalfSize, u_ParticleHalfSize, 0, 0));
	EmitVertex();*/

	EndPrimitive();

}