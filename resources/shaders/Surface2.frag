#version 430

// === Uniform ===
uniform sampler1D u_FresnelLookup;

uniform sampler2D u_LocalReflection;
uniform sampler2D u_LocalRefraction;
uniform samplerCube u_ReflectionMap;

uniform sampler3D u_Noise3D; 

uniform int u_Mode;
uniform float u_Alpha;
uniform ivec2 u_WinDim;
uniform vec3 u_Color;
uniform vec3 u_LightPos;
uniform vec3 u_CameraPos;
 
uniform float u_NoiseTime;
uniform float u_NoiseStrength;
uniform float u_NoiseScale;

//const float distotionPower = 0.12;
uniform float u_TexRayDistortion;
uniform float u_FresnelR0;
uniform float u_FresnelPow;

uniform vec2 u_SunEffect;

// === Varying ===
in tes2fs {
	vec4 positionUnmoved;	// clip space
	//vec4 positionScreen;	// clip space
	vec4 position;			// view space
	vec4 positionWorld;		// world space
	vec3 normal;			// view space
	vec3 normalWorld;		// world space
	vec4 color;	
	vec2 dZ;
} In;


// --- Out ---
layout (location = 0) out vec4 v_FragColor;


// Schlick's approaximation of Fresnel term
float fresnelTerm(float NdotL, float r0, float pow5) {
	float grazing = (1.0 - NdotL);
	return max(mix(pow(grazing, pow5), 1.0, r0), 0);
	//return max(r0 + (1.0 - r0) * pow(grazing, pow5), 0.0);
}

#define GRADIENT_NOISE
//#define UNMOVED_REFLECTION_COORD

void main(){
	//vec3 N = normalize(In.normal); // normal (view space)
	//vec3 E = normalize(-In.position.xyz); // to eye
	//
	//vec3 L = normalize(u_LightPos - In.position.xyz); // light ray
	//vec3 lightR = (reflect(-L, N)); // light reflected

	//float diffuse  = max(dot(N, L), 0.0); // must be positive
	//float specular = pow(max(dot(lightR, E), 0.0), 128);

	//v_FragColor = vec4(u_Color.xyz * diffuse + specular, u_Alpha);
	

	// Nice effect
	// v_FragColor = vec4( In.positionScreen.z, In.positionScreen.z / 1000, In.positionScreen.z / 1000, 1);

	//--------------------------
//	vec3 noiseCoord3 = vec3(In.positionWorld.xy * u_NoiseScale, u_NoiseTime);
//#ifdef GRADIENT_NOISE // Compute normals from gradients in shader from noise gradients (from preprocess step)
//	// This method is slower but we can change the Noise strength while running
//	vec3 grads = normalize( texture(u_Noise3D, noiseCoord3 ).xyz );
//	vec3 va = normalize(vec3(2.0, 0.0, grads.x));
//	vec3 vb = normalize(vec3(0.0, 2.0, grads.y));
//	vec3 Nw1 = cross(va, vb);
//#else //NORMAL_NOISE // Get normals from the preprocess step
//	// This method is faster but we can't change the Noise strength
//	vec3 Nw1 = normalize( texture2D(u_Noise3D, noiseCoord3).xyz );
//#endif
	
	//vec3 Nw = normalize(mix(Nw1, Nw2, 0.5));
	vec3 Nw = normalize( In.normalWorld ); // normal (world space)
	//vec3 Nw = normalize(vec3(-In.dZ / 2.0f, 1.0f));
	vec3 Ew = normalize(u_CameraPos - In.positionWorld.xyz); // to eye (world space)

	vec3 eyeRw = reflect(-Ew, Nw); // eye reflection (world space)

	float fr = fresnelTerm(dot(Nw, eyeRw), u_FresnelR0, u_FresnelPow);
	//float fr = texture1D(u_FresnelLookup, dot(Nw, eyeRw)).r;
	

	vec4 globalRefl = texture(u_ReflectionMap, eyeRw.xzy);
	
	// unmoved causes the textCoord to move out of the texture on the bottom of the screen (when waves)
#ifdef UNMOVED_REFLECTION_COORD
	vec4 unmoved = ((In.positionUnmoved / In.positionUnmoved.w) + 1) * 0.5; // clip space to screen space
	vec2 localReflCoord = (unmoved.xy) + ((Nw.xy * u_TexRayDistortion) / unmoved.z); // screen coord + distortion
#else
	vec2 localReflCoord = (gl_FragCoord.xy / u_WinDim) + (Nw.xy * u_TexRayDistortion); // screen coord + distortion
#endif
	vec4 localRefl = texture(u_LocalReflection, localReflCoord);
	vec4 localRefr = texture(u_LocalRefraction, localReflCoord);

	vec4 reflColor = mix(globalRefl, localRefl, localRefl.a);
	

	v_FragColor = mix(localRefr, reflColor, fr) + pow(globalRefl.x, u_SunEffect.x) * u_SunEffect.y;
	
	//v_FragColor = vec4(int(In.positionWorld.z > 0), 0, 0, 1);

	//v_FragColor = reflColor;
	//v_FragColor = vec4(Nw, 1);
	//v_FragColor = gl_FragCoord / 1000;
	//v_FragColor = vec4(In.positionUnmoved.z / 2000, 0, 0, 1);
	
	if(u_Mode == 2){ // if wireframe
		v_FragColor = vec4(0.5, 0.5, 0.5, 1);
	}
}

