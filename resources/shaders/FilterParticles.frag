#version 330 compatibility


uniform float u_Alpha;
uniform sampler2D u_InputTexture;
uniform vec2 u_TexSizeInv; // inverse number of size of the texture

in block{
	vec3 v_Normal;
	vec2 v_TexCoord;
} In;

layout (location = 0) out vec4 v_FragColor;
layout (location = 1) out vec4 v_FragColor_1;

#define M_PI 3.1415926535897932384626433832795

#define FILTER_HALFWIDTH 10
// filter half width (=radius) inverse
#define R_INV 0.1f
// introduce into vec2()
uniform int u_KernelSize;
uniform float u_InvKernelSize;
uniform int u_Axis;

#define X 0
#define Y 1

void main(){
	 
	vec4 deviation = vec4(0,0,0,1);
	vec4 gradient = vec4(0,0,0,1);

	vec2 texShift;
	float arg;
	float cosArg;
	float sinArg;
	if(u_Axis == X){
		// For filtering in X direction (seperable filter first pass)
		for(int p = -u_KernelSize; p <= u_KernelSize; ++p){ // p = relative Pixel coordinates
			texShift.x = p;
			// (pixel * Pi) / R
			arg = M_PI * p * u_InvKernelSize;
			cosArg = cos(arg);
			sinArg = sin(arg);
			vec4 curColor = texture(u_InputTexture, (gl_FragCoord.xy + texShift) * u_TexSizeInv);
			// Vertical deviation d_z(x)
			deviation.z += curColor.z * ( 0.5f * (cosArg+1) ); 
			// Horizontal deviation d_x(x)
			deviation.x += curColor.z * (-0.5f * sinArg * (cosArg+1) ); 
			// Horizontal deviation d_y(x)
			deviation.y += curColor.z * ( 0.25f * pow(cosArg+1, 2.0f) );
			
			// Surface gradient g_x(x)
			//gradient.x += curColor.z * (-0.5 * sinArg * (M_PI * u_InvKernelSize));
			// Surface gradient g_y(x)
			//gradient.y += curColor.z * ( 0.5 * (cosArg+1));
		}

		deviation.xyz = deviation.xyz * u_InvKernelSize;
		//gradient.xyz  = gradient.xyz  * u_InvKernelSize;
	} else {
		// For filtering in Y direction (seperable filter second pass)
		for(int p = -u_KernelSize; p <= u_KernelSize; ++p){ // p = relative Pixel coordinates
			texShift.y = p;
			arg = M_PI * p * u_InvKernelSize;
			cosArg = cos(arg);
			sinArg = sin(arg);
			vec4 curColor = texture(u_InputTexture, (gl_FragCoord.xy + texShift) * u_TexSizeInv);
			// Vertical deviation d_z(y)
			deviation.z += curColor.z * ( 0.5f * (cosArg+1) ); 
			// Horizontal deviation d_x(y)
			deviation.x += curColor.x * ( 0.25f * pow(cosArg+1, 2.0f) );
			// Horizontal deviation d_y(y)
			deviation.y += curColor.y * (-0.5f * sin(arg) * (cosArg+1) ); 

			// Surface gradient g_x(y)
			//gradient.x += curColor.x * ( 0.5 * (cosArg+1));
			// Surface gradient g_y(y)
			//gradient.y += curColor.y * (-0.5 * sinArg * (M_PI * u_InvKernelSize));
		}

		//deviation.xyz = color.xyz / (2*FILTER_HALFWIDTH+1);
		deviation.xyz = deviation.xyz * u_InvKernelSize;
		//gradient.xyz  = gradient.xyz  * u_InvKernelSize;
	}
	

	v_FragColor = deviation; 
	//v_FragColor_1 = gradient; 
	v_FragColor_1 = vec4(0,0,0,0);
}

