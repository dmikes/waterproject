#version 330 compatibility

uniform sampler2D u_SilhouetteTexture; // A
uniform sampler2D u_WaveEffectTexture; // B
uniform sampler2D u_SilhouetteEdgeTex; // C
uniform vec4 u_WaterPosScreen;
uniform vec2 u_InvTexSize;
uniform vec2 u_TexSize;
uniform int u_Step;
uniform int u_SourceMipmapLevel;
uniform int u_nMipmapLevels;

uniform float u_ObjSpeed;
uniform vec3 u_ObjDirection;


in block{
	vec3 normalObj;	// object space
	vec3 normal;	// view space
	vec4 position;	// clip space
	vec4 positionObj;
	vec4 color;
} In;

layout (location = 0) out vec4 v_FragColor;




int isValid(vec4 vec){
	//return int(vec.g > 1.0f && vec.r == 0.0f);
	return int(vec.b > 0.0f);
}

/**
* Object can be partially under the water level
* 
* @param vec4 color of step 1 sillhouette.
*/
int isSliceSilhouette(vec4 step1data){
	// Pixels is part of the silhouette if: 
	return int(step1data.r > 0 && step1data.g <= 0); //< error: creates double silhouette on object with "walls" up and "waleys" down (eg. ship) => solution:convex object
	//return int(step1data.b > 0);
}

/**
* Pixel is a boundary pixel if there is a direction vector of a boundary normals stored in the pixel values.
* 
* @param pixel. Channels: Green = x coordinate of boundary normal. Blue = y coordinate of boundary normal.
*/
bool isBoundaryPixel(vec4 data){
	//return (data.g == 0	&& data.b == 0);
	return data.a == 1.0f;
}

void main(){
	if(In.position.z < u_WaterPosScreen.z){
		discard; // no face cullings
	}
	// Step 1: Low resolution silhouette of the object from ortho top view
	// r = depth in water (how much is the object submerged by water. fragment depth from water level)
	// g = vertical component of normal (determine surface orientation in top ortho view. is normal up or down)
	// b = is pixel part of the silhouette?
	if(u_Step == 1){
		vec3 normal = normalize(In.normal);
		// color.x = distance from water surface, color.y = vertical normal direction from the camera (normal.z)
		v_FragColor = vec4(In.position.z - u_WaterPosScreen.z, normal.z + 0.02, 0, 1);	
	}

	// Step 2: Draw each face as a point, writing the wave effect of the face
	// r = indirect wave effect 
	// g = direct wave effect
	// b = r channel from step 1 (save 1 texture read in the next step)
	// a = g channel from step 1 (save 1 texture read in the next step)
	if(u_Step == 2){
		vec2 coord = (In.position.xy * 0.5f) + 0.5f; // screen (after P projection) to texture coordinates
		// step 1 output texture
		vec4 step1data = texture2D(u_SilhouetteTexture, coord, 0); 
		
		// V_effect = A_face * (U_face . N_face) * delta t

		//vec3 objVel = u_ObjVelocity;
		//vec3 objVel = mat3(u_RotVelocity) * u_ObjVelocity;
		// TODO: add water level
		float reduce = log(-In.positionObj.z + 1); //TODO: exponencially
		float V = reduce * u_ObjSpeed * In.color.b * dot(u_ObjDirection, In.normalObj); // Volume effect
		v_FragColor = vec4(V * In.color.r, In.color.g, step1data.r, step1data.g); 
		//v_FragColor = vec4(In.normalObj.xyz, 1); 
		//v_FragColor = vec4(u_ObjVelocity, 1); 
		//v_FragColor = vec4(10 * In.color.b, 0, 0, 1);
	}

	// Step 3: Identify boundary pixels and assign boundary directions
	// r = indirect wave effect
	// g = direction of the boundary normal (x)
	// b = direction of the boundary normal (y)
	// a = number of color composites (elementar pixel: alpha = 1)
	if(u_Step == 3){
		//vec2 coord = (In.position.xy * 0.5f) + 0.5f;
		// step 1 output texture
		//TODO: textureOffset
		ivec2 coord = ivec2((In.position.xy * 0.5f + 0.5) * u_TexSize);

		vec4 cur = texelFetch(u_SilhouetteTexture, coord, 0);
		vec4 right	= texelFetchOffset(u_SilhouetteTexture, coord, 0, ivec2(1,0));
		vec4 left	= texelFetchOffset(u_SilhouetteTexture, coord, 0, ivec2(-1,0));
		vec4 top	= texelFetchOffset(u_SilhouetteTexture, coord, 0, ivec2(0,1));
		vec4 bottom = texelFetchOffset(u_SilhouetteTexture, coord, 0, ivec2(0,-1));
		
		// is boundary?
		int isBound		= isSliceSilhouette(cur);
		int isBoundRight	= isSliceSilhouette(right);
		int isBoundLeft		= isSliceSilhouette(left);
		int isBoundTop		= isSliceSilhouette(top);
		int isBoundBottom	= isSliceSilhouette(bottom);

		// step 2 output texture
		vec4 effect = texelFetch(u_WaveEffectTexture, coord, 0); 
		
		// Is not boundary pixel (store 
		v_FragColor = vec4(effect.r, 0, 0, 0.0f); //important: alpha = count of pixel addents
		 
		// == 0 : outer boundary |  == 1 : would be inner boundary
		if(isBound == 0){ 
			vec2 propagDir; // propagation direction

			propagDir.x = (-isBoundRight + isBoundLeft);
			propagDir.y = (-isBoundTop   + isBoundBottom);
			propagDir = normalize(propagDir);

			// Is boundary pixel
			v_FragColor = vec4(0, propagDir.x, propagDir.y, 1.0f); // important: alpha = count of pixels contributions in a summed color
			// If the Normal.z value at this pixel is positive (object is fully submerged) invert the direction
			// because if only the bottom flat face is included (only one face under water) 
			if (effect.a < 0){
				v_FragColor = vec4(0, -propagDir.x, -propagDir.y, 1.0f);
			}

			// Direct wave effect
			// Is inner part of object but is not a boundary ("gradient" == 0)
			if(isBoundRight + isBoundLeft + isBoundTop + isBoundBottom == 0){
				v_FragColor = vec4(effect.r, 0, 0, 0.0f);
			}
		}		
	}
	// Step 4: Sum neighbouring directions in order to smooth it in step 5
	// r = indirect wave effect
	// g = direction of the boundary normal (x)
	// b = direction of the boundary normal (y)
	// a = number of color composites (elementar pixel: alpha = 1)
	if(u_Step == 4){
		// Constant: texture is not bordered due to higher texture resolution than the current level image.
		// The texture contains the whole mipmap tree (left bottom part overriden because its temp texture and only the current part is important)
		float levelShrink = pow(2.0f, -u_SourceMipmapLevel) * 0.99999f;
		
		vec2 coord = ((In.position.xy * 0.5f) + 0.5f) * levelShrink;
		vec4 bottomLeft	 = texture2D(u_SilhouetteEdgeTex, coord,											0);
		vec4 bottomRight = texture2D(u_SilhouetteEdgeTex, coord + levelShrink * vec2(u_InvTexSize.x, 0),	0);
		vec4 topLeft	 = texture2D(u_SilhouetteEdgeTex, coord + levelShrink * vec2(0, u_InvTexSize.y),	0);
		vec4 topRight	 = texture2D(u_SilhouetteEdgeTex, coord + levelShrink * u_InvTexSize,				0);
		
		// We identify how many of these four pixels are on the silhouette boundary
		// The final direction vector is taken as the sum all four direction vectors
		v_FragColor.yzw = bottomLeft.yzw + bottomRight.yzw + topLeft.yzw + topRight.yzw;
		
		// wave effect is the sum of the wave effects from all non-boundary pixels
		v_FragColor.x = (bottomLeft.x + bottomRight.x + topLeft.x + topRight.x);// // pixel is non-boundary : v_FragColor.a == 0.0f

	}
	// Step 5: Final texture - silhouette with smoothed directions and distributed wave effect
	// r = indirect wave effect
	// g = direction of the boundary normal (x)
	// b = direction of the boundary normal (y)
	// a = direct wave effect
	if(u_Step == 5){
		vec2 coord = (In.position.xy * 0.5f) + 0.5f; // projection coord to texture coord
		// first texture = max mipmap level (1px)
		vec4 average = vec4(0,0,0,0);
		vec4 curLevel;
		for(int i = u_nMipmapLevels - 1; i >= 0; --i)
		{
			curLevel = texture2D(u_SilhouetteEdgeTex, coord, i);
			
			if(int(curLevel.a) != 0){ // alpha is the number of pixel which have been sumed
				curLevel.xyz /= curLevel.a;
			}

			average += curLevel;
		}
		average /= u_nMipmapLevels;
	
		// curLevel is the level 0 after the loop. Level 0 has the information about the real silhouette boundary.
		v_FragColor.xyz = average.xyz * int(isBoundaryPixel(curLevel));
		
		// direct wave effect
		vec4 effect = texture2D(u_WaveEffectTexture, coord, 0);
		v_FragColor.a = effect.g; 
	}
}



