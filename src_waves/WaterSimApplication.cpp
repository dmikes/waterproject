#include "WaterSimApplication.h"

#include "devil.h"

#include "utils.h"

#include "MeshVBO.h"
#include "Camera.h"
#include "Stage.h"
#include "ConfigIni.h"
#include "GUI.h"

#include "Model3D.h"

#include "WaveParticleSimulation.h"
#include "DrawableWrapper.h"
#include "ObjectToWaterCoupling.h"
#include "WaterToObjectCoupling.h"
#include "wrappers.h"
#include "FilterParticles.h"
#include "Texture2D.h"
#include "Camera.h"
#include "NoiseGenerator.h"
#include "WaterSurface.h"
#include "FloatingObject.h"
#include "ResourceList.h"
#include "WaterOptics.h"

#include "Transformation3D.h"


using namespace std;
typedef GraphicsContext GC;

WaterSimApplication::WaterSimApplication(int argc, char* argv[])
{
	defaultSimFile = paths.waterSimulationConfig + "default.ini";


	//inputConfig = paths.waterSimulationConfig + "setup_01.ini";
	if (argc > 1){ // Current scene
		int nScene = atoi(argv[1]);
		nScene = max(nScene, 0);
		nScene = min(nScene, 7);
		curScene = static_cast<E_Scene>(nScene);	
	}
	if (argc > 2){
		sceneParam = atoi(argv[2]);
	}
	if (argc > 3){ // Testing config (water simulation)
		inputConfig = paths.waterSimulationConfig + std::string(argv[3]);
		gc.runFromConsole = true;
	}
	if (argc > 4){ // Testing 
		gc.testRun = static_cast<bool>(atoi(argv[4]));
	}
}


WaterSimApplication::~WaterSimApplication()
{
	cleanup(); // clean up the "new" resources
}


void WaterSimApplication::setGUICompileShaders(void (TW_CALL *function)(void *clientData)){
	guiCompileShaders = function;
}


void WaterSimApplication::display(GLFWwindow* mainWindow) {
	static double initTime  = glfwGetTime();
	static double startTime = glfwGetTime();
	static double lastTimeUpdate = glfwGetTime();
	static int nFrames = 0;
	static int nFramesTotal = 0;

	double currentTime = glfwGetTime();
	double totalTime = currentTime - initTime;	
	nFramesTotal++;

	gc.avgFramesPerSec = static_cast<uint>(nFramesTotal / totalTime);

	nFrames++;
	if ( currentTime - startTime >= 1.0 ){ // do every second
		gc.framesPerSec = nFrames;
		nFrames = 0;
		startTime += currentTime - startTime;
	}

	if (gc.testRun && curScene == SCE_PARTICLE_TEST){
		waveParticles->insertWaveParticles(vec2(0, 0), 1000);
	}

	if (gc.testRun){
		stage.measureUpdate(gc);
		stage.measure(gc);
	} else {
		if (currentTime - lastTimeUpdate >= gc.simulationSpeed){
			gc.deltaTime = static_cast<float>(currentTime - lastTimeUpdate);
			stage.update(gc);
			lastTimeUpdate += gc.simulationSpeed;
			keyboard.keyboardCheckState(gc, mainWindow, gc.deltaTime);
		}
		//stage.update(gc);
		stage.display(gc);
	}

	if (gc.testRun && gc.frameCount == gc.simConf->runUntil)	{	
		stage.writeMeasurementResults(gc, "../measurements/", inputConfig);
		exitSimulation(0);
	}

}

void WaterSimApplication::exitSimulation(int errcode){
	quitProgram = true;
}

void WaterSimApplication::checkRequirements(){
	int maxPointSize;
	glGetIntegerv(GL_POINT_SIZE_MAX, &maxPointSize);
	printf("Max point size: %d\n", maxPointSize);
}

void WaterSimApplication::enableState(){
	glEnable(GL_TEXTURE_1D);
	glEnable(GL_TEXTURE_2D);
	//glEnable(GL_TEXTURE_CUBE_MAP)
	glEnable(GL_TEXTURE_3D);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void WaterSimApplication::initShaders(){
	
	gc.addShaderSet(GC::SHA_WAVE_SURFACE,		new ShaderSet(paths.shaderConfig + "WaveSurface.ini"));
	gc.addShaderSet(GC::SHA_WAVE_PARTICLE_SIM,	new ShaderSet(paths.shaderConfig + "WaveParticleSimulation.ini"));
	gc.addShaderSet(GC::SHA_FIXED_PIPELINE,		new ShaderSet(paths.shaderConfig + "FixedPipeline.ini"));
	gc.addShaderSet(GC::SHA_RENDER_PARTICLES,	new ShaderSet(paths.shaderConfig + "RenderParticles.ini"));
	gc.addShaderSet(GC::SHA_FILTER_PARTICLES,	new ShaderSet(paths.shaderConfig + "FilterParticles.ini"));
	gc.addShaderSet(GC::SHA_DISPLAY_TEX,		new ShaderSet(paths.shaderConfig + "DisplayTexture.ini"));
	gc.addShaderSet(GC::SHA_OBJECT_PARTICLE_GEN, new ShaderSet(paths.shaderConfig + "ObjectToParticlesGeneration.ini"));
	gc.addShaderSet(GC::SHA_WATER_TO_OBJECT,	new ShaderSet(paths.shaderConfig + "WaterToObject.ini"));
	gc.addShaderSet(GC::SHA_WATER_OPTICS,		new ShaderSet(paths.shaderConfig + "WaterOptics.ini"));
	gc.addShaderSet(GC::SHA_COMPUTE,			new ShaderSet(paths.shaderConfig + "ComputeExample.ini"));
	gc.addShaderSet(GC::SHA_REDUCTION,			new ShaderSet(paths.shaderConfig + "SumReduction.ini"));
	gc.addShaderSet(GC::SHA_REDUCTION4,			new ShaderSet(paths.shaderConfig + "SumReduction4.ini"));
	gc.addShaderSet(GC::SHA_FLOATER_DEVIATION,	new ShaderSet(paths.shaderConfig + "FloaterDeviation.ini"));

	// Set transform feedback before shader link
	gc.getShaders(gc.SHA_WAVE_PARTICLE_SIM)->beforeLinkFunc = [](GLint programId){
		const GLchar* feedbackVaryings[] = {
			"feedback.f_Pos2_Orig_Size",
			"feedback.f_Ampl_Prop_Disp_Spee"
		};
		glTransformFeedbackVaryings(programId, 2, feedbackVaryings, GL_INTERLEAVED_ATTRIBS); // GL_INTERLEAVED_ATTRIBS vs GL_SEPARATE_ATTRIBS
	};

	compileShaders();
}

void WaterSimApplication::initCameras(){
	gc.addCamera(GC::CAM_PERSP, new Camera(paths.cameraConfig + "PerspMain.ini"));
	gc.addCamera(GC::CAM_ORTHO, new Camera(paths.cameraConfig + "OrthoTopView.ini"));
	gc.addCamera(GC::CAM_ORTHO_TEXTURE, new Camera(paths.cameraConfig + "OrthoTextureView.ini"));
	gc.addCamera(GC::CAM_OBJECT_SILHOUETTE, new Camera(paths.cameraConfig + "OrthoSilhouetteView.ini"));
	gc.addCamera(GC::CAM_CUBE_MAP_RENDER, new Camera(paths.cameraConfig + "PerspCubeMapRender.ini"));
	gc.addCamera(GC::CAM_NOISE_RENDER, new Camera(paths.cameraConfig + "PerspCubeMapRender.ini"));
	gc.addCamera(GC::CAM_PLANE, new Camera(paths.cameraConfig + "OrthoPlaneCam.ini"));

	gc.mainCamera = gc.getCamera(GC::CAM_PERSP);
	gc.windowCamera = gc.mainCamera;
	
	// Lights 
	gc.addLight(gc.LIG_MAIN, new Light());
}

void WaterSimApplication::initTextures(){
	// --- Textures ---
	Texture* PerlinNoise3D = new Texture();

	Texture* borders = new Texture();
	borders->init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_EDGE);
	std::string boundaryName = paths.images + "boundary/no_boundary.png";
	if (curScene == E_Scene::SCE_POOL || curScene == E_Scene::SCE_HOUSE_POOL) {
		boundaryName = paths.images + "boundary/pool.png";
	}
	if (curScene == E_Scene::SCE_POOL_OBSTACLE) {
		boundaryName = paths.images + "boundary/level1_u.png";
	}	
	borders->loadImage(boundaryName.c_str());

	Texture* pointTex = new Texture();
	std::string pointTexFile = paths.images + "circle_01.png";
	pointTex->init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_EDGE);
	pointTex->loadImage(pointTexFile.c_str());

	// --- enviromental cubemap ---
	Texture* skyboxTex = new Texture();
	skyboxTex->init(GL_TEXTURE_CUBE_MAP, GL_LINEAR, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	std::array<std::string, 6> names = { "xpos", "xneg", "ypos", "yneg", "zpos", "zneg" };	

	if (curScene == SCE_LIGHTHOUSE){
		
		skyboxTex->loadImageCubemap(paths.images + "skybox/reef/", names, ".tga", GL_RGB, GL_RGBA, GL_UNSIGNED_BYTE);
	} else 	if (curScene == SCE_HOUSE_POOL || curScene == SCE_POOL || curScene == SCE_POOL_OBSTACLE){
		skyboxTex->loadImageCubemap(paths.images + "skybox/hills/", names, ".tga", GL_RGB, GL_RGBA, GL_UNSIGNED_BYTE);
	} else {
		skyboxTex->loadImageCubemap(paths.images + "skybox/sky/", names, ".tga", GL_RGB, GL_RGBA, GL_UNSIGNED_BYTE);
	}

	Texture* localRefraction = new Texture();
	localRefraction->init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_EDGE);
	localRefraction->allocate(gc.getCamera(gc.CAM_PERSP)->winDim, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);

	Texture* localReflectionMap = new Texture();
	localReflectionMap->init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_EDGE);
	localReflectionMap->allocate(gc.getCamera(gc.CAM_PERSP)->winDim, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);

	Texture* lookupFresnel = new Texture();
	lookupFresnel->init(GL_TEXTURE_1D, GL_LINEAR, GL_CLAMP_TO_EDGE);
	lookupFresnel->loadImage(paths.images + "lookup/fresnel_water_linear.png");
	//lookupFresnel.loadImage(paths.images + "lookup/fresnel_water_sRGB.png");

	Texture* texCompute = new Texture();
	texCompute->init(GL_TEXTURE_2D, GL_LINEAR, GL_MIRRORED_REPEAT);
	texCompute->textureUnit = 1;
	glActiveTexture(GL_TEXTURE0 + texCompute->textureUnit);
	glBindTexture(GL_TEXTURE_2D, texCompute->texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, NULL);

	Texture* texTemp = new Texture();
	texTemp->init(GL_TEXTURE_2D, GL_LINEAR, GL_MIRRORED_REPEAT);
	texTemp->textureUnit = 2;
	glActiveTexture(GL_TEXTURE0 + texTemp->textureUnit);
	glBindTexture(GL_TEXTURE_2D, texTemp->texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 128, 128, 0, GL_RGBA, GL_FLOAT, NULL);
	
	// ===
	gc.addTexture(GC::TEX_PARTICLE_SHAPE, pointTex);
	gc.addTexture(GC::TEX_BORDERS, borders);
	gc.addTexture(GC::TEX_SKYBOX, skyboxTex);
	gc.addTexture(GC::TEX_LOCAL_REFRACTION, localRefraction);
	gc.addTexture(GC::TEX_LOCAL_REFLECTION, localReflectionMap);
	gc.addTexture(GC::TEX_FRESNEL_LOOKUP, lookupFresnel);
	gc.addTexture(GC::TEX_PERLIN_NOISE_3D, PerlinNoise3D);
	gc.addTexture(GC::TEX_COMPUTE, texCompute);
	gc.addTexture(GC::TEX_TEMP, texTemp);
}

// MeshVBO data must be set before model MeshVBO.init and before Object3D.init
void WaterSimApplication::initPrimitives(){
	MeshVBO* cube = new MeshVBO();
	cube->setVertices(8, (GLfloat*)cubeVertices);
	cube->setTexCoords((GLfloat*)cubeTexCoords);
	cube->setIndices(24, (GLushort*)cubeFaceIndices, MeshVBO::VBO_USHORT);
	cube->setTextureCoordFormat(3);
	cube->init();

	MeshVBO* cubeLine = new MeshVBO();
	cubeLine->setVertices(8, (GLfloat*)cubeVertices);
	cubeLine->setTexCoords((GLfloat*)cubeTexCoords);
	cubeLine->setIndices(24, (GLushort*)cubeLineIndices, MeshVBO::VBO_USHORT);
	cubeLine->init();

	MeshVBO* quad = new MeshVBO();
	quad->setVertices(4, (GLfloat*)planeVertices);
	quad->init();

	MeshVBO* unitQuad = new MeshVBO();
	unitQuad->setVertices(4, (GLfloat*)unitPlaneVertices);
	unitQuad->setNormals((GLfloat*)unitPlaneNormals);
	unitQuad->setTexCoords((GLfloat*)unitPlaneTexCoords);
	unitQuad->init();

	gc.addMesh(GC::MES_CUBE, cube);
	gc.addMesh(GC::MES_CUBE_LINE, cubeLine);
	gc.addMesh(GC::MES_PLANE, quad);
	gc.addMesh(GC::MES_PLANE_UNIT, unitQuad);
}

void WaterSimApplication::initModels(){
	// --- Transformations ---
	Transformation3D tfSkybox = Transformation3D(paths.transforms + "skybox.ini");

	Transformation3D tfLighthouse	= Transformation3D(paths.transforms + "lighthouse.ini");
	Transformation3D tfTerrain		= Transformation3D(paths.transforms + "terrain.ini");
	Transformation3D tfShip_01		= Transformation3D(paths.transforms + "boat.ini");
	Transformation3D tfSphere		= Transformation3D(paths.transforms + "sphere.ini");
	Transformation3D tfIsle1		= Transformation3D(paths.transforms + "isle_1.ini");
	Transformation3D tfIsle2		= Transformation3D(paths.transforms + "isle_2.ini");
	Transformation3D tfIsle3		= Transformation3D(paths.transforms + "isle_3.ini");
	Transformation3D tfHousePool	= Transformation3D(paths.transforms + "housepool.ini");
	Transformation3D tfPool			= Transformation3D(paths.transforms + "pool.ini");

	// --- Floater models ---
	
	std::string resPath1 = paths.models + "boat_01/Texture/";
	std::string resPath2 = paths.models + "shallop/";
	Model3D* shipNormal = new Model3D(paths.models + "boat_01/obj/normal.obj", resPath1.c_str());
	Model3D* shallop    = new Model3D(paths.models + "shallop/shallop_simple.obj", resPath2.c_str());

	Model3D* shipConvex = new Model3D(paths.models + "boat_01/obj/convex.obj", true);
	Model3D* dummyShipLevel1 = new Model3D(paths.models + "DummyShip/ship_convex_1.obj", true);
	Model3D* dummyShipLevel2 = new Model3D(paths.models + "DummyShip/ship_convex_2.obj", true);
	Model3D* dummyShipLevel3 = new Model3D(paths.models + "DummyShip/ship_convex_3.obj", true);
	Model3D* dummyShipLevel4 = new Model3D(paths.models + "DummyShip/ship_convex_4.obj", true);
	Model3D* dummyShipLevel5 = new Model3D(paths.models + "DummyShip/ship_convex_5.obj", true);
	Model3D* dummyShipLevel6 = new Model3D(paths.models + "DummyShip/ship_convex_6.obj", true);


	//Model3D* sphere = new Model3D(paths.models + "Sphere2/sphere.obj", true);
	std::string resPath3 = paths.models + "Ball/Texture/";
	Model3D* sphere = new Model3D(paths.models + "Ball/ball.obj", resPath3.c_str(), true, true);
	

	// Ship type 1
	shipNormal->setDefaultTransform(tfShip_01);
	shipNormal->init(gc);
	gc.addModel(shipNormal);

	shipConvex->setDefaultTransform(tfShip_01);
	shipConvex->init(gc);
	gc.addModel(shipConvex);

	sphere->setDefaultTransform(tfSphere);
	sphere->init(gc);
	gc.addModel(sphere);

	dummyShipLevel1->setDefaultTransform(tfShip_01);
	dummyShipLevel1->init(gc);
	gc.addModel(dummyShipLevel1);

	//dummyShipLevel2->setDefaultTransform(tfShip_01);
	//dummyShipLevel2->init(gc);
	//gc.addModel(dummyShipLevel2);

	//dummyShipLevel3->setDefaultTransform(tfShip_01);
	//dummyShipLevel3->init(gc);
	//gc.addModel(dummyShipLevel3);
	//
	//dummyShipLevel4->setDefaultTransform(tfShip_01);
	//dummyShipLevel4->init(gc);
	//gc.addModel(dummyShipLevel4);

	//dummyShipLevel5->setDefaultTransform(tfShip_01);
	//dummyShipLevel5->init(gc);
	//gc.addModel(dummyShipLevel5);

	//dummyShipLevel6->setDefaultTransform(tfShip_01);
	//dummyShipLevel6->init(gc);
	//gc.addModel(dummyShipLevel6);

	shallop->setDefaultTransform(tfShip_01);
	shallop->init(gc);
	gc.addModel(shallop);
	
	// --- Static models ---
	std::string lighthouseTextures = paths.models + "Lighthouse/Textures_diffuse/";
	Model3D* lighthouse = new Model3D(paths.models + "Lighthouse/lighthouse.3ds", lighthouseTextures.c_str());

	std::string terrainTextures = paths.models + "Terrain_sand/Texture/";
	Model3D* terrain1 = new Model3D(paths.models + "Terrain_sand/isle_1.obj", terrainTextures.c_str());
	Model3D* terrain2 = new Model3D(paths.models + "Terrain_sand/isle_2.obj", terrainTextures.c_str());
	Model3D* terrain3 = new Model3D(paths.models + "Terrain_sand/isle_3.obj", terrainTextures.c_str());

	std::string housepoolTextures = paths.models + "HousePool/";
	Model3D* housePool = new Model3D(paths.models + "HousePool/housepool_retex.obj", housepoolTextures.c_str(), true); //subfolder is part of the mtl

	std::string poolTextures = paths.models + "Pool/";
	Model3D* pool = new Model3D(paths.models + "Pool/pool_plain.obj", poolTextures.c_str());
	Model3D* poolObstacle = new Model3D(paths.models + "Pool/pool.obj", poolTextures.c_str());

	lighthouse->setDefaultTransform(tfLighthouse);
	gc.addModel(lighthouse);

	terrain1->setDefaultTransform(tfIsle1);
	gc.addModel(terrain1);

	terrain2->setDefaultTransform(tfIsle2);
	gc.addModel(terrain2);

	terrain3->setDefaultTransform(tfIsle3);
	gc.addModel(terrain3);

	housePool->setDefaultTransform(tfHousePool);
	gc.addModel(housePool);

	pool->setDefaultTransform(tfPool);
	gc.addModel(pool);

	poolObstacle->setDefaultTransform(tfPool);
	gc.addModel(poolObstacle);

	if (curScene == E_Scene::SCE_LIGHTHOUSE){
		lighthouse->init(gc);
		terrain1->init(gc);
		terrain2->init(gc);
		terrain3->init(gc);

		gc.staticObjects.push_back(lighthouse);
		gc.staticObjects.push_back(terrain1);
		gc.staticObjects.push_back(terrain2);
		gc.staticObjects.push_back(terrain3);
	}
	if (curScene == E_Scene::SCE_HOUSE_POOL){
		housePool->init(gc);
		gc.staticObjects.push_back(housePool);
	}
	if (curScene == E_Scene::SCE_POOL){
		pool->init(gc);
		gc.staticObjects.push_back(pool);
	}
	if (curScene == E_Scene::SCE_POOL_OBSTACLE){
		poolObstacle->init(gc);
		gc.staticObjects.push_back(poolObstacle);
	}

	
	// Transform
	gc.addTransform(GC::TRA_SHIP,   new Transformation3D(paths.transforms + "boat.ini"));
	gc.addTransform(GC::TRA_SKYBOX, new Transformation3D(paths.transforms + "skybox.ini"));
}


void WaterSimApplication::initFloaters(){
	// Floater templates
	FloatingObjectConfig*floater_type1 = new FloatingObjectConfig(paths.floaterConfig + "type_1.ini");
	FloatingObjectConfig*floater_type2 = new FloatingObjectConfig(paths.floaterConfig + "type_2.ini");
	FloatingObjectConfig*floater_ball = new FloatingObjectConfig(paths.floaterConfig + "ball.ini");

	// Impulse lists
	Impulses empty;
	Impulses i1;
	i1.push_back(new Impulse(1e6, 1, 500, 1000));
	i1.push_back(new Impulse(0, 0.1, 1000, 1200));

	Impulses i2;	
	i2.push_back(new Impulse(1.7e6, 0, 10, 5000));

	Impulses i3;
	i3.push_back(new Impulse(1.7e6, 0, 10, 2000));
	i3.push_back(new Impulse(0, 0.5, 2010, 2360));
	i3.push_back(new Impulse(1.7e6, 0, 2360, 2000));
	

	std::vector<FloaterType*> floaterInput;

	// Should be loaded from config file
	if (curScene == SCE_HOUSE_POOL || curScene == SCE_POOL || curScene == SCE_POOL_OBSTACLE){
		floaterInput.push_back(new FloaterType(vec3(300, 150, 0), floater_ball, i1));
	} 
	if (curScene == SCE_LIGHTHOUSE){
		// One ship with impulse and lots of static ships
		floaterInput.push_back(new FloaterType(vec3(2140, 1275, 0), floater_type1, i2));
		// 600 - 1000
		int upper = min(sceneParam, 8);
		for (int i = 0; i < upper; i++){
			floaterInput.push_back(new FloaterType(vec3(600+65*i, 1450, 0), floater_type2, empty));
		}
		// 160 - -360
		int upper2 = max(min(sceneParam - 8, 8), 0);
		for (int i = 0; i < upper2; i++){
			floaterInput.push_back(new FloaterType(vec3(160 - 65 * i, 1450, 0), floater_type2, empty));
		}
		// -170 - 850
		int upper3 = max(min(sceneParam - 16, 8), 0);
		for (int i = 0; i < upper3; i++){
			floaterInput.push_back(new FloaterType(vec3(870-65*i, 1030, 0), floater_type2, empty));
		}
	}
	if (curScene == SCE_OCEAN_SHIPS){
		// More ships with the same impulse
		int nShips = max(min(sceneParam, 32), 0); // 0 .. 32
		for (int i = 0; i < nShips; i++){
			floaterInput.push_back(new FloaterType(vec3(150, i * 250, 0), floater_type1, i3));
		}
	}
	if (curScene == SCE_OCEAN || curScene == SCE_PARTICLE_TEST){
		// Only one ship
		floaterInput.push_back(new FloaterType(vec3(300, 150, 0), floater_type1, i1));
	}

	for (int i = 0; i < floaterInput.size(); i++){
		// Create new floating object from the floating object-template information
		FloatingObject* floater = new FloatingObject(
			floaterInput[i]->config,
			paths.cameraConfig + "OrthoSilhouetteView.ini",
			paths.cameraConfig + "OrthoTopView.ini");
		// Set instance specific attributes
		floater->setModel(gc, floaterInput[i]->config);
		floater->setPosition(floaterInput[i]->position);
		floater->impulses = floaterInput[i]->impulses;

		if (curScene == SCE_LIGHTHOUSE){
			// All the ships are rotated
			floater->rotation.z = 90;
		}
		floater->init(gc);
		
		gc.floaters.push_back(floater);
	}
	gc.activeFloater = gc.floaters[0];

	if (curScene == SCE_LIGHTHOUSE){
		gc.activeFloater->rotation.z = 180;
	}
}


/*!

*/
void WaterSimApplication::prepareStages(){
	// Init resource lists
	// Resources which are not the same for all the stages:
	//	- Floating objects
	//  - Camera
	ResourceList* allFloaters = new ResourceList();
	allFloaters->floaters = gc.floaters;

	ResourceList* mainCamFloaters = new ResourceList();
	mainCamFloaters->floaters = gc.floaters;
	mainCamFloaters->inputCamera = gc.getCamera(GC::CAM_PERSP);

	ResourceList* orthoCamFloaters = new ResourceList();
	orthoCamFloaters->floaters = gc.floaters;
	orthoCamFloaters->inputCamera = gc.getCamera(GC::CAM_ORTHO);
	
	// Init Drawable stages
	waterOptics		= new WaterOptics();
	waterSurface	= new WaterSurface();
	filterParticles = new FilterParticles();
	waveParticles	= new WaveParticleSimulation();
	object2water	= new ObjectToWaterCoupling();
	water2object	= new WaterToObjectCoupling(waterSurface);
	perlin			= new NoiseGenerator();

	setupSceneBoxDW();
	setupTexturePreviewDW();
	setupComputeShaderExample();

	// Add to "scene graph"
	// Components working with floater-specific cameras
	stage.addDrawable("object2water", object2water, allFloaters);
	stage.addDrawable("water2object", water2object, allFloaters);
	// Components working with orthographic cam
	stage.addDrawable("WPsimulation", waveParticles, orthoCamFloaters); // simulation
	stage.addDrawable("WPfilter", filterParticles, orthoCamFloaters);
	// Componenets working with the main cam
	stage.addDrawable("renderScene", getSceneBoxDW(), mainCamFloaters);
	stage.addDrawable("waterOptics", waterOptics, mainCamFloaters);
	stage.addDrawable("displayWP",   waveParticles, mainCamFloaters); // note: registered twice! (different stage information)
	stage.addDrawable("waterSurface", waterSurface, mainCamFloaters);
	// Components not dependend on any cam nor any floaters
	stage.addDrawable("computeExample", getComputeShaderExample());
	stage.addDrawable("displayTexture", getTexturePreviewDW());
}

void WaterSimApplication::prepareScene(){
	std::string path;
	switch (curScene)
	{
	case SCE_OCEAN:
		path = paths.sceneConfig + "ocean.ini";
		break;
	case SCE_OCEAN_SHIPS:
		path = paths.sceneConfig + "ocean.ini";
		break;
	case SCE_LIGHTHOUSE:
		path = paths.sceneConfig + "lighthouse.ini";
		break;
	case SCE_HOUSE_POOL:
		path = paths.sceneConfig + "housepool.ini";
		break;
	case SCE_POOL:
		path = paths.sceneConfig + "pool.ini";
		break;
	case SCE_POOL_OBSTACLE:
		path = paths.sceneConfig + "pool.ini";
		break;
	}
	
	if (curScene != SCE_PARTICLE_TEST){
		SimulationConfig customScene = SimulationConfig(path.c_str());
		gc.simConf->customSceneOverride(customScene);
	}

 	gc.deepWaterColour = gc.simConf->waterColor / 255.0f;
}

void WaterSimApplication::InitGUI(GraphicsContext& gc){
	//WaterSurface* = gc.
	// Water surface related
	Gui.setMinTessDistance(&waterSurface->m_Tess.minDistance);
	Gui.setMaxTessDistance(&waterSurface->m_Tess.maxDistance);
	Gui.setMaxTessLevel(&waterSurface->m_Tess.maxLevel);
	Gui.setMinTessLevel(&waterSurface->m_Tess.minLevel);
	Gui.setFresnelPow(&waterSurface->fresnelPow);
	Gui.setFresnelR0(&waterSurface->fresnelR0);
	Gui.setHorizontalCoef(&waterSurface->horizontalCoef);
	Gui.setVerticalCoef(&waterSurface->verticalCoef);
	Gui.setNoiseScale(&waterSurface->mNoiseScale);
	Gui.setNoiseSpeed(&waterSurface->mNoiseSpeed);
	Gui.setNoiseStrength(&waterSurface->mNoiseStrength);
	Gui.setSunEffectPow(&waterSurface->sunEffect.x);
	Gui.setSunEffectStrength(&waterSurface->sunEffect.y);
	Gui.setTexRayDistortion(&waterSurface->texRayDistortion);
	Gui.setUseWaterHole(&waterSurface->useWaterHole);

	// Wave particles stuff
	Gui.setUseBoundaries(&waveParticles->useBoundaries);
	Gui.setStartupSpeed(&waveParticles->startupSpeed);
	Gui.setStartupHalfSize(&waveParticles->startupHalfSize);
	Gui.setThresholdDisp(&waveParticles->thresholdDisp);
	Gui.setMinDisperAngle(&waveParticles->minDisperAngle);
	Gui.setWaveDampening(&waveParticles->waveDampening);

	// Filtering
	Gui.setFilterKernelSize(&filterParticles->filterKernelSize);
	// Optics
	Gui.setWaterClarity(&waterOptics->waterClarity);
	Gui.setReflectDarkening(&waterOptics->reflectDarkening);
	// Object to Water
	Gui.setWaveDepthReduce(&object2water->waveDepthReduce.x);
	Gui.setWaveDepthReducePow(&object2water->waveDepthReduce.y);

	Gui.setShaderCompileFunc(guiCompileShaders);
	Gui.init(gc);

	// Keyboard
	keyboard.Gui = &Gui;
	keyboard.setShaderCompileFunc(guiCompileShaders);
	keyboard.waveParticles = waveParticles;
}



/// An init callback.
void WaterSimApplication::init() {
	//
	checkRequirements();

	// Simulation config initialization
	SimulationConfig* simConf = new SimulationConfig(defaultSimFile.c_str());
	gc.simConf = simConf;

	if (!inputConfig.empty()){
		SimulationConfig customInput = SimulationConfig(inputConfig.c_str());
		gc.simConf->customSceneOverride(customInput);
	}
	
	gc.simulationSpeed = (1.0f / (float)simConf->simStepsPerSec); // = 1 / simulation steps per second (in float)
	if (curScene == SCE_PARTICLE_TEST && gc.testRun){
		gc.simConf->mWaveParticle.nMaxParticles = sceneParam;
	}

	if (gc.testRun){
		gc.pauseDemonstration = false;
	}
	// === Init lib ===
	ilInit(); // devil

	// === Common ===
	enableState();
	initShaders();
	initCameras();
	initTextures();
	initPrimitives();
	initModels();
	initFloaters();
	prepareStages();
	
	prepareScene();
	
	// init each individual stages
	stage.init(gc); 
	
	// init window
	windowSizeChanged(gc.mainCamera->winDim.x, gc.mainCamera->winDim.y);
	// init gui (after stages)
	InitGUI(gc);

	//--- one time preprocess ---
	perlin->genTilableNoiseCPU(gc);
	//---

	// The very last thing in the init method
	stage.preprocess(gc);
	stage.update(gc);
}


void WaterSimApplication::cleanup(){
	gc.cleanup();
}


void WaterSimApplication::compileShaders()
{
	for(ShaderSet* shaders : gc.shaderSets)
	{
	   shaders->compileShaderProgram();
	}
}

void WaterSimApplication::windowSizeChanged(int width, int height)
{
	stage.winResize(gc, width, height);
}

void WaterSimApplication::keyboardChanged(int key, int scancode, int action, int mods)
{
	keyboard.keyboardChanged(gc, key, scancode, action, mods);
}

void WaterSimApplication::mousePositionChanged(int ix, int iy)
{
	mouse.mousePositionChanged(gc, ix, iy);
}

void WaterSimApplication::mouseButtonChanged(int button, int action)
{
	mouse.mouseButtonChanged(button, action);
}

