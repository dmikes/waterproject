#include "DrawableWrapper.h"

#include "GraphicsContext.h"


void DrawableWrapper::winResize(GraphicsContext& gc, int width, int height )
{
	if(winResizeFunc){
		winResizeFunc(gc, width, height);
	}
}


void DrawableWrapper::init(GraphicsContext& gc)
{
	if(isInitialized) return;
	isInitialized = true;

	if(initFunc){
		initFunc(gc);
	}
}

void DrawableWrapper::update(GraphicsContext& gc)
{
	if(updateFunc){
		updateFunc(gc);
	}
}

void DrawableWrapper::display(GraphicsContext& gc)
{
	if(displayFunc){
		displayFunc(gc);
	}
}

void DrawableWrapper::preprocess(GraphicsContext& gc)
{
	if (preprocessFunc){
		preprocessFunc(gc);
	}
}
