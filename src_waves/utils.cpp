#include "utils.h"

using namespace std;

float uniformRandomInRange(float min, float max) {
    assert(min < max);
    return min + ((double) rand() / (double) RAND_MAX) * (max - min);
}
