#include "GraphicsContext.h"

#include "Texture2D.h"
#include "Model3D.h"
#include "MeshVBO.h"
#include "Transformation3D.h"

GraphicsContext::GraphicsContext() : displayParticles(false), displayTexLOD(0)
{

}


GraphicsContext::~GraphicsContext()
{
	//for (int i = models.size()-1; i > 0; i--)
	//{
	//	delete models[i];
	//}
	delete simConf;
}


void GraphicsContext::addShaderSet(ShaderSel shaderId, ShaderSet* shaders){
	if(curShader == NULL){
		curShader = shaders;
	}
	//shaderSets.push_back(shaders);
	if(shaderSets.size() <= static_cast<uint>(shaderId)){
		shaderSets.resize(shaderId+1);
	}
	shaderSets[shaderId] = shaders;
}


void GraphicsContext::addCamera(CameraSel camId, Camera* camera){
	if(curCamera == NULL){
		curCamera = camera;
	}
	//cameras.push_back(camera);
	if(cameras.size() <= static_cast<uint>(camId)){
		cameras.resize(camId+1);
	}
	cameras[camId] = camera;
}

void GraphicsContext::addTexture(TexSel textureOrdinal, Texture* texture)
{
	if(textures.size() <= static_cast<uint>(textureOrdinal)){
		textures.resize(textureOrdinal+1);
	}
	textures[textureOrdinal] = texture;
}

void GraphicsContext::addLight(LightSel lightId, Light* light)
{
	if(lights.size() <= static_cast<uint>(lightId)){
		lights.resize(lightId+1);
	}
	lights[lightId] = light;
}


void GraphicsContext::addModel(Model3D* model)
{
	models.push_back(model);
}

void GraphicsContext::addFloater(FloaterSel floaterId, FloatingObject* model)
{
	if (floaters.size() <= static_cast<uint>(floaterId)){
		floaters.resize(floaterId + 1);
	}
	floaters[floaterId] = model;
}


void GraphicsContext::addMesh(MeshSel meshId, MeshVBO* mesh)
{
	if(meshes.size() <= static_cast<uint>(meshId)){
		meshes.resize(meshId+1);
	}
	meshes[meshId] = mesh;
}

void GraphicsContext::addTransform( TransformSel transId, Transformation3D* trans )
{
	if(transformations.size() <= static_cast<uint>(transId)){
		transformations.resize(transId+1);
	}
	transformations[transId] = trans;
}


void GraphicsContext::startTimer(std::string label){
	curLabel = label;
	beforeTimer = glfwGetTime();
}

void GraphicsContext::endTimer(){
	//glFinish();
	double afterTime = glfwGetTime();
	printf("timer: %f ms (%s)\n", (afterTime - beforeTimer) * 1e3, curLabel.c_str()); // s to ms
}




ShaderSet* GraphicsContext::getShaders(ShaderSel shaId) const
{
	return shaderSets[shaId];
}

Camera* GraphicsContext::getCamera(CameraSel camId) const
{
	return cameras[camId];
}

Texture* GraphicsContext::getTexture(TexSel texId) const
{
	return textures[texId];
}

Light* GraphicsContext::getLight(LightSel lightId) const
{
	return lights[lightId];
}

FloatingObject* GraphicsContext::getFloater(FloaterSel floatId) const
{
	return floaters[floatId];
}

MeshVBO* GraphicsContext::getMesh(MeshSel meshId) const
{
	return meshes[meshId];
}

Transformation3D* GraphicsContext::getTransform(TransformSel transId) const
{
	return transformations[transId];
}

void GraphicsContext::cleanup()
{
	// cameras
	for(auto it = cameras.begin(); it != cameras.end(); it++){
		delete *it;
	} 
	cameras.clear();

	// shaders
	for(auto it = shaderSets.begin(); it != shaderSets.end(); it++){
		delete *it;
	} 
	cameras.clear();

	// textures
	for(auto it = textures.begin(); it != textures.end(); it++){
		delete *it;
	} 
	textures.clear();

	// lights
	for(auto it = lights.begin(); it != lights.end(); it++){
		delete *it;
	} 
	lights.clear();

	// models
	for(auto it = models.begin(); it != models.end(); it++){
		delete *it;
	} 
	models.clear();

	// staticObjects
	for(auto it = staticObjects.begin(); it != staticObjects.end(); it++){
		delete *it;
	} 
	staticObjects.clear();

	// floaters
	for(auto it = floaters.begin(); it != floaters.end(); it++){
		delete *it;
	} 
	floaters.clear();

	// meshes
	for(auto it = meshes.begin(); it != meshes.end(); it++){
		delete *it;
	} 
	meshes.clear();

	// transformations
	for(auto it = transformations.begin(); it != transformations.end(); it++){
		delete *it;
	} 
	transformations.clear();
}
