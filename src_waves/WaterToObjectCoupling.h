#pragma once
#include "Drawable.h"

#include "WaterSurface.h"
//class WaterSurface;

class WaterToObjectCoupling : public Drawable {
public:
	GLuint FBOobjectRender;
	GLuint RBOobjectDepth;
	GLuint FBOheightfield;
	GLuint RBOheightfield;

	bool floaterInteraction = true;
	int nMaxAdjacentFloater = 5;
	int adjTestFrequency = 10;
	int waterHoleFrequency = 20;
	int buoyancyFrequency = 1;

	WaterSurface* waterSurface;
	WaterToObjectCoupling(WaterSurface* waterSurface);

	virtual void init(GraphicsContext& gc);
	virtual void winResize(GraphicsContext& gc, int width, int height);
	virtual void update(GraphicsContext& gc);
	virtual void display(GraphicsContext& gc);
	virtual void preprocess(GraphicsContext& gc);
	
	float countVolume(GraphicsContext& gc, FloatingObject* floater);
	//void processFloater(GraphicsContext& gc, FloatingObject* floater);
	void readWaterForces(GraphicsContext& gc, FloatingObject* floater);
	void applyShipDeviation(GraphicsContext& gc, FloatingObject* floater);
	void renderHeightTexture(GraphicsContext& gc, FloatingObject* floater);
	void renderBuoyancyTexture(GraphicsContext& gc, FloatingObject* floater);
	void renderDragAndLiftForce(GraphicsContext& gc, FloatingObject* floater);
	void renderWaterHole(GraphicsContext& gc, FloatingObject* floater);
	void processFloaters(GraphicsContext& gc);
	void readWaterForcesGPU(GraphicsContext& gc, FloatingObject* floater);
};