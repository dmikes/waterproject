#include "GUI.h"

#include <assert.h>

#include "myGL.h"
#include "GraphicsContext.h"
#include "Camera.h"
#include "WaterSurface.h"
#include "WaveParticleSimulation.h"
#include "FilterParticles.h"
#include "FloatingObject.h"
#include "WaterOptics.h"
#include "ObjectToWaterCoupling.h"



void TW_CALL setSquareIvec2(const void* value, void* clientData){
	ivec2* v = reinterpret_cast<ivec2*>(clientData);
	v->x = *(reinterpret_cast<const int*>(value));
	v->y = *(reinterpret_cast<const int*>(value));
}

void TW_CALL getSquareIvec2(void* value, void* clientData)
{
	ivec2* v = reinterpret_cast<ivec2*>(clientData);
	*reinterpret_cast<int*>(value) = v->x;
}

void TW_CALL setSquareVec2(const void* value, void* clientData){
	vec2* v = reinterpret_cast<vec2*>(clientData);
	v->x = *(reinterpret_cast<const float*>(value));
	v->y = *(reinterpret_cast<const float*>(value));
}

void TW_CALL getSquareVec2(void* value, void* clientData)
{
	vec2* v = reinterpret_cast<vec2*>(clientData);
	*reinterpret_cast<float*>(value) = v->x;
}

void TW_CALL setParticleTexSize(const void* value, void* clientData){
	int size = *(reinterpret_cast<const int*>(value));
	ivec2 dim = ivec2(size, size);

	FloatingObject* floater = reinterpret_cast<FloatingObject*>(clientData);
	floater->waveParticleCamera->winDim = dim;

	// set texture dimension
	GUI::gc->simConf->texSize = dim;
	floater->particleRender.allocate(dim, GL_RGB32F, GL_RGB, GL_FLOAT);
	floater->particleFilterByX.allocate(dim, GL_RGB32F, GL_RGB, GL_FLOAT);
	floater->particleFiltered.allocate(dim, GL_RGB32F, GL_RGB, GL_FLOAT);
	floater->gradientFilterByX.allocate(dim, GL_RGB32F, GL_RGB, GL_FLOAT);
	floater->gradientFiltered.allocate(dim, GL_RGB32F, GL_RGB, GL_FLOAT);
}

void TW_CALL getParticleTexSize(void* value, void* clientData)
{
	FloatingObject* floater = reinterpret_cast<FloatingObject*>(clientData);
	*reinterpret_cast<int*>(value) = floater->waveParticleCamera->winDim.x;
}

void TW_CALL setParticleCamSize(const void* value, void* clientData){
	float width = *(reinterpret_cast<const float*>(value));
	vec2 size = vec2(width, width);

	FloatingObject* floater = reinterpret_cast<FloatingObject*>(clientData);

	floater->waveParticleCamera->ortho.viewMax = size;
	floater->texO2WHalfsize = floater->waveParticleCamera->ortho.viewMax.x / 2.f;
	floater->waveParticleCamera->updateProjection();
}

void TW_CALL getParticleCamSize(void* value, void* clientData)
{
	FloatingObject* floater = reinterpret_cast<FloatingObject*>(clientData);
	*reinterpret_cast<float*>(value) = floater->waveParticleCamera->ortho.viewMax.x;
}

// =============== GUI class ================
GraphicsContext* GUI::gc = NULL;

GUI::GUI()
{

}


void GUI::init(GraphicsContext& gc){
	// Initialize AntTweakBar GUI
	if (!TwInit(TW_OPENGL, NULL)){
		assert(0);
	}

	controlBar = TwNewBar("Controls");
	TwDefine(" Controls position='0 0' size='270 750' valueswidth=90 refresh=0.1 alpha=190");
	addControlVars(gc);

	motionBar = TwNewBar("Motion");
	TwDefine(" Motion position='1650 0' size='270 500' valueswidth=90 refresh=0.1 alpha=190");
	addMotionVars(gc);
}

void GUI::addControlVars(GraphicsContext& gc){
	GUI::gc = &gc;
	TwAddButton(controlBar, "build", compileShaders, NULL, " group='Shaders' label='build' help='Build shader program.'");
	
	TwAddVarRW(controlBar, "fps", TW_TYPE_INT32, &gc.framesPerSec, " group='Info' label='FPS' readonly='true' ");
	TwAddVarRW(controlBar, "avgFps", TW_TYPE_INT32, &gc.avgFramesPerSec, " group='Info' label='avgFPS' readonly='true' ");
	TwAddVarRW(controlBar, "frameCount", TW_TYPE_INT32, &gc.frameCount, " group='Info' label='frame' readonly='true' ");

	TwAddVarRW(controlBar, "faceCulling", TW_TYPE_BOOLCPP, &gc.faceCulling, " group='Render' label='face culling' ");
	TwAddVarRW(controlBar, "wiremode", TW_TYPE_BOOLCPP, &gc.wireMode, " group='Render' label='wire mode' ");
	TwAddVarRW(controlBar, "texPreview", TW_TYPE_BOOLCPP, &gc.displayTex, " group='Render' label='dispay texture'");
	TwAddVarRW(controlBar, "particleVisible", TW_TYPE_BOOLCPP, &gc.displayParticles, " group='Render' label='dispay particles'");
	TwAddVarRW(controlBar, "texLOD", TW_TYPE_INT32, &gc.displayTexLOD, " group='Render' label='TexLOD' min=-5");

	//TwAddVarRW(controlBar, "tessLevelInner",TW_TYPE_INT32,	&tessLevelInner,	" group='Render' label='tess. inner' min=0");
	//TwAddVarRW(controlBar, "tessLevelOuter",TW_TYPE_INT32,	&tessLevelOuter,	" group='Render' label='tess. outer' min=0");
	TwAddVarRW(controlBar, "speed", TW_TYPE_FLOAT, noiseSpeed, " group='Noise' label='speed' min=0 step=0.0005");
	TwAddVarRW(controlBar, "strength", TW_TYPE_FLOAT, noiseStrength, " group='Noise' label='strength' min=0 step=0.1");
	TwAddVarRW(controlBar, "scale", TW_TYPE_FLOAT, noiseScale, " group='Noise' label='scale' min=1 step=10");

	TwAddVarRW(controlBar, "minTessDistance", TW_TYPE_FLOAT, minTessDistance, " group='Tessellation' label='min distance [wc]' min=0 step=10");
	TwAddVarRW(controlBar, "maxTessDistance", TW_TYPE_FLOAT, maxTessDistance, " group='Tessellation' label='max distance [wc]' min=0 step=10");
	TwAddVarRW(controlBar, "minLevel", TW_TYPE_FLOAT, minTessLevel, " group='Tessellation' label='min level' min=0 step=1");
	TwAddVarRW(controlBar, "maxLevel", TW_TYPE_FLOAT, maxTessLevel, " group='Tessellation' label='max level' min=0 step=1");

	TwAddVarRW(controlBar, "texRayDistortion", TW_TYPE_FLOAT, texRayDistortion, " group='Water Surface' label='Tex Ray Distortion' min=0 step=0.001");
	TwAddVarRW(controlBar, "fresnelR0", TW_TYPE_FLOAT, fresnelR0, " group='Water Surface' label='Fresnel R0' min=0 max=1 step=0.01");
	TwAddVarRW(controlBar, "fresnelPow", TW_TYPE_FLOAT, fresnelPow, " group='Water Surface' label='Fresnel pow' min=0  step=0.1");
	TwAddVarRW(controlBar, "sunEffectPow", TW_TYPE_FLOAT, sunEffectPow, " group='Water Surface' label='Sun effect pow' min=0  step=0.1");
	TwAddVarRW(controlBar, "sunEffect", TW_TYPE_FLOAT, sunEffectStrength, " group='Water Surface' label='Sun effect strength' min=0  step=0.1");
	TwAddVarRW(controlBar, "waterHole", TW_TYPE_BOOLCPP, useWaterHole, " group='Water Surface' label='water hole' ");
	TwAddVarRW(controlBar, "useBoundaries", TW_TYPE_BOOLCPP, useBoundaries, " group='Water Surface' label='boundaries' ");
	TwAddVarRW(controlBar, "waveDepthReduce", TW_TYPE_FLOAT, waveDepthReduce, " group='Water Surface' label='wave reduce base' min=0 step=0.01 ");
	TwAddVarRW(controlBar, "waveDepthReducePow", TW_TYPE_FLOAT, waveDepthReducePow, " group='Water Surface' label='wave reduce pow' min=0 step=0.01 ");


	TwAddVarRW(controlBar, "waterClarity", TW_TYPE_FLOAT, waterClarity, " group='Water Surface' label='water clarity' min=0 step=0.0001");
	TwAddVarRW(controlBar, "reflectDarkening", TW_TYPE_FLOAT, reflectDarkening, " group='Water Surface' label='reflect darkening' min=0 step=0.05");

	TwAddVarRW(controlBar, "Color", TW_TYPE_COLOR3F, &gc.deepWaterColour.x, "colororder=rgba colormode=hls");

	TwAddVarRW(controlBar, "horizontalCoef", TW_TYPE_FLOAT, &horizontalCoef, " group='WP Surface' label='horizontal coef' min=0 step=0.1");
	TwAddVarRW(controlBar, "verticalCoef", TW_TYPE_FLOAT, &verticalCoef, " group='WP Surface' label='vertical coef' min=0 step=0.5");
	TwAddVarCB(controlBar, "wpOrthoCamDim", TW_TYPE_INT32, setParticleTexSize, getParticleTexSize,
		gc.activeFloater, " group='WP Surface' label='wp cam/tex dim [px]' min=2 step=2");
	TwAddVarCB(controlBar, "wpOrthoCamView", TW_TYPE_FLOAT, setParticleCamSize, getParticleCamSize,
		gc.activeFloater, " group='WP Surface' label='wp cam viewport [wc]' min=2 step=1");
	//TwAddVarCB(controlBar, "wpTexDim",			TW_TYPE_INT32, setParticleTexSize, getSquareIvec2,
	//	&gc.textures[TexSel::TEX_PARTICLES_FILTER_XY]->dim, " group='WP Surface' label='wp tex size [px]' min=2 step=2");

	TwAddVarRW(controlBar, "particleGenFreq", TW_TYPE_FLOAT, &gc.motionFreq, " group='WP Motion' label='WP Gen Frequency' step=0.1 min=0");

	TwAddVarRW(controlBar, "nParticles", TW_TYPE_INT32, &gc.activeFloater->particleCounter, " group='WaveParticles' label='nParticles' readonly='true' ");
	TwAddVarRW(controlBar, "particleSpeed", TW_TYPE_FLOAT, startupSpeed, " group='WaveParticles' label='startup speed' min=0 step=0.01");
	TwAddVarRW(controlBar, "particleSize", TW_TYPE_FLOAT, startupHalfSize, " group='WaveParticles' label='startup size [wc]' min=0.1");
	TwAddVarRW(controlBar, "thresholdDisp", TW_TYPE_FLOAT, thresholdDisp, " group='WaveParticles' label='dispersion threshold[wc]' min=0.1");
	TwAddVarRW(controlBar, "particleDisper", TW_TYPE_FLOAT, minDisperAngle, " group='WaveParticles' label='min dispersion [deg]' min=0 max=180");
	TwAddVarRW(controlBar, "waveDampening", TW_TYPE_FLOAT, waveDampening, " group='WaveParticles' label='wave damping' min=0 step=0.000001");
	TwAddVarRW(controlBar, "filterKernelSize", TW_TYPE_UINT32, filterKernelSize, " group='WaveParticles' label='filter kernel size' ");

}

void GUI::addMotionVars(GraphicsContext& gc){
	TwAddVarRW(motionBar, "currentShip", TW_TYPE_INT32, &gc.activeFloater->id, " group='Motion' label='current ship id' readonly='true' ");

	TwAddVarRW(motionBar, "waveDistance", TW_TYPE_FLOAT, &gc.activeFloater->waveDistance, " group='Motion' label='wave distance' min=0 step=0.01");
	TwAddVarRW(motionBar, "swayReduce", TW_TYPE_FLOAT, &gc.activeFloater->swayReduce, " group='Motion' label='sway reduce' min=0 step=0.01");
	TwAddVarRW(motionBar, "shipFloatSpeed", TW_TYPE_FLOAT, &gc.activeFloater->floatSpeed, " group='Motion' label='ship speed' min=0 step=10000");
	TwAddVarRW(motionBar, "shipDensity", TW_TYPE_FLOAT, &gc.activeFloater->density, " group='Motion' label='ship density [g/cm3]' min=0.05 step=0.01");
	TwAddVarRW(motionBar, "cumulCoef", TW_TYPE_FLOAT, &gc.activeFloater->volumeCumulationCoef, " group='Motion' label='cumulation coef' min=0.001 step=0.0001");
	TwAddVarRW(motionBar, "triangleCoef", TW_TYPE_FLOAT, &gc.activeFloater->meshProjectCoef, " group='Motion' label='triangle proj. coef' min=0 max=1 step=0.01");
	TwAddVarRW(motionBar, "dragCoef", TW_TYPE_FLOAT, &gc.activeFloater->dragCoef, " group='Motion' label='drag coef' min=0 step=0.01");
	TwAddVarRW(motionBar, "liftCoef", TW_TYPE_FLOAT, &gc.activeFloater->liftCoef, " group='Motion' label='lift coef' min=0 step=0.01");
	TwAddVarRW(motionBar, "rollSpeed", TW_TYPE_FLOAT, &gc.activeFloater->rotSpeedVec.x, " group='Motion' label='roll speed' min=0 step=0.01");
	TwAddVarRW(motionBar, "rollMax", TW_TYPE_FLOAT, &gc.activeFloater->rollMax, " group='Motion' label='roll max' min=0 step=0.10");
	TwAddVarRW(motionBar, "rollRestore", TW_TYPE_FLOAT, &gc.activeFloater->rollRestore, " group='Motion' label='roll restore' min=0 max=1 step=0.01");
	TwAddVarRW(motionBar, "pitchSpeed", TW_TYPE_FLOAT, &gc.activeFloater->rotSpeedVec.y, " group='Motion' label='pitch speed' min=0 step=0.01");
	TwAddVarRW(motionBar, "pitchMax", TW_TYPE_FLOAT, &gc.activeFloater->pitchMax, " group='Motion' label='pitch max' min=0 step=1.00");
	TwAddVarRW(motionBar, "pitchMaxVelocity", TW_TYPE_FLOAT, &gc.activeFloater->pitchMaxSpeed, " group='Motion' label='pitch max velocity' min=0 step=1.00");
	TwAddVarRW(motionBar, "yawSpeed", TW_TYPE_FLOAT, &gc.activeFloater->rotSpeedVec.z, " group='Motion' label='yaw speed' min=0 step=0.01");
	TwAddVarRW(motionBar, "maxWaveSlopeX", TW_TYPE_FLOAT, &gc.activeFloater->maxWaveSlope.x, " group='Motion' label='max wave slope x' min=0 step=50");
	TwAddVarRW(motionBar, "maxWaveSlopeY", TW_TYPE_FLOAT, &gc.activeFloater->maxWaveSlope.y, " group='Motion' label='max wave slope y' min=0 step=50");
	TwAddVarRW(motionBar, "rotationSpeed", TW_TYPE_FLOAT, &gc.activeFloater->rotationSpeed, " group='Motion' label='wave rotat. speed' min=0 step=0.005");
	TwAddVarRW(motionBar, "waveForceStrength", TW_TYPE_FLOAT, &gc.activeFloater->waveStrength, " group='Motion' label='wave force strength' min=0 step=0.01");
}

void GUI::renewGUI(GraphicsContext& gc){
	TwRemoveAllVars(controlBar);
	TwRemoveAllVars(motionBar);
	addControlVars(gc);
	addMotionVars(gc);
}


void GUI::setMinTessDistance(float* value) { minTessDistance = value; }
void GUI::setMaxTessDistance(float* value) { maxTessDistance = value; }
void GUI::setMinTessLevel(float* value) { minTessLevel = value; }
void GUI::setMaxTessLevel(float* value) { maxTessLevel = value; }
void GUI::setNoiseSpeed(float* value) { noiseSpeed = value; }
void GUI::setNoiseStrength(float* value) { noiseStrength = value; }
void GUI::setNoiseScale(float* value) { noiseScale = value; }
void GUI::setTexRayDistortion(float* value) { texRayDistortion = value; }
void GUI::setHorizontalCoef(float* value) { horizontalCoef = value; }
void GUI::setVerticalCoef(float* value) { verticalCoef = value; }
void GUI::setFresnelR0(float* value) { fresnelR0 = value; }
void GUI::setFresnelPow(float* value) { fresnelPow = value; }
void GUI::setSunEffectPow(float* value) { sunEffectPow = value; }
void GUI::setSunEffectStrength(float* value) { sunEffectStrength = value; }
void GUI::setUseWaterHole(bool* value) { useWaterHole = value; }
void GUI::setUseBoundaries(bool* value) { useBoundaries = value; }
void GUI::setStartupSpeed(float* value) { startupSpeed = value; }
void GUI::setStartupHalfSize(float* value) { startupHalfSize = value; }
void GUI::setThresholdDisp(float* value) { thresholdDisp = value; }
void GUI::setMinDisperAngle(float* value) { minDisperAngle = value; }
void GUI::setWaveDampening(float* value) { waveDampening = value; }
void GUI::setFilterKernelSize(uint* value) { filterKernelSize = value; }
void GUI::setWaterClarity(float* value) { waterClarity = value; }
void GUI::setReflectDarkening(float* value) { reflectDarkening = value; }
void GUI::setWaveDepthReduce(float* value) { waveDepthReduce = value; }
void GUI::setWaveDepthReducePow(float* value) { waveDepthReducePow = value; }


void GUI::setShaderCompileFunc(void (TW_CALL *func)(void *clientData))
{
	compileShaders = func;
}
