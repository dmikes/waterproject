#include "WaterSurface.h"
#include "Texture2D.h"
#include "ResourceList.h"
#include "FloatingObject.h"
#include "Model3D.h"

typedef GraphicsContext GC;

void WaterSurface::init(GraphicsContext& gc){
	if(isInitialized) return;
	isInitialized = true;

	m_Tess.minDistance = gc.simConf->mTess.minTessDistance;
	m_Tess.maxDistance = gc.simConf->mTess.maxTessDistance;
	m_Tess.minLevel	= static_cast<float>(gc.simConf->mTess.minTessLevel);
	m_Tess.maxLevel	= static_cast<float>(gc.simConf->mTess.maxTessLevel);

	horizontalCoef = gc.simConf->m_Surface.horizontalCoef;
	verticalCoef   = gc.simConf->m_Surface.verticalCoef;

	useWaterHole = gc.simConf->useWaterHole;

	mNoiseSpeed = gc.simConf->mNoise.speed;
	mNoiseStrength = gc.simConf->mNoise.strength;
	mNoiseScale = gc.simConf->mNoise.scale;

	ivec2 nVertices = gc.simConf->gridVertexCount;
	vec2 gridStep = gc.simConf->waterSize / vec2(nVertices.x - 1, nVertices.y - 1);

	std::vector<vec3> vertices;
	std::vector<vec3> colors;

	float xpos = gc.simConf->waterOrigin.x;
	float ypos = gc.simConf->waterOrigin.y;
	for(int y = 0; y < nVertices.y; ++y)
	{
		xpos = gc.simConf->waterOrigin.x;
		for (int x = 0; x < nVertices.x; ++x)
		{
			vertices.push_back(vec3(xpos, ypos, 0));
			xpos += gridStep.x;
		}
		ypos += gridStep.y;
	}


	/* Indices are created as follows
		y ^		 .
		  |		 .
		  3------2 . .
		  |      |
		  |      |
	[0,0] 0------1---->  x (world coord) */
	std::vector<GLushort> indices;
	for (int y = 0; y < nVertices.y-1; ++y){
		for (int x = 0; x < nVertices.x-1; ++x) {
			GLushort i = x + y * nVertices.x;
			// ccw order
			indices.push_back(i);
			indices.push_back(i + 1);
			indices.push_back(i + 1 + nVertices.x);
			indices.push_back(i     + nVertices.x);	
		}
	}
	
	water = new MeshVBO();
	water->setIndices(indices.size(), &indices[0], MeshVBO::VBO_USHORT);
	water->setVertices(vertices.size(), &vertices[0][0]);
	water->init();
}


void WaterSurface::update(GraphicsContext& gc){
	if (!gc.pauseAnimation){
		mNoiseTime += mNoiseSpeed;
	}
}


void WaterSurface::display(GraphicsContext& gc){
	if (gc.curCamera != gc.curResources->inputCamera){
		gc.curCamera = gc.windowCamera;
		gc.curCamera->updateCameraViewMatrix();
		glViewport(0, 0, gc.curCamera->winDim.x, gc.curCamera->winDim.y);
	}
	glPolygonMode(GL_FRONT_AND_BACK, gc.wireMode ? GL_LINE : GL_FILL);

	if (gc.curShader != gc.getShaders(GC::SHA_WAVE_SURFACE)){
		gc.curShader = gc.getShaders(GC::SHA_WAVE_SURFACE);
		glUseProgram(gc.curShader->programId);
	}
	gc.applyProjectionMatrix();
	
	// ==== Generic surface related ====
	//gc.getTexture(TexSel::TEX_FRESNEL_LOOKUP)->bind(3);
	Texture* texLocRefl = gc.getTexture(GC::TEX_LOCAL_REFLECTION);
	Texture* texLocRefr = gc.getTexture(GC::TEX_LOCAL_REFRACTION);
	Texture* texNoise3d = gc.getTexture(GC::TEX_PERLIN_NOISE_3D);
	Texture* skybox = gc.getTexture(GC::TEX_SKYBOX);
	texLocRefl->bind(1);
	texLocRefr->bind(2);
	texNoise3d->bind(3);
	skybox->bind(4);
	
	glUniform2iv(gc.getUniformLocation("u_WinDim"), 1, &gc.curCamera->winDim.x);
	glUniform3fv(gc.getUniformLocation("u_CameraPos"), 1, &gc.curCamera->position.x);
	glUniform3fv(gc.getUniformLocation("u_LightPos"), 1, &gc.getLight(GC::LIG_MAIN)->position.x);
	// 3D noise
	glUniform1i(gc.getUniformLocation("u_Noise3D"), texNoise3d->textureUnit);
	glUniform1f(gc.getUniformLocation("u_NoiseTime"), mNoiseTime);
	glUniform1f(gc.getUniformLocation("u_NoiseStrength"), mNoiseStrength);
	glUniform1f(gc.getUniformLocation("u_NoiseScale"), 1.0f / mNoiseScale);
	// Skybox
	glUniform1i(gc.getUniformLocation("u_ReflectionMap"), skybox->textureUnit);
	// Refraction/Reflection 
	//glUniform1i(gc.getUniformLocation("u_FresnelLookup"), gc.textures[TexSel::TEX_FRESNEL_LOOKUP]->textureUnit);
	glUniform1i(gc.getUniformLocation("u_LocalReflection"), texLocRefl->textureUnit);
	glUniform1i(gc.getUniformLocation("u_LocalRefraction"), texLocRefr->textureUnit);

	glUniform1f(gc.getUniformLocation("u_TexRayDistortion"), texRayDistortion);
	glUniform1f(gc.getUniformLocation("u_FresnelR0"), fresnelR0);
	glUniform1f(gc.getUniformLocation("u_FresnelPow"), fresnelPow);
	glUniform2fv(gc.getUniformLocation("u_SunEffect"), 1, &sunEffect.x);

	
	// Wave particles
	glUniform1f(gc.getUniformLocation("u_WP_Horizontal"), horizontalCoef);
	glUniform1f(gc.getUniformLocation("u_WP_Vertical"), verticalCoef);	
	// Tesselation
	glUniform4fv(gc.getUniformLocation("u_TessLimits"), 1, &m_Tess.minDistance); // minDistance is the first
	glUniform1i(gc.getUniformLocation("u_UseAngleCorr"), true);


	// === Instanced: Floater related ===
	int size = gc.curResources->floaters.size();
	// Fixed number of ships
	static std::vector<int> wpSamplers(size);
	static std::vector<vec2> wpScale_Shift(2 * size);
	
	static std::vector<int> waterHoleSamplers(size);
	static std::vector<mat4> waterHoleTransform(size);
	//for (FloatingObject* floater : gc.curResources->floaters){
	for (int i = 0; i < size; i++){
		FloatingObject* floater = gc.curResources->floaters[i];
		// Wave Particles
		wpScale_Shift[2 * i] = 1.f / (floater->waveParticleCamera->ortho.viewMax); // World to Texture coordinates conversion
		wpScale_Shift[2 * i + 1] = wpScale_Shift[2 * i] * static_cast<vec2>(floater->waveParticleCamera->getPosition()); // camPos * WPTexScale (MAD shader instruction optimization)
		
		floater->particleFiltered.bind(5 + 2 * i);
		wpSamplers[i] = floater->particleFiltered.textureUnit;
		
		// Water holes
		floater->texWaterHole.bind(6 + 2 * i);
		waterHoleSamplers[i] = floater->texWaterHole.textureUnit;

		waterHoleTransform[i] = floater->waterTexTransform;
	}
	
	glUniform1i(gc.getUniformLocation("u_nFloaters"), size);

	glUniform1iv(gc.getUniformLocation("u_WaveParticleTex"), size, &wpSamplers[0]);
	glUniform4fv(gc.getUniformLocation("u_WPTexScale_Shift"), size, &wpScale_Shift[0].x);

	glUniform1i(gc.getUniformLocation("u_UseWaterHole"), useWaterHole);
	glUniform1iv(gc.getUniformLocation("u_WaterHole"), size, &waterHoleSamplers[0]); 
	glUniformMatrix4fv(gc.getUniformLocation("u_WaterHoleMatrix"), size, GL_FALSE, &waterHoleTransform[0][0][0]);
	oglCheckError("WS: after vars");

	//glUniform2fv(gc.getUniformLocation("u_WPTexScale"), 1, &WPTexScale.x);
	//glUniform2fv(gc.getUniformLocation("u_WPTexShift"), 1, &WPTexShift.x);
	//glUniform1i(gc.getUniformLocation("u_WaveParticleTex"), floater->filtered.textureUnit); // can be instanced
	////glUniform1i(gc.getUniformLocation("u_GradientTexture"), floater->gradientFiltered.textureUnit); // can be instanced

	//glUniform1i(gc.getUniformLocation("u_WaterHole"), floater->texWaterInteraction.textureUnit); // can be instanced
	//glUniformMatrix4fv(gc.getUniformLocation("u_WaterHoleMatrix"), 1, GL_FALSE, &floater->waterTexTransform[0][0]);
	
	
	// === Display ===
	gc.applyModelViewMatrix();
	if(gc.wireMode){
		glUniform3fv(gc.getUniformLocation("u_Color"), 1, &GREY.r);
		glUniform1i(gc.getUniformLocation("u_Mode"), 2);
	} else {
		glUniform1i(gc.getUniformLocation("u_Mode"), 1); 
	}
	
	oglCheckError("WS: before draw");
	glPatchParameteri(GL_PATCH_VERTICES, 4); // TODO: every frame?
	//water->draw_indexed(GL_QUADS);
	water->draw_indexed(GL_PATCHES);
}

void WaterSurface::winResize( GraphicsContext& gc, int width, int height )
{

}

void WaterSurface::preprocess(GraphicsContext& gc)
{

}


