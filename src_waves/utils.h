#ifndef __UTILS_H__
#define __UTILS_H__

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <assert.h>

#define M_PI 3.14159265358979323846f

typedef unsigned char byte;
typedef unsigned short ushort;
typedef unsigned int uint;

float uniformRandomInRange(float min, float max);


enum FixedPipelineShaderMode {
	UNIFORM_COLOR = 1,
	SHADED_COLOR = 2,
	TEXTURE_COLOR = 3,
	CUBEMAP = 4,
	CUBEMAP_RENDERED = 5
};


#endif