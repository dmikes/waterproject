#ifndef __MYGL_H__
#define __MYGL_H__

#include "../libs/glew-1.11.0/include/GL/glew.h"
#include "../libs/glfw-3.0.4.bin.WIN32/include/GLFW/glfw3.h"
#include "../libs/AntTweakBar/include/AntTweakBar.h"


#define oglCheckError(s)
#ifndef oglCheckError
void oglCheckError(std::string message);
#endif

#endif