#ifndef __DRAWABLE_H__
#define __DRAWABLE_H__

// forward declaration - not to cause cyclic includes
class GraphicsContext;
#include "utils.h"

class Drawable {
public:
	
	bool isInitialized = false;
	virtual void init(GraphicsContext& gc){
		isInitialized = true;
	}
	virtual void winResize(GraphicsContext& gc, int width, int height) = 0;
	virtual void update(GraphicsContext& gc) = 0;
	virtual void display(GraphicsContext& gc) = 0;
	virtual void preprocess(GraphicsContext& gc) = 0;
};


#endif