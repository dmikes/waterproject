#ifndef __DATALOADER_H__
#define	__DATALOADER_H__

#include "assimp.h"

#include "devil.h"

#include <stdio.h>
#include <vector>
#include "myGL.h"
#include "glm.h"
#include "utils.h"

class Texture;

class ModelLoader {
	typedef unsigned int uint;

	static void updateBB(glm::vec3& vert, glm::vec3& max, glm::vec3& min);
public:	
	template<typename IndexInt> 
	static void importModel(const std::string &file, 
		std::vector<IndexInt>& outIndices, 
		std::vector<vec3>& outVertices, 
		std::vector<vec3>& outNormals, 
		std::vector<vec2>& outTexCoords, 
		std::vector<std::string>& outTexNames, 
		std::vector<GLuint>& outMeshVertixCount,
		glm::vec3& offset, glm::vec3& minBB, glm::vec3& maxBB);


	static bool importTextures(const std::string path, std::vector<std::string> textureNames, std::vector<Texture*>& outTextures);
	static int countIndices(const std::string &file);
	static bool fileExists(const std::string& name);
};

template<typename IndexInt>
void ModelLoader::importModel(const std::string &file,
	std::vector<IndexInt>& outIndices,
	std::vector<vec3>& outVertices,
	std::vector<vec3>& outNormals,
	std::vector<vec2>& outTexCoords,
	std::vector<std::string>& outTexNames,
	std::vector<GLuint>& outMeshVertixCount,
	glm::vec3& offset, glm::vec3& minBB, glm::vec3& maxBB) 
{
	Assimp::Importer importer;

	//const aiScene* scene = importer.ReadFile(file,
	//			(aiProcessPreset_TargetRealtime_Quality
	//			& ~aiProcess_FindDegenerates)
	//			& ~aiProcess_GenSmoothNormals
	//			| aiProcess_GenNormals
	//			| aiProcess_OptimizeMeshes
	//			| aiProcess_OptimizeGraph);

	importer.SetPropertyFloat(AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE, 45.f);
	const aiScene* scene = importer.ReadFile(file,
		aiProcess_ImproveCacheLocality |
		aiProcess_Triangulate |
		aiProcess_SortByPType |
		aiProcess_FindInvalidData |
		aiProcess_GenSmoothNormals |
		aiProcess_OptimizeMeshes |
		aiProcess_OptimizeGraph |
		0);

	if (scene == NULL) {
		printf("ModelLoader: cannot load file: %s\n", importer.GetErrorString());
		throw importer.GetErrorString();
	}

	int faces = 0;
	minBB = glm::vec3(scene->mMeshes[0]->mVertices[0].x, scene->mMeshes[0]->mVertices[0].y, scene->mMeshes[0]->mVertices[0].z);
	maxBB = minBB;

	//printf("num of materials: %d\n", scene->mNumMaterials);
	// --- Materials ---
	for (unsigned int m = 0; m < scene->mNumMaterials; ++m)
	{
		int texIndex = 0;
		aiString path;  // filename
		aiReturn texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
		while (texFound == AI_SUCCESS) {
			outTexNames.push_back(path.data);
			texIndex++;
			texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
		}
	}

	// --- Meshes ---
	for (uint m = 0; m < scene->mNumMeshes; ++m) {
		const aiMesh* mesh = scene->mMeshes[m];
		outIndices.reserve(outIndices.size() + mesh->mNumFaces * 3);
		outMeshVertixCount.push_back(mesh->mNumVertices);

		int indOffset = outVertices.size();
		for (uint i = 0; i < mesh->mNumFaces; ++i) {
			const aiFace &face = mesh->mFaces[i];
			assert(face.mNumIndices == 3);
			outIndices.push_back(indOffset + face.mIndices[0]);
			outIndices.push_back(indOffset + face.mIndices[1]);
			outIndices.push_back(indOffset + face.mIndices[2]);
		}

		uint nVertices = outVertices.size() + mesh->mNumVertices;
		outVertices.reserve(nVertices);
		outNormals.reserve(nVertices);
		outTexCoords.reserve(nVertices);

		for (uint i = 0; i < mesh->mNumVertices; ++i) {
			if (mesh->HasPositions()) {
				vec3 pos = glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
				updateBB(pos, maxBB, minBB);
				outVertices.push_back(pos);
			}

			if (mesh->HasNormals()) {
				outNormals.push_back(glm::vec3(
					mesh->mNormals[i].x,
					mesh->mNormals[i].y,
					mesh->mNormals[i].z
				));
			}

			if (mesh->HasTextureCoords(0)){
				outTexCoords.push_back(glm::vec2(
					mesh->mTextureCoords[0][i].x,
					mesh->mTextureCoords[0][i].y
				));
			}
		}
		faces += mesh->mNumFaces;
	}


	vec3 center = minBB + ((maxBB - minBB) / 2.f);
	for (vec3& vertex : outVertices){
		vertex -= center;
	}
	minBB -= center;
	maxBB -= center;

	offset = center;
	printf("ModelLoader: success: %s. %d faces, %d vertices, %d indices.\n", file.c_str(), faces, outVertices.size(), outIndices.size());
}


#endif
