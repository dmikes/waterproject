#ifndef __FLOATING_OBJECT_H__
#define __FLOATING_OBJECT_H__

#include <vector>
#include "glm.h"
#include "Texture2D.h"
#include "Transformation3D.h"
class Model3D;
class GraphicsContext;
class Camera;

// --- Impulse ---
struct Impulse{
	float force;
	float turn;
	int from;
	int to;
	Impulse(float f, float t, int from, int to) : force(f), turn(t), from(from), to(to)
	{}
};

typedef std::vector<Impulse*> Impulses;

// --- Force ---
class Force {
public:
	Force(vec3 dir, float mag) : direction(dir), magnitude(mag) 
	{}

	vec3 direction;
	//vec3 atPoint;
	float magnitude;
};

struct FloaterType {
	vec3 position;
	FloatingObjectConfig* config;
	Impulses impulses;

	FloaterType(vec3 p, FloatingObjectConfig* c, Impulses i) : position(p), config(c), impulses(i)
	{}
};

// --- Floating Object ---
class FloatingObject {
public:
	FloatingObject();
	//FloatingObject(CameraConfig* silhouetteCamConfig, CameraConfig* waveParticleCamConfig);
	FloatingObject(GraphicsContext& gc, FloatingObjectConfig* config);
	FloatingObject(std::string silhouetteCamConfig, std::string waveParticleCamConfig);
	FloatingObject(FloatingObjectConfig* config, std::string silhouetteCamConfig, std::string waveParticleCamConfig);
	~FloatingObject();

	//static float volumeCoef;
	static float waterRho;
	static int instanceCount;

	int id;
	Model3D* objNormal;
	Model3D* objConvex;
	Impulses impulses;
	int curImpuls = 0;
	

	// Spatial
	vec3 position;
	vec3 underwaterPos; // position for volume counting
	vec3 front = vec3(1, 0, 0);
	//float mass;			// g
	float density = 1.0f;	// g/cm^3
	float volume = 0.f;		// cm^3
	float volumeInWater = 0.f;// cm^3
	float waveDistance = 1.5f;

	// Forces
	vec3 dragForce;
	vec3 liftForce;
	vec3 moveImpuls;
	vec3 waveImpuls;
	vec3 forces;
	float swayReduce = 10.0;
	float waveStrength = 0.25f;

	// Dynamic
	bool movesForward = true;
	float speed = 0.0f; // dual to velocity (speedup)
	vec3 direction; // dual to velocity (speedup)
	vec3 velocity;  // velocity = speed * direction
	vec3 acceleration;
	float floatSpeed = 1.3e6;	// force addition
	float moveStep	= 1.f;		// displacement addition (used only for lifting up and down)

	float dragCoef = 0.4f;
	float liftCoef = 0.2f;
	float meshProjectCoef = 0.5f;
	float volumeCumulationCoef = 0.005f;

	// Rotation
	float prevRotX = 0;
	vec3 rotation;
	vec3 rotSpeedVec = vec3(1.f, 1.f, 0.5f); // speed (step) of roll, pitch, yaw
	float rollRestore = 0.97f;
	float rollMax = 10.f;
	float pitchMax = 0.3f;
	float pitchMaxSpeed = 37.3f;
	// Rotation by wave
	vec3 rotationSurface;
	vec2 maxWaveSlope = vec2(4000.f, 4000.f);
	float rotationSpeed = 0.025f;

	// Wave particles
	float cumulativeWaveTime = 0.f;
	float cumulativeWaveMotion = 0.f;
	bool createParticles = false;

	float activeDistance;
	std::vector<FloatingObject*> adjacent;

	Camera* waveParticleCamera;
	Texture particleRender;
	Texture particleFilterByX;
	Texture particleFiltered;
	Texture gradientFilterByX;
	Texture gradientFiltered;


	Camera* silhouetteCamera;
	float texW2OOverflow = 2.f;	// "pixels" (float type - recalculated into world coordinates later)
	float texW2OHalfsize;		// Square
	Texture texBuoyancy;
	Texture texWaterHole;
	Texture texWaterForces;
	Texture texLiftForces;
	Texture texHeightfield;
	mat4 waterTexTransform;
	
	// These five textures have the same resolution
	float texO2WHalfsize;		
	Texture texObjectSilhouette;
	Texture texWaveEffect;
	Texture texSilhouetteEdge;
	Texture texTemp;
	Texture texFinalWaveParticle;
	mat4 particleGenTexTransform;

	// === Particles ===
	GLint particleCounter = 0;
	uint queryTF;
	GLuint particleVAOs[2];
	GLuint particleVBOs[2];
	GLuint particleTFBs[2];
	bool isFirstIteration = true;
	
	//GLuint nMaxParticleBound;

	enum Move {
		FORWARD,
		BACK,
		LEFT,
		RIGHT,
		UP,
		DOWN
	};

	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void move(GraphicsContext& gc, Move dir);
	void setPosition(vec3 position);
	void addPosition(vec3 off);
	void setVolume(float vol);
	void updateMovement();
	float getMass();
	void particlesGenerated(GraphicsContext& gc);
	void setModel(Model3D* model, Model3D* modelConvex);
	void setModel(GraphicsContext& gc, FloatingObjectConfig* config);
	void reset(FloatingObjectConfig* config);
	// staticky pole identifikatoru bufferu
	//MeshVBO vertexPixelGrid;
private:
	bool hasMoved = true;

};


#endif