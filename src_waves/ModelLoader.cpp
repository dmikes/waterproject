#include "ModelLoader.h"

#include "Texture2D.h"


void ModelLoader::updateBB(glm::vec3& vert, glm::vec3& max, glm::vec3& min) {
	max = glm::max(max, vert);
	min = glm::min(min, vert);
}


bool ModelLoader::fileExists(const std::string& name) {
	if (FILE *file = fopen(name.c_str(), "r")) {
		fclose(file);
		return true;
	}
	else {
		return false;
	}
}

int ModelLoader::countIndices(const std::string &file)
{

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(file, 0);

	if (scene == NULL) {
		printf("ModelLoader: cannot load file: %s\n", importer.GetErrorString());
		throw importer.GetErrorString();
	}

	int nIndices = 0;
	for (uint m = 0; m < scene->mNumMeshes; ++m) {
		const aiMesh* mesh = scene->mMeshes[m];
		nIndices += 3 * mesh->mNumFaces;
	}
	
	return nIndices;
}

bool ModelLoader::importTextures(const std::string path, std::vector<std::string> textureNames, std::vector<Texture*>& outTextures)
{
	uint nTex = textureNames.size();
	if (nTex == 0){
		return false;
	}

	std::vector<uint>imageIds(nTex);
	outTextures.resize(nTex);
		
	// generate an image name
	ilGenImages(nTex, &imageIds[0]);

	for (int i = 0; i < nTex; i++){
		// bind it
		ilBindImage(imageIds[i]);
		// match image origin to OpenGL's
		ilEnable(IL_ORIGIN_SET);
		ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
		// load  the image
		std::string filename = path + textureNames[i];
		printf("tex path: %s\n", filename.c_str());
		ILboolean success = ilLoadImage((ILstring)filename.c_str());

		// Convert image to RGBA with unsigned byte data type
		ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
		

		if (success){
			outTextures[i] = new Texture();
			outTextures[i]->init(GL_TEXTURE_2D, GL_LINEAR, GL_REPEAT);
			outTextures[i]->bind(0);
		
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
				ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
				0, GL_RGBA, GL_UNSIGNED_BYTE, ilGetData());
		} else {
			printf("ModelLoader: cannot load texture %s.\n", filename.c_str());
		}
	}
	
	// check to see if everything went OK
	//if (!success) {
		//ilDeleteImages(nTex, &imageID);
		//return 0;
	//}
	return true;
}

