#include "WaterToObjectCoupling.h"

#include "myGL.h"
#include "Model3D.h"
#include "FloatingObject.h"
#include "GraphicsContext.h"
#include "ResourceList.h"
#include "WaterSurface.h"

typedef GraphicsContext GC;

WaterToObjectCoupling::WaterToObjectCoupling(WaterSurface* waterSurface)
	: waterSurface(waterSurface)
{
}

void WaterToObjectCoupling::init(GraphicsContext& gc)
{
	glGenFramebuffers(1, &FBOobjectRender);
	assert(FBOobjectRender > 0);
	glBindFramebuffer(GL_FRAMEBUFFER, FBOobjectRender);
	//glFramebufferTexture2D will be set in draw

	glGenRenderbuffers(1, &RBOobjectDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, RBOobjectDepth);
	//glRenderbufferStorage will be set in draw
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, RBOobjectDepth);
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	glGenFramebuffers(1, &FBOheightfield);
	assert(FBOheightfield > 0);
	glBindFramebuffer(GL_FRAMEBUFFER, FBOheightfield);
	//glFramebufferTexture2D will be set in draw

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void WaterToObjectCoupling::winResize(GraphicsContext& gc, int width, int height)
{

}

void WaterToObjectCoupling::update(GraphicsContext& gc)
{

}

void WaterToObjectCoupling::preprocess(GraphicsContext& gc){
	if (gc.curShader != gc.getShaders(GC::SHA_WATER_TO_OBJECT)){
		gc.curShader = gc.getShaders(GC::SHA_WATER_TO_OBJECT); 
		glUseProgram(gc.curShader->programId);
	}
	
	for (FloatingObject* floater : gc.curResources->floaters){
		//floater->update(gc); // set camera for the first time
		floater->underwaterPos = floater->position;
		float minShift = floater->objConvex->worldBB.mMax.z - floater->underwaterPos.z;
		// Sink it under water 
		floater->addPosition(vec3(0, 0, -minShift));
	}
	// Compute volume under water
	processFloaters(gc);

	gc.curShader = gc.getShaders(GC::SHA_REDUCTION4);
	glUseProgram(gc.curShader->programId);
	for (FloatingObject* floater : gc.curResources->floaters){	
		floater->setVolume(countVolume(gc, floater));
		// Return it to the previous position
		floater->setPosition(floater->underwaterPos);
	}
}

void WaterToObjectCoupling::renderHeightTexture(GraphicsContext& gc, FloatingObject* floater){
	static GLenum colorAttachments[1] = { GL_COLOR_ATTACHMENT0 };
	
	// Render waves beyond the ship
	glUniform1i(gc.getUniformLocation("u_Step"), 4);
	
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texHeightfield.texID, 0);

	glDrawBuffers(1, colorAttachments);
	glViewport(0, 0, floater->texHeightfield.width, floater->texHeightfield.height);
	glClear(GL_COLOR_BUFFER_BIT);

	// camera set to be aligned with the default plane (for texture render) only viewport has to be set
	gc.curCamera = gc.getCamera(GC::CAM_PLANE);
	gc.applyProjectionMatrix();
	gc.applyModelViewMatrix();

	Texture* wpSurfTex = &floater->particleFiltered;
	Texture* wpPointTex = &floater->particleRender; // same dimension as the above tex
	Texture* noiseTex = gc.getTexture(GC::TEX_PERLIN_NOISE_3D);
	wpSurfTex->bind(0);
	//wpPointTex->bind(1);
	noiseTex->bind(2);

	// This is from Texture to Texture so there is no world coordinate adjust. 
	vec2 WPTexScale = static_cast<vec2>(floater->texHeightfield.getDim2()) / static_cast<vec2>(wpSurfTex->getDim2());
	vec2 WPTexShift = (1.f - WPTexScale) / 2.f;

	vec3 noiseTexScale = vec3(static_cast<vec2>(floater->texHeightfield.getDim2()) / static_cast<vec2>(noiseTex->getDim2()),
		1.f / waterSurface->mNoiseScale);

	vec2 silCamPos = static_cast<vec2>(floater->silhouetteCamera->getPosition());
	vec2 silCamView = floater->silhouetteCamera->ortho.viewMax;

	if (floaterInteraction){
		int size = glm::min(static_cast<int>(floater->adjacent.size()), nMaxAdjacentFloater);
		if (size > 0){
			std::vector<vec2> scale_shift(2 * size);
			std::vector<int> samplersSurf(size);
			std::vector<int> samplersPoint(size);
			for (int i = 0; i < size; i++){
				FloatingObject* other = floater->adjacent[i];

				// IMPORTANT: non colliding binding points (with the previous textures)
				other->particleFiltered.bind(3 + 2 * i);
				samplersSurf[i] = other->particleFiltered.textureUnit;

				other->particleRender.bind(4 + 2 * i);
				samplersPoint[i] = other->particleRender.textureUnit;

				scale_shift[2 * i] = static_cast<vec2>(floater->texHeightfield.getDim2()) / static_cast<vec2>(other->particleFiltered.getDim2());
				scale_shift[2 * i + 1] = static_cast<vec2>(floater->silhouetteCamera->position - other->waveParticleCamera->position) /
					other->waveParticleCamera->ortho.viewMax;
				//printf("ship %d is in the sector of ship %d. Coords: %f %f\n", floater->id, other->id, scale_shift[i + size].x, scale_shift[i + size].y);
			}
			glUniform1i(gc.getUniformLocation("u_AdjacentSize"), size);
			glUniform4fv(gc.getUniformLocation("u_AdjacentWPScale_Shift"), size, &scale_shift[0].x);
			glUniform1iv(gc.getUniformLocation("u_AdjacentWPSurfTex"), size, &samplersSurf[0]);
			glUniform1iv(gc.getUniformLocation("u_AdjacentWPPointTex"), size, &samplersPoint[0]);
		}
		else {
			glUniform1i(gc.getUniformLocation("u_AdjacentSize"), 0);
		}
	}

	// Wave particles
	glUniform1i(gc.getUniformLocation("u_WPSurfTex"), wpSurfTex->textureUnit);
	//glUniform1i(gc.getUniformLocation("u_WPPointTex"), wpPointTex->textureUnit);
	glUniform2fv(gc.getUniformLocation("u_WPTexScale"), 1, &WPTexScale.x);
	glUniform2fv(gc.getUniformLocation("u_WPTexShift"), 1, &WPTexShift.x);

	glUniform1f(gc.getUniformLocation("u_WP_Horizontal"), waterSurface->horizontalCoef);
	glUniform1f(gc.getUniformLocation("u_WP_Vertical"), waterSurface->verticalCoef);
	// 3D noise
	glUniform1i(gc.getUniformLocation("u_Noise3D"), noiseTex->textureUnit);
	glUniform1f(gc.getUniformLocation("u_NoiseTime"), waterSurface->mNoiseTime);
	glUniform1f(gc.getUniformLocation("u_NoiseStrength"), waterSurface->mNoiseStrength);
	glUniform3fv(gc.getUniformLocation("u_NoiseScale"), 1, &noiseTexScale.x);

	glUniform2fv(gc.getUniformLocation("u_SilCamPos"), 1, &silCamPos.x);
	glUniform2fv(gc.getUniformLocation("u_SilCamView"), 1, &silCamView.x);
	//glUniform2fv(gc.getUniformLocation("u_NoiseShift"), 1, &noiseTexShift.x);

	static MeshVBO* texPlane = gc.getMesh(GC::MES_PLANE_UNIT);
	texPlane->display(GL_QUADS);

	oglCheckError("W2O: after disp");

}

void WaterToObjectCoupling::renderBuoyancyTexture(GraphicsContext& gc, FloatingObject* floater){
	
	gc.curCamera = floater->silhouetteCamera;
	gc.applyProjectionMatrix();

	glUniform1i(gc.getUniformLocation("u_Step"), 1);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texBuoyancy.texID, 0);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, floater->texBuoyancy.width, floater->texBuoyancy.height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	// world to texture
	vec2 silCamScale = 1.0f / floater->silhouetteCamera->ortho.viewMax;
	vec4 heightTexToWorld = vec4(silCamScale, silCamScale * static_cast<vec2>(floater->silhouetteCamera->position));

	floater->texHeightfield.bind(1);
	glUniform1i(gc.getUniformLocation("u_WaterHeightTex"), floater->texHeightfield.textureUnit);
	glUniform4fv(gc.getUniformLocation("u_HeightTexToWorld"), 1, &heightTexToWorld.x);

	vec4 waterMVP = gc.getProjectionMatrix() * gc.getModelViewMatrix() * vec4(gc.simConf->waterOrigin, 1);
	glUniform4fv(gc.getUniformLocation("u_WaterPosClip"), 1, &waterMVP.x);
	
	glViewport(0, 0, floater->texBuoyancy.width, floater->texBuoyancy.height);

	floater->objConvex->display(gc);
}

void WaterToObjectCoupling::renderDragAndLiftForce(GraphicsContext& gc, FloatingObject* floater){

	static GLenum colorAttachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	// Draw each face as a point, writing the drag and lift force of each face of the object
	// Convex object produces smaller errors 
	glUniform1i(gc.getUniformLocation("u_Step"), 3);
	glUniform1f(gc.getUniformLocation("u_DragCoef"), floater->dragCoef);
	glUniform1f(gc.getUniformLocation("u_LiftCoef"), floater->liftCoef);
	glUniform1f(gc.getUniformLocation("u_ObjProjWeight"), floater->meshProjectCoef);

	glUniform1f(gc.getUniformLocation("u_ObjSpeed"), floater->speed);
	glUniform3fv(gc.getUniformLocation("u_ObjDirection"), 1, &floater->direction.x);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texWaterForces.texID, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, floater->texLiftForces.texID, 0);
	glDrawBuffers(2, colorAttachments);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPointSize(1); // Points represent Faces (one per pixels so no information duplicity occur)
	Model3D* convexObj = floater->objConvex;
	convexObj->meshCentroids->draw_nonIndexed(GL_POINTS);

	//readWaterForces(gc, floater);
	oglCheckError("W2O: after bouyancy display routine");

}

void WaterToObjectCoupling::renderWaterHole(GraphicsContext& gc, FloatingObject* floater){
	
	gc.curCamera = floater->silhouetteCamera;
	gc.applyProjectionMatrix();
	glUniform1i(gc.getUniformLocation("u_Step"), 2);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texWaterHole.texID, 0);

	vec4 waterMV = gc.getModelViewMatrix() * vec4(gc.simConf->waterOrigin, 1);
	glUniform4fv(gc.getUniformLocation("u_WaterPosWorld"), 1, &waterMV.x);
		
	glViewport(0, 0, floater->texWaterHole.width, floater->texWaterHole.height);
	glClearColor(0.f, 0.f, 0.f, 0.f); // alpha == 0
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	static GLenum colorAttachments[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, colorAttachments); // only the first one
	
	// Water hole creation
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	floater->objConvex->display(gc);
	// return face cull to previous state
	gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
}

void WaterToObjectCoupling::display(GraphicsContext& gc)
{
	oglCheckError("W2O: before display");
	// Put nearby ships into a adjacent collection
	if (gc.frameCount % adjTestFrequency == 0){
		for (FloatingObject* floater : gc.curResources->floaters){
			floater->adjacent.clear();
		}

		int size = gc.curResources->floaters.size();
		for (int i = 0; i < size; i++){
			FloatingObject* floater = gc.curResources->floaters[i];
			for (int j = i + 1; j < size; j++){
				FloatingObject* other = gc.curResources->floaters[j];
				if (glm::distance(floater->position, other->position) < floater->activeDistance){
					floater->adjacent.push_back(other);
					other->adjacent.push_back(floater);
				}
			}
		}
	}

	// Shader set
	if (gc.curShader != gc.getShaders(GC::SHA_WATER_TO_OBJECT)){
		gc.curShader = gc.getShaders(GC::SHA_WATER_TO_OBJECT);
		glUseProgram(gc.curShader->programId);
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	processFloaters(gc);
	
	// Count volume of underwater part of the object 
	gc.curShader = gc.getShaders(GC::SHA_REDUCTION4);
	glUseProgram(gc.curShader->programId);
	for (FloatingObject* floater : gc.curResources->floaters){
		floater->volumeInWater = countVolume(gc, floater);
	}

	// Count drag and lift forces on GPU
	gc.curShader = gc.getShaders(GC::SHA_REDUCTION);
	glUseProgram(gc.curShader->programId);
	for (FloatingObject* floater : gc.curResources->floaters){
		readWaterForcesGPU(gc, floater);
	}
	
	// Ship deviation and wave force 
	if (gc.frameCount > 2){
		// Floater deviation is in a deadlock after initialization step
		// texWaterInteraction is needed and the output influences it (or it was another texture?)
		gc.curShader = gc.getShaders(GC::SHA_FLOATER_DEVIATION);
		glUseProgram(gc.curShader->programId);
		for (FloatingObject* floater : gc.curResources->floaters){
			applyShipDeviation(gc, floater);
		}
	}

}


void WaterToObjectCoupling::processFloaters(GraphicsContext& gc){
	// =========== Step 4 ===========
	glBindFramebuffer(GL_FRAMEBUFFER, FBOheightfield);
	for (FloatingObject* floater : gc.curResources->floaters){
		renderHeightTexture(gc, floater);
	}

	// Bind once
	// !!! When rendering to a texture with different size both (Render buffer) and Framebuffer must be rebound
	// =========== Step 2 ===========
	if (gc.frameCount % waterHoleFrequency == 0){
		for (FloatingObject* floater : gc.curResources->floaters){	
			renderWaterHole(gc, floater);
		}
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, FBOobjectRender);
	glBindRenderbuffer(GL_RENDERBUFFER, RBOobjectDepth);
	
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND); // need to take overlaping pixels into account
	glBlendFunc(GL_ONE, GL_ONE);
	glDisable(GL_DEPTH_TEST); // need both front and back face
	// =========== Step 1 ===========
	if (gc.frameCount % buoyancyFrequency == 0){
		for (FloatingObject* floater : gc.curResources->floaters){
			renderBuoyancyTexture(gc, floater);
		}
	}
	
	// =========== Step 3 ===========
	for (FloatingObject* floater : gc.curResources->floaters){
		renderDragAndLiftForce(gc, floater); 
	}

	glDisable(GL_BLEND); // need to take overlaping pixels into account
	glEnable(GL_DEPTH_TEST); // need both front and back face
	
	gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
}


float WaterToObjectCoupling::countVolume(GraphicsContext& gc, FloatingObject* floater){
	// ----- CPU variant ------
	//int size = floater->texBuoyancy.width * floater->texBuoyancy.height;
	//std::vector<float> depths;
	//depths.resize(size);

	//floater->texBuoyancy.bind(2);
	//glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, &depths[0]);
	//
	//float volume = 0.f;
	//for (int i = 0; i < size; ++i){
	//	volume += depths[i]; // R
	//}
	//printf("volume: %f\n", volume);
	
	
	// ---- GPU variant -----
	static GLuint SSBO = -1;
	if (SSBO == -1){
		glGenBuffers(1, &SSBO);
	}

	//static vec4 zero = vec4(0, 0, 0, 0);
	static float zero = 0;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float), &zero, GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBO); // Second parameter = binding point (shader buffer)

	floater->texBuoyancy.bind(1);
	glUniform1i(gc.getUniformLocation("u_InputTex"), floater->texBuoyancy.textureUnit);

	glDispatchCompute(floater->texBuoyancy.width / 16, floater->texBuoyancy.height / 16, 1);
	//glMemoryBarrier(GL_ALL_BARRIER_BITS);

	float volume = 0.f;
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float), &volume);

	// ------ volume adjust --------
	// because volume is independent on the texture resolution
	float tex2word = static_cast<float>(gc.curCamera->ortho.viewMax.x) / floater->texBuoyancy.width; // assume square pixels
	float depth_word2clip = -gc.curCamera->projectionMatrix[2][2];

	volume = (volume * tex2word * tex2word) / depth_word2clip;

	return volume;
}



void WaterToObjectCoupling::readWaterForces(GraphicsContext& gc, FloatingObject* floater){
	// CPU variant
	int size = 4 * floater->texWaterForces.width * floater->texWaterForces.height;
	std::vector<float> depths;
	depths.resize(size);

	floater->texWaterForces.bind(1);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &depths[0]);

	vec3 finalForce; 
	for (int i = 0; i < size; i += 4) {
		finalForce += vec3(depths[i], depths[i+1], depths[i+2]);
	}
	
	floater->dragForce = finalForce;
	printf("cpu drag: %f %f %f\n", vec3Elements(finalForce));
	//printf("dragF: %f (%f) | liftF: %f (%f) \n", dragForce, FloatingObject::dragCoef * dragForce, liftForce, FloatingObject::liftCoef *  liftForce);
	// -------------------------------

	std::vector<float> depths2;
	depths2.resize(size);

	floater->texLiftForces.bind(2);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &depths2[0]);

	vec3 liftForce;
	for (int i = 0; i < size; i += 4) {
		liftForce += vec3(depths2[i], depths2[i + 1], depths2[i + 2]);
	}
	floater->liftForce = liftForce;
	printf("cpu lift: %f %f %f\n", vec3Elements(liftForce));
}

void WaterToObjectCoupling::readWaterForcesGPU(GraphicsContext& gc, FloatingObject* floater){
	// ---- GPU variant ------
	gc.curShader = gc.getShaders(GC::SHA_REDUCTION);
	glUseProgram(gc.curShader->programId);

	static GLuint SSBO = -1;
	if (SSBO == -1){
		glGenBuffers(1, &SSBO);
	}

	static vec4 zero = vec4(0);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4), &zero, GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBO); // Second parameter = binding point (shader buffer)

	floater->texWaterForces.bind(1);
	glUniform1i(gc.getUniformLocation("u_InputTex"), floater->texWaterForces.textureUnit);

	glDispatchCompute(floater->texWaterForces.width / 16, floater->texWaterForces.height / 16, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	//vec3 drag;
	//glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(vec3), &floater->dragForce);
	//printf("gpu drag: %f %f %f\n", vec3Elements(drag));
	

	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4), &zero, GL_STATIC_DRAW);
	floater->texLiftForces.bind(2);
	glUniform1i(gc.getUniformLocation("u_InputTex"), floater->texLiftForces.textureUnit);

	glDispatchCompute(floater->texLiftForces.width / 16, floater->texLiftForces.height / 16, 1);

	//vec3 lift;
	//glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	//glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(vec3), &lift);
	glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(vec3), &floater->liftForce);
	//printf("gpu lift: %f %f %f\n", vec3Elements(lift));
}


void WaterToObjectCoupling::applyShipDeviation(GraphicsContext& gc, FloatingObject* floater){
	oglCheckError("W2O: begin ship deviation");

	static GLuint SSBO = -1;
	if (SSBO == -1){
		glGenBuffers(1, &SSBO);
	}

	//static vec4 zero = vec4(0, 0, 0, 0);
	static float zero[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	glBufferData(GL_SHADER_STORAGE_BUFFER, 2 * sizeof(vec4), zero, GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBO); // Second parameter = binding point (shader buffer)
	int size = floater->texHeightfield.width;
	ivec2 imgCenter = ivec2(size / 2, size / 2);
	glUniform2iv(gc.getUniformLocation("u_Center"), 1, &imgCenter.x);

	floater->texHeightfield.bind(1);
	floater->texBuoyancy.bind(2);
	floater->particleRender.bind(3);
	glUniform1i(gc.getUniformLocation("u_HeightTex"), floater->texHeightfield.textureUnit);
	glUniform1i(gc.getUniformLocation("u_StencilTex"), floater->texBuoyancy.textureUnit);
	glUniform1i(gc.getUniformLocation("u_ForceTex"), floater->particleRender.textureUnit);
	glUniform1f(gc.getUniformLocation("u_InvTexSize"), 1.0f / size);

	// Rotation in the center of the image
	mat4 orientMat = glm::translate(mat4(), glm::vec3(0.5, 0.5, 0));
	orientMat = glm::rotate(orientMat, -floater->rotation.z, Z_AXIS);
	orientMat = glm::translate(orientMat, -glm::vec3(0.5, 0.5, 0));
	glUniformMatrix4fv(gc.getUniformLocation("u_RotationMat"), 1, GL_FALSE, &orientMat[0][0]);
	

	// For visualization
	Texture* texTemp = gc.getTexture(GC::TEX_TEMP);
	texTemp->bind(4);
	glUniform1i(gc.getUniformLocation("u_DestTex"), texTemp->textureUnit);
	glBindImageTexture(texTemp->textureUnit, texTemp->texID, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

	glDispatchCompute(size / 16, size / 16, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	glm::vec4 data[2]; // sector heights
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
	glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, 2 * sizeof(vec4), &data);
	glm::vec4 secH = data[0];
	secH /= static_cast<float>(size / 4); 
	//glm::vec4 velocity = data[1];
	//printf("sectors: %f %f %f %f\n", vec4Elements(secH));
	//printf("velocity: %f %f %f %f\n", vec4Elements(velocity));
	//if (velocity.x != velocity.x){
	//	printf("NAN %f\n", velocity.x);
	//}
	// sectors to rotation
	//  0,  0 = 0 (lower left)
	//  1,  0 = 1 (lower right)
	//  0,  1 = 2 (upper left)
	//  1,  1 = 3 (upper right)
	
	float dY = (secH[2] + secH[3]) - (secH[0] + secH[1]); // x rotation
	float dX = (secH[1] + secH[3]) - (secH[0] + secH[2]); // y rotation
	// y bounding
	if (dY < 0){
		dY = std::max(dY, -floater->maxWaveSlope.y);
	} else {
		dY = std::min(dY, floater->maxWaveSlope.y);
	}
	// x bounding
	if (dX < 0){
		dX = std::max(dX, -floater->maxWaveSlope.x);
	} else {
		dX = std::min(dX, floater->maxWaveSlope.x);
	}

	floater->rotationSurface = static_cast<vec3>(orientMat * vec4(dY * floater->rotationSpeed, -dX * floater->rotationSpeed, 0, 1));
	floater->waveImpuls = static_cast<vec3>(data[1]) * floater->waveStrength;

	//if (isNANvec(floater->waveImpuls)){
	//	printf("%f %f %f\n", vec3Elements(floater->waveImpuls));
	//}
	oglCheckError("W2O: end ship deviation");
}