#ifndef __LIGHT_H__
#define __LIGHT_H__

#include "glm.h"

class Light{
public:
	glm::vec4 position;			// object space
	glm::vec4 positionWorld;	// world space
};

#endif