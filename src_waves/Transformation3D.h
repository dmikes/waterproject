#ifndef __TRANSFORMATION_3D__
#define __TRANSFORMATION_3D__

#include "glm.h"
#include <string>

// forward declaration
class TransformationConfig;


class Transformation3D {
public:
	std::string filename;
	vec3 translation;
	vec3 rotation;
	vec3 scale;

	Transformation3D();
	Transformation3D(TransformationConfig* config);
	Transformation3D(std::string configFile);
	void merge(Transformation3D& other);
	
	enum E_Axis {
		X = 0,
		Y,
		Z
	};

	void setRotation(vec3 val);
	void setRotation(float val, E_Axis axis);
	vec3 getRotation() const;
	void setTranslation(vec3 val);
	vec3 getTranslation() const;
	void setScale(vec3 val);
	void setScale(vec2 val);
	vec3 getScale() const;

	void applyTRSToMatrix(mat4& mat);
	void applySTRToMatrix(mat4& mat);
	void applyScaleToMatrix(mat4& mat);
	void applyTranslationToMatrix(mat4& mat);
	void applyRotationToMatrix(mat4& mat);
	void applyRotationXToMatrix(mat4& mat);
	void applyRotationYToMatrix(mat4& mat);
	void applyRotationZToMatrix(mat4& mat);
	void applySRTToMatrix(mat4& mat);
	void applySRzTToMatrix(mat4& mat);
};

#endif