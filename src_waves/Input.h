#pragma once
#include "myGL.h"
#include "glm.h"

class GraphicsContext;
class WaveParticleSimulation;
class GUI;

class Mouse{
private:
	bool leftClicked = false;
	bool leftReleased = false;
	bool rightClicked = false;
	bool rightReleased = false;
	ivec2 lastFramePos;
public:
	void mousePositionChanged(GraphicsContext& gc, int x, int y);

	void mouseButtonChanged(int button, int action);

};


class Keyboard{
	void (TW_CALL *compileShaders)(void *clientData);
public:
	GUI* Gui;
	WaveParticleSimulation* waveParticles;
	void setShaderCompileFunc(void (TW_CALL *func)(void *clientData));


	void keyboardChanged(GraphicsContext& gc, int key, int scancode, int action, int mods);
	void keyboardCheckState(GraphicsContext& gc, GLFWwindow* window, float deltaTime);
};