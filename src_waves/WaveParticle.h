#ifndef __WAVE_PARTICLE_H__
#define __WAVE_PARTICLE_H__

#include "myGL.h"
#include "glm.h"


#define WPV_LOC_POSITION_ORIGIN 0
#define WPV_LOC_AMP_PROPAG_DISP 1
#define WPV_LOC_COLOR 2
#define WPV_LOC_INFO 3



// ! This struct must have the same structure as the one in shader
struct WaveParticle{
private: 
	// ! IMPORTANT ! Ordering of the data must obey structure of the shader!

	// xy: current position of wave particle
	// z: distance to the wave particle origin
	// w: particle width (max value 65536.0f) and number of consequtive frames (not accesible on cpu) packed into 1 float
	glm::vec4 mPos2_Orig_Size;
	// x: amplitude
	// y: propagation angle
	// z: dispersion angle
	// w: wave particle speed | sign of the speed = sign of the origin amplitude
	glm::vec4 mAmp_Prop_Disp_Spee;
public:	
	// Methods
	glm::vec2 getPosition() const;
	glm::vec2 getOrigin() const;
	glm::vec4 getColor() const;
	float getAmplitude() const;
	float getPropagationAngle() const;
	float getDispersionAngle() const;
	float getSize() const;
	float getSpeed() const;

	void setPosition(glm::vec2 val);
	void setOrigin(glm::vec2 val);
	void setColor(glm::vec4 val);
	void setAmplitude(float val);
	void setPropagationAngle(float val);
	void setDispersionAngle(float val);
	void setSize(float val);
	void setSpeed(float val);	
};

#endif