#include "MeshVBO.h"

#include <assert.h>

// TODO: unified locations across application!
const int POSITION_LOC = 0;
const int NORMAL_LOC = 1;
const int COLOR_LOC = 2;
const int TEXTURE_LOC = 3;

#define BUFFER_OFFSET(i) ((char*) NULL + (i))

MeshVBO::MeshVBO(): VBO(0), IBO(0), VAO(0),
	vertices(NULL), normals(NULL), texCoords(NULL), colors(NULL), indices(NULL), 
	alpha(1.0f), initialized(false), indexed(false)
{
	locPosition = POSITION_LOC;
	locNormal	= NORMAL_LOC;
	locTexCoord = TEXTURE_LOC;
	locColor	= COLOR_LOC;
	
	nTexCoordFloats = 2;
}


MeshVBO::~MeshVBO(void) {
	//glDeleteBuffers(1, &newParticleVBO);
	//glDeleteBuffers(1, &particleGpuVBO);
	//glDeleteVertexArrays(1, &newParticleVAO);
	//glDeleteVertexArrays(1, &particleGpuVAO);
}

void MeshVBO::init()
{
	assert(nVertices > 0);
	if(indexed){
		init_indexed();
	}else{
		init_nonIndexed();
	}
	initialized = true;
}

void MeshVBO::display(int glDrawMode){
	if(indexed){
		draw_indexed(glDrawMode);
	}else{
		draw_nonIndexed(glDrawMode);
	}
}


void MeshVBO::init_nonIndexed(){
	initialized = true;
	bufferFloats = nVertexFloats;
	if(normals != NULL){
		bufferFloats += nNormalFloats;
	}
	if(texCoords != NULL){
		bufferFloats += nTexCoordFloats;
	}
	if(colors != NULL){
		bufferFloats += nColorFloats;
	}

	int verticesBytes = nVertices * sizeof(GLfloat);
	GLint start = 0;
	GLint size;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// upload vertices data to gpu
		glBufferData(GL_ARRAY_BUFFER, verticesBytes * bufferFloats, NULL, GL_STATIC_DRAW);
		size = nVertexFloats * verticesBytes;
		glBufferSubData(GL_ARRAY_BUFFER, start, size, vertices);
		start = start + size;
	// upload normals to gpu
	if(normals != NULL){
		size = nNormalFloats * verticesBytes;
		glBufferSubData(GL_ARRAY_BUFFER, start, size, normals);
		start = start + size;
	}
	// upload tex coord to gpu
	if(texCoords != NULL){
		size = nTexCoordFloats * verticesBytes;
		glBufferSubData(GL_ARRAY_BUFFER, start, size, texCoords);
		start = start + size;
	}
	// upload colors to gpu
	if(colors != NULL){
		size = nColorFloats * verticesBytes;
		glBufferSubData(GL_ARRAY_BUFFER, start, size, colors);
		start = start + size;
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Bind VBO and current attribute locations into VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	setVertexAttrib();
	glBindVertexArray(0);
}


void MeshVBO::init_indexed(){
	initialized = true;
	init_nonIndexed();

	int indexBytes = sizeof(GLuint);
	switch (indexFormat){
		case E_VBO_IndexFormat::VBO_UINT :
			indexBytes = sizeof(GLuint);
			break;
		case E_VBO_IndexFormat::VBO_USHORT :
			indexBytes = sizeof(GLushort);
			break;
	}	

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBytes * nIndices, indices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Add IBO to VAO
	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBindVertexArray(0);
}

void MeshVBO::setVertexAttrib(){
	int verticesBytes = nVertices * sizeof(GLfloat);
	int previousFloats = 0;
	glEnableVertexAttribArray(locPosition);
	glVertexAttribPointer(locPosition, nVertexFloats, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	previousFloats += nVertexFloats;

	if(normals != NULL){
		glEnableVertexAttribArray(locNormal);
		glVertexAttribPointer(locNormal, nNormalFloats,	GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(previousFloats * verticesBytes));
		previousFloats += nNormalFloats;
	}
	if(texCoords != NULL){
		glEnableVertexAttribArray(locTexCoord);
		glVertexAttribPointer(locTexCoord, nTexCoordFloats, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(previousFloats * verticesBytes));
		previousFloats += nTexCoordFloats;
	}
	if(colors != NULL){
		glEnableVertexAttribArray(locColor);
		glVertexAttribPointer(locColor, nColorFloats,		GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(previousFloats * verticesBytes));
		previousFloats += nColorFloats;
	}
}

void MeshVBO::unsetVertexAttrib(){
	glDisableVertexAttribArray(locPosition);
	glDisableVertexAttribArray(locNormal);
	glDisableVertexAttribArray(locTexCoord);
	glDisableVertexAttribArray(locColor);
}

void MeshVBO::draw_indexed(int glDrawMode){
	glBindVertexArray(VAO);
	glDrawElements(glDrawMode, nIndices, indexFormat, BUFFER_OFFSET(0)); // Indexed
	glBindVertexArray(0);
}


void MeshVBO::draw_nonIndexed(int glDrawMode){
	glBindVertexArray(VAO);
	glDrawArrays(glDrawMode, 0, nVertices);// Non-indexed
	glBindVertexArray(0);
}

void MeshVBO::draw_nonIndexed(int glDrawMode, int offset, int count){
	glBindVertexArray(VAO);
	glDrawArrays(glDrawMode, offset, count);
	glBindVertexArray(0);
}

void MeshVBO::setVertices( GLuint nVertex, GLfloat* vertices )
{
	this->nVertices = nVertex;
	this->vertices = vertices;
}

void MeshVBO::setNormals( GLfloat* normals )
{
	this->normals = normals;
}

void MeshVBO::setTexCoords( GLfloat* texCoords )
{
	this->texCoords = texCoords;
}

void MeshVBO::setColors( GLfloat* colors )
{
	this->colors = colors;
}

void MeshVBO::setIndices( GLuint nIndex, GLvoid* indices, E_VBO_IndexFormat indexType )
{
	indexed = true;
	this->nIndices = nIndex;
	this->indices = indices;
	this->indexFormat = indexType;
}

/*
* Sets the format of the tex coords dimension
* \param[in] format Number of dimensions of texture coordinates (usually 2 = 2D tex coords, 3 = 3d tex coords)
*/
void MeshVBO::setTextureCoordFormat(int format)
{
	nTexCoordFloats = format;
}

/*
* Sets the format of the tex coords dimension
* \param[in] format Number of dimensions of color values (usually 3 or 4)
*/
void MeshVBO::setColorFormat(int format)
{
	nColorFloats = format;
}

void MeshVBO::setVertexFormat(int format)
{
	nVertices = format;
}

void MeshVBO::setNormalFormat(int format)
{
	nNormalFloats = format;
}

bool MeshVBO::isInitialized()
{
	return initialized;
}
