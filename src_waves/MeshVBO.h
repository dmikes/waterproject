#ifndef __SINGLE_MESH_H__
#define __SINGLE_MESH_H__

#include <iostream>
#include "myGL.h"


enum eMeshMode{
	UNDEFINED = 0,
	VERTEX,
	VERTEX_NORMAL,
	VERTEX_NORMAL_TEX,
	VERTEX_NORMAL_COLOR
};



class MeshVBO {
private:
	int nVertexFloats = 3;
	int nNormalFloats = 3;
	int nTexCoordFloats = 2;
	int nColorFloats = 3;

	GLuint nVertices = 0;	///< number of vertices
	GLuint nIndices = 0;	///< number of indices

	GLuint VAO = 0; ///< vertex attrib array
	GLuint VBO = 0; ///< vertex buffer object
	GLuint IBO = 0; ///< index buffer object

	int mode = 0;
	bool indexed = false;
	bool initialized = false;
	int bufferFloats = 0;
	float alpha = 1.0f;
public:
	MeshVBO();

	~MeshVBO(void);

	enum E_VBO_IndexFormat {
		VBO_USHORT = GL_UNSIGNED_SHORT,
		VBO_UINT = GL_UNSIGNED_INT
	};
	E_VBO_IndexFormat indexFormat;

	void setVertices(GLuint nVertex, GLfloat* vertices);
	void setNormals(GLfloat* normals);
	void setTexCoords(GLfloat* texCoords);
	void setColors( GLfloat* colors );
	void setIndices(GLuint nIndex, GLvoid* indices, E_VBO_IndexFormat indexType);

	void setColorFormat(int format);
	void setVertexFormat(int format);
	void setNormalFormat(int format);
	void setTextureCoordFormat(int format);

	bool isInitialized();

	void init();
	void display(int glDrawMode);
	void draw_indexed(int glDrawMode);
	void draw_nonIndexed(int glDrawMode);
	void draw_nonIndexed(int glDrawMode, int offset, int count);
private:
	// Deleting is not responcibility of this class
	GLfloat* vertices;
	GLfloat* normals;
	GLfloat* texCoords;
	GLfloat* colors;
	GLvoid* indices; // void - choose bethween ushort and uint. Better be a template

	int locPosition;
	int locNormal;
	int locTexCoord;
	int locColor;

	void init_nonIndexed();
	void init_indexed();
	void setVertexAttrib();
	void unsetVertexAttrib();
};


#endif