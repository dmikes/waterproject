#pragma once
#include "Drawable.h"
#include "myGL.h"

class FloatingObject;
class Model3D;

class WaterOptics : public Drawable {
private: // fields
	GLuint FBOwaterOptics;
	GLuint RBOwaterOptics;
public: // methods
	float waterClarity = 0.002f;
	float reflectDarkening = 0.55f;

	virtual void init(GraphicsContext& gc);
	virtual void winResize(GraphicsContext& gc, int width, int height);
	virtual void update(GraphicsContext& gc);
	virtual void display(GraphicsContext& gc);
	void preprocess(GraphicsContext& gc);
private: // methods
	void displayRefractionReflection(GraphicsContext& gc, FloatingObject* floater, bool isFirst);
	void displayRefractionReflection(GraphicsContext& gc, Model3D* staticObject);
	void displayRefraction(GraphicsContext& gc, Model3D* staticObject);
	void displayRefraction(GraphicsContext& gc, FloatingObject* floater, bool isFirst);
	void displayReflection(GraphicsContext& gc, Model3D* staticObject);
	void displayReflection(GraphicsContext& gc, FloatingObject* floater, bool isFirst);
};