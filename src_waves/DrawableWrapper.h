#ifndef __DRAWABLE_WRAPPER_H__
#define __DRAWABLE_WRAPPER_H__

#include <functional>
#include "Drawable.h"

class GraphicsContext;

class DrawableWrapper : public Drawable {
public:
	std::function<void(GraphicsContext& gc, int width, int height)> winResizeFunc;
	std::function<void(GraphicsContext& gc)> initFunc;
	std::function<void(GraphicsContext& gc)> updateFunc;
	std::function<void(GraphicsContext& gc)> displayFunc;
	std::function<void(GraphicsContext& gc)> preprocessFunc;

	void winResize(GraphicsContext& gc, int width, int height);
	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);	
	void preprocess(GraphicsContext& gc);
};

#endif