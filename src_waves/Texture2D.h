#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <array>
#include "myGL.h"
#include "../libs/lodepng/lodepng.h"
#include "GraphicsContext.h"
#include "MeshVBO.h"
#include "GeometryPrimitives.h"

class GraphicsContext;

class Texture {
public:
	static MeshVBO sprite;
	static bool spriteInitialized;
	static int nMaxTexUnits;
	static int nTexUnits;


	GLuint texID; // Id of texture object (glBindTexture)
	GLuint textureUnit; // Texture unit (glActiveTexture)
	uint width;
	uint height;
	uint depth;
	//GLint internalFormat;
	//GLenum inputFormat;
	GLenum target;

	Texture();
	void bind();
	void bind(GLuint texUnit);
	void unbind();
	// init
	void init(GLuint target, GLuint filter, GLuint wrap);
	void init3D(GLenum target, GLuint minFilter, GLuint magFilter, GLuint wrap);
	// allocate
	void allocate(int width, int height, GLint internalFormat, GLenum inputFormat, GLenum dataType);
	void allocate(ivec2 size, GLenum internalFormat, GLenum inputFormat, GLenum dataType);
	void allocateCubemap(ivec2 size, GLint internalFormat, GLenum inputFormat, GLenum dataType);
	void allocate3D(int width, int height, int depth, GLint internalFormat, GLenum inputFormat, GLenum dataType);
	// load image
	void loadImage(std::string, GLint internalFormat = GL_RGB, GLenum inputFormat = GL_RGBA, GLenum dataType = GL_UNSIGNED_BYTE);

	void loadImageCubemap(std::string path, std::array<std::string, 6> filenames, std::string ext, GLint internalFormat, GLenum inputFormat, GLenum dataType);
	void display(GraphicsContext& gc, GLint x, GLint y, GLsizei wight, GLsizei height);
	void display3D(GraphicsContext& gc, GLint x, GLint y, GLsizei wight, GLsizei height);

	uint getDim1();
	ivec2 getDim2();
	ivec3 getDim3();
	void decodeImage(std::string filename, uint& width, uint& height, byte* dataPtr);
};

#endif