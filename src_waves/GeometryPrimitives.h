#ifndef __GEOMETRY_PRIMITIVES__
#define __GEOMETRY_PRIMITIVES__

#include "myGL.h"

const GLfloat cubeVertices[24] = {
	0.0f,	0.0f,	0.0f,
	0.0f,	0.0f,	1.0f,
	0.0f,	1.0f,	0.0f,
	0.0f,	1.0f,	1.0f,
	1.0f,	0.0f,	0.0f,
	1.0f,	0.0f,	1.0f,
	1.0f,	1.0f,	0.0f,
	1.0f,	1.0f,	1.0f,
};

const GLfloat cubeTexCoords[24] = {
	0.0f,	0.0f,	1.0f, // 2
	0.0f,	0.0f,	0.0f, // 2
	0.0f,	1.0f,	1.0f, // 2
	0.0f,	1.0f,	0.0f, // 2
	1.0f,	0.0f,	1.0f,
	1.0f,	0.0f,	0.0f,
	1.0f,	1.0f,	1.0f,
	1.0f,	1.0f,	0.0f,
};
const GLushort cubeLineIndices[24] = {0,1, 1,3, 2,3, 2,0,		0,4, 1,5, 2,6, 3,7,		4,5, 5,7, 6,7, 6,4};
const GLushort cubeFaceIndices[24] = {4,0,1,5,	0,2,3,1,	2,6,7,3,	6,4,5,7,	0,4,6,2,	1,3,7,5};


#define GD_PLANE_LOWER -1.0f
#define GD_PLANE_HIGHER 1.0f
const GLfloat planeVertices[12] = {
	GD_PLANE_LOWER,		GD_PLANE_LOWER,		0.0f,
	GD_PLANE_HIGHER,	GD_PLANE_LOWER,		0.0f,
	GD_PLANE_HIGHER,	GD_PLANE_HIGHER,	0.0f,
	GD_PLANE_LOWER,		GD_PLANE_HIGHER,	0.0f,
};
#undef GD_PLANE_LOWER
#undef GD_PLANE_HIGHER


#define GD_PLANE_LOWER  0.0f
#define GD_PLANE_HIGHER 1.0f
const GLfloat unitPlaneVertices[12] = {
	GD_PLANE_LOWER,		GD_PLANE_LOWER,		0.0f,
	GD_PLANE_HIGHER,	GD_PLANE_LOWER,		0.0f,
	GD_PLANE_HIGHER,	GD_PLANE_HIGHER,	0.0f,
	GD_PLANE_LOWER,		GD_PLANE_HIGHER,	0.0f,
};

#undef GD_PLANE_LOWER
#undef GD_PLANE_HIGHER

const GLfloat unitPlaneNormals[12] = {
	0.0f,		0.0f,		1.0f,
	0.0f,		0.0f,		1.0f,
	0.0f,		0.0f,		1.0f,
	0.0f,		0.0f,		1.0f,
};

const GLfloat unitPlaneTexCoords[8] = {
	0.0f,	0.0f,
	1.0f,	0.0f,
	1.0f,	1.0f,
	0.0f,	1.0f,
};


#endif