#include "myGL.h"

#ifndef oglCheckError
void oglCheckError(std::string message)
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		std::string errStr;
		switch (err){
			case GL_INVALID_ENUM:		errStr = "GL_INVALID_ENUM";			break;
			case GL_INVALID_VALUE:		errStr = "GL_INVALID_VALUE";		break;
			case GL_INVALID_OPERATION:  errStr = "GL_INVALID_OPERATION";	break;
			case GL_STACK_OVERFLOW:		errStr = "GL_STACK_OVERFLOW";		break;
			case GL_STACK_UNDERFLOW:	errStr = "GL_STACK_UNDERFLOW";		break;
			case GL_OUT_OF_MEMORY:		errStr = "GL_OUT_OF_MEMORY";		break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:  errStr = "GL_INVALID_FRAMEBUFFER_OPERATION";		break;
		}
		printf("OpenGL error: %s  err: %#08x (%s) \n", message.c_str(), err, errStr.c_str());
		//abort();
	}
}
#endif
