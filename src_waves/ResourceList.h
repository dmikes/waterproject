#pragma once
#include <vector>

class FloatingObject;
class Model3D;
// ResourceLists are used only for object that theoretically can change at runtime
// When there is a shader which can and will be used only by specific class there is no need to pass it in ResourceList

class ResourceList {
public:
	bool doInit = true;
	bool doDraw = true;
	bool doUpdate = true;
	bool doWinResize = true;
	Camera* inputCamera;
	ShaderSet* inputShader;

	std::vector<FloatingObject*> floaters;
	std::vector<Model3D*> models;
};