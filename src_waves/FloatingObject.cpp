#include "FloatingObject.h"
#include "Camera.h"
#include "Model3D.h"

//float FloatingObject::volumeCoef = 6.f;

int FloatingObject::instanceCount = 0;
float FloatingObject::waterRho = 1.f; // g/cm^3

//FloatingObject::FloatingObject(CameraConfig* silhouetteCamConfig, CameraConfig* waveParticleCamConfig)
FloatingObject::FloatingObject(FloatingObjectConfig* config, std::string silhouetteCamConfig, std::string waveParticleCamConfig)
{
	silhouetteCamera = new Camera(silhouetteCamConfig);
	waveParticleCamera = new Camera(waveParticleCamConfig);
	density = 0.80f;
	activeDistance = waveParticleCamera->ortho.viewMax.x / 2.f;
	id = instanceCount++;
	reset(config);
}

//FloatingObject::FloatingObject(GraphicsContext& gc, FloatingObjectConfig* config)
//{
//	for (Camera* cam : gc.cameras){
//		if (config->silhouetteCamera == cam->filename){
//			silhouetteCamera = new Camera(cam->config);
//		}
//		if (config->waveParticleCamera == cam->filename){
//			waveParticleCamera = new Camera(cam->config);
//		}
//	}
//	
//	reset(config);
//	activeDistance = waveParticleCamera->ortho.viewMax.x / 2.f;
//	id = instanceCount++;
//}

void FloatingObject::reset(FloatingObjectConfig* config){
	density		= config->density;
	swayReduce	= config->swayReduce;
	waveStrength= config->waveStrength;
	floatSpeed	= config->floatSpeed;
	moveStep	= config->moveStep;
	dragCoef	= config->dragCoef;
	liftCoef	= config->liftCoef;
	meshProjectCoef = config->meshProjectCoef;
	volumeCumulationCoef = config->volumeCumulationCoef;
	rollRestore = config->rollRestore;
	rollMax		= config->rollMax;
	pitchMax	= config->pitchMax;
	pitchMaxSpeed	= config->pitchMaxSpeed;
	maxWaveSlope.x	= config->maxWaveSlopeX;
	maxWaveSlope.y	= config->maxWaveSlopeY;
	rotationSpeed	= config->rotationSpeed;

	rotSpeedVec		= config->rotSpeedVec;
}

FloatingObject::~FloatingObject()
{
	delete silhouetteCamera;
	delete waveParticleCamera;
	delete objNormal;
	delete objConvex;
}

float FloatingObject::getMass(){
	return density * volume;
}

void FloatingObject::setVolume(float vol){
	volume = vol;
	//mass = density * volume;
}

void FloatingObject::updateMovement(){	
	if (hasMoved){
		// W2O
		silhouetteCamera->position.x = position.x - texW2OHalfsize;
		silhouetteCamera->position.y = position.y - texW2OHalfsize;
		silhouetteCamera->updateCameraViewMatrix();
		
		// O2W
		waveParticleCamera->position.x = position.x - texO2WHalfsize;
		waveParticleCamera->position.y = position.y - texO2WHalfsize;
		waveParticleCamera->updateCameraViewMatrix();

		waterTexTransform = mat4();
		waterTexTransform = glm::scale(waterTexTransform, 1.f / vec3(silhouetteCamera->ortho.viewMax, 1.f));
		waterTexTransform = glm::translate(waterTexTransform, -(silhouetteCamera->getPosition() - position));
		waterTexTransform = glm::rotate(waterTexTransform, -rotation.z, Z_AXIS);
		//waterTexTransform = glm::rotate(waterTexTransform,		0.0f, Z_AXIS);
		waterTexTransform = glm::translate(waterTexTransform, -position);

		hasMoved = false;
	}
}

#define isNANvec(v) v.x != v.x || v.y != v.y || v.z != v.z
#define isNAN(x) x != x
void FloatingObject::update(GraphicsContext& gc)
{
	updateMovement();
	
	if (!gc.pauseDemonstration){
		if (curImpuls < impulses.size()){
			Impulse* imp = impulses[curImpuls];

			if (imp->from < gc.frameCount && gc.frameCount < imp->to){
				moveImpuls += front * imp->force;
				rotation.z += imp->turn;
			}
			if (gc.frameCount >= imp->to){
				curImpuls + 1;
			}
		}
	}

	float timeStep = 0.1f; //TODO: gc.deltaTime (draw method deltaTime)
	if (!gc.pauseForces){
		const static vec3 g = vec3(0, 0, -9.8);
		forces = vec3();
		// move force
		forces += moveImpuls;
		forces += waveImpuls;
		moveImpuls = vec3(0); // reset impulse forces
		waveImpuls = vec3(0);

		if (!gc.pauseWorldForces){
			// gravity	
			forces += g * getMass();
			// buoyancy
			// -g * rho * V
			forces -= g * waterRho * volumeInWater;// * volumeCoef
			// drag and lift
			dragForce.z *= swayReduce; // reduce sway induced by gravity-buoyancy inbalance 
			forces += dragForce;
			forces += liftForce;
		}

		// a = F / m
		acceleration = forces / getMass(); 
		velocity += acceleration * timeStep;
		addPosition(velocity * timeStep);
		
		// velocity = speed * direction
		speed = glm::length(velocity);
		if (speed > 0.01){
			direction = velocity / speed;
		} else {
			direction = vec3(0, 0, 0);
		}

		// -- Boat animation --
		if (speed > 0){
			rotation.x += rotSpeedVec.x * (rotation.z - prevRotX);
			objNormal->rotation.x = (rotation.x > 0) ? -glm::min(rotation.x, rollMax) : -glm::max(rotation.x, -rollMax);
			objNormal->rotation.y = glm::min(speed * rotSpeedVec.y, pitchMaxSpeed) * pitchMax * (movesForward ? -1 : 0.5);
		}
		rotation.x *= rollRestore;
		prevRotX = rotation.z; 
		
		objNormal->rotation.z = rotation.z;
		objNormal->rotation += rotationSurface;
		//objConvex->rotation.z = rotation.z; //= different approach rotate the silhouette instead of rotating the query

		//mat4 rotM = glm::rotate(mat4(), rotation.z, Z_AXIS);
		//front = static_cast<vec3>(rotM * vec4(1, 0, 0, 1));
		float rotZ = glm::radians(rotation.z);
		front = vec3(cos(rotZ), sin(rotZ), 0);
	}

	if (gc.motionParticles){
		cumulativeWaveTime += timeStep; 
		cumulativeWaveMotion += speed * FloatingObject::volumeCumulationCoef;

		if (cumulativeWaveTime > gc.motionFreq){
			createParticles = true;
			cumulativeWaveTime = 0.f; // reset
		}
	}

}


void FloatingObject::particlesGenerated(GraphicsContext& gc)
{

}

void FloatingObject::init(GraphicsContext& gc)
{

	// Render wave particles
	particleRender.init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_BORDER);
	particleRender.allocate(waveParticleCamera->winDim, GL_RGB32F, GL_RGB, GL_FLOAT);

	particleFilterByX.init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_BORDER);
	particleFilterByX.allocate(gc.simConf->texSize, GL_RGB32F, GL_RGB, GL_FLOAT);

	particleFiltered.init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_BORDER);
	particleFiltered.allocate(gc.simConf->texSize, GL_RGB32F, GL_RGB, GL_FLOAT);

	gradientFilterByX.init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_BORDER);
	gradientFilterByX.allocate(gc.simConf->texSize, GL_RGB32F, GL_RGB, GL_FLOAT);

	gradientFiltered.init(GL_TEXTURE_2D, GL_LINEAR, GL_CLAMP_TO_BORDER);
	gradientFiltered.allocate(gc.simConf->texSize, GL_RGB32F, GL_RGB, GL_FLOAT);

	// Water to Object
	// Notice: only red
	texBuoyancy.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texBuoyancy.allocate(ivec2(32, 32), GL_R32F, GL_RED, GL_FLOAT);

	// Notice: only red
	texWaterHole.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texWaterHole.allocate(ivec2(256, 256), GL_R32F, GL_RED, GL_FLOAT);
	

	texWaterForces.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texWaterForces.allocate(ivec2(32, 32), GL_RGBA32F, GL_RGBA, GL_FLOAT);

	texLiftForces.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texLiftForces.allocate(texWaterForces.getDim2(), GL_RGBA32F, GL_RGBA, GL_FLOAT);

	texHeightfield.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texHeightfield.allocate(ivec2(256, 256), GL_RGBA32F, GL_RGBA, GL_FLOAT);
	
	// Object to Water
	texObjectSilhouette.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texObjectSilhouette.allocate(silhouetteCamera->winDim, GL_RGB32F, GL_RGB, GL_FLOAT);

	texWaveEffect.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texWaveEffect.allocate(silhouetteCamera->winDim, GL_RGB32F, GL_RGB, GL_FLOAT);

	texSilhouetteEdge.init(GL_TEXTURE_2D, GL_NEAREST_MIPMAP_NEAREST, GL_CLAMP_TO_BORDER);
	texSilhouetteEdge.allocate(silhouetteCamera->winDim, GL_RGBA32F, GL_RGBA, GL_FLOAT); //notice: + alpha
	glGenerateMipmap(GL_TEXTURE_2D); //allocate lower mipmap levels (no data so far - RenderToTexture is used)

	texTemp.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texTemp.allocate(silhouetteCamera->winDim, GL_RGBA32F, GL_RGBA, GL_FLOAT);

	texFinalWaveParticle.init(GL_TEXTURE_2D, GL_NEAREST, GL_CLAMP_TO_BORDER);
	texFinalWaveParticle.allocate(silhouetteCamera->winDim, GL_RGBA32F, GL_RGBA, GL_FLOAT);

	vec2 texSize = 1.f / vec2(texObjectSilhouette.getDim2());
	vec3 size = objConvex->worldBB.mMax - objConvex->worldBB.mMin;
	texW2OHalfsize = (glm::max(size.x, size.y) / 2.f) *(1.f + texW2OOverflow * texSize.x);

	silhouetteCamera->ortho.viewMax = 2.f * vec2(texW2OHalfsize, texW2OHalfsize);
	silhouetteCamera->updateProjection();


	texO2WHalfsize = waveParticleCamera->ortho.viewMax.x / 2.f;

	updateMovement();

	if (!objNormal->isInitialized){
		objNormal->init(gc);
	}
	if (!objConvex->isInitialized){
		objConvex->init(gc);
	}
	
}

void FloatingObject::move(GraphicsContext& gc, Move dir) {
	//hasMoved = true;

	//printf("%d ", gc.frameCount);
	//if (dir == FORWARD){
	//	printf("forward  ");
	//} else {
	//	if (dir == BACK){
	//		printf("backward ");
	//	}
	//	else {
	//		printf("         ");
	//	}
	//}
	//if (dir == RIGHT){
	//	printf("right");
	//}
	//if (dir == LEFT){
	//	printf("left ");
	//}
	//printf("\n");

	switch (dir) {
	case FORWARD:
		moveImpuls += front * floatSpeed;
		break;
	case BACK:
		moveImpuls -= front * floatSpeed;
		break;
	case LEFT:
		rotation.z += rotSpeedVec.z;
		break;
	case RIGHT:
		rotation.z -= rotSpeedVec.z;
		break;
	case UP:
		addPosition(glm::vec3(0, 0, -moveStep));
		break;
	case DOWN:
		addPosition(glm::vec3(0, 0, moveStep));
		break;
	}

	movesForward = glm::dot(front, direction) > 0;

}

void FloatingObject::setPosition(vec3 position)
{
	hasMoved = true;
	this->position = position;
	objNormal->setPosition(position);
	objConvex->setPosition(position);
}

void FloatingObject::addPosition(vec3 off){
	setPosition(position + off);
}

void FloatingObject::setModel(Model3D* model, Model3D* modelConvex){
	objNormal = model;
	objConvex = modelConvex;
}

void FloatingObject::setModel(GraphicsContext& gc, FloatingObjectConfig* config){
	for (Model3D* model : gc.models){
		if (config->modelNormal == model->filename){
			objNormal = new Model3D(*model);
		}
		if (config->modelConvex == model->filename){
			objConvex = new Model3D(*model);
		}
	}
}