#ifndef __WRAPPERS_H__
#define __WRAPPERS_H__


#include "myGL.h"
#include "glm.h"	

class DrawableWrapper;

DrawableWrapper* getSceneBoxDW(); 
DrawableWrapper* getTexturePreviewDW();
DrawableWrapper* getReflectionMapDW();
DrawableWrapper* getComputeShaderExample();

void setupSceneBoxDW(); 
void setupTexturePreviewDW();
void setupReflectionMapDW();
void setupComputeShaderExample();
#endif