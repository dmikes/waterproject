#include "ShaderSet.h"

std::string ShaderSet::filepath = "../resources/shaders/";

ShaderSet::ShaderSet(std::string configFile)
{
	configuration = new ShaderConfig(configFile);
}

ShaderSet::ShaderSet( ShaderConfig* config ) : configuration(config)
{

}

void ShaderSet::applyModelViewMatrix(GLfloat* modelView) const
{
	glUniformMatrix4fv(glGetUniformLocation(programId, "u_ModelViewMatrix"), 1, GL_FALSE, modelView);	
}

void ShaderSet::applyProjectionMatrix(GLfloat* projection) const
{
	glUniformMatrix4fv(glGetUniformLocation(programId, "u_ProjectionMatrix"), 1, GL_FALSE, projection);	
}

void ShaderSet::applyModelViewProjectionMatrix(GLfloat* mvp) const
{
	glUniformMatrix4fv(glGetUniformLocation(programId, "u_MVP"), 1, GL_FALSE, mvp);	
}

void ShaderSet::compileShaderProgram()
{
	// Delete shader program if exists
	if(programId){
		glDeleteProgram(programId);
	}
	programId = glCreateProgram();
	printf("Creating shader: [%i] %s \n", programId, configuration->description.c_str() );

	if (configuration->useCompute){
		std::string computeName = ShaderSet::filepath + configuration->filenameCompute;
		GLuint compId = createShaderFromFile(GL_COMPUTE_SHADER, computeName.c_str());
		glAttachShader(programId, compId);
		glDeleteShader(compId);
	} else {
		// Create shader objects for VERTEX shader
		if(configuration->useVertex){
			std::string vertexName = ShaderSet::filepath + configuration->filenameVertex;
			GLuint vertId = createShaderFromFile(GL_VERTEX_SHADER, vertexName.c_str());
			glAttachShader(programId, vertId);
			glDeleteShader(vertId);
		}
		if(configuration->useTessellation){
			std::string tessControlName = ShaderSet::filepath + configuration->filenameTessControl;
			GLuint tessControlId = createShaderFromFile(GL_TESS_CONTROL_SHADER, tessControlName.c_str());
			glAttachShader(programId, tessControlId);
			glDeleteShader(tessControlId);
		}
		if(configuration->useTessellation){
			std::string tessEvalName = ShaderSet::filepath + configuration->filenameTessEval;
			GLuint tessEvalId = createShaderFromFile(GL_TESS_EVALUATION_SHADER, tessEvalName.c_str());
			glAttachShader(programId, tessEvalId);
			glDeleteShader(tessEvalId);
		}
		// Create shader objects for geometry shader
		if(configuration->useGeometry){
			std::string geometryName = ShaderSet::filepath + configuration->filenameGeometry;
			GLuint geomId = createShaderFromFile(GL_GEOMETRY_SHADER, geometryName.c_str());
			glAttachShader(programId, geomId);
			glDeleteShader(geomId);
		}
		// Create shader objects for FRAGMENT shader
		if(configuration->useFragment){
			std::string fragmentName = ShaderSet::filepath + configuration->filenameFragment;
			GLuint fragId = createShaderFromFile(GL_FRAGMENT_SHADER, fragmentName.c_str());
			glAttachShader(programId, fragId);
			glDeleteShader(fragId);
		}
	}
	
	// if function is set
	if(beforeLinkFunc){
		beforeLinkFunc(programId);
	}

	// Link shader program
	glLinkProgram(programId);
	if (!checkProgramLinkStatus(programId)){
		checkProgramInfoLog(programId);
		printf("Shader program creation failed.\n\n");
		glDeleteProgram(programId);
		programId  = 0;
		return;
	}else{
		printf("Shader program compiled successfully.\n\n");
	}
}

int ShaderSet::createShaderFromFile(GLenum shader_type, const char* file_name)
{
	if (file_name == NULL)
	{
		return 0;
	}

	char* buffer = NULL;

	// Read input file
	if (file_name != NULL) 
	{
		FILE* fin = fopen(file_name,"rb");
		if (fin != NULL) 
		{
			fseek(fin, 0, SEEK_END);
			long file_size = ftell(fin);
			rewind(fin);

			if (file_size > 0) 
			{
				buffer = new char[file_size + 1];
				int count = fread(buffer, sizeof(char), file_size, fin);
				//              assert(count == file_size);
				buffer[count] = '\0';
			}
			fclose(fin);
		}
		else
		{
			return 0;
		}
	}

	assert(buffer != NULL);

	GLuint shader_id = createShaderFromSource(shader_type, buffer);
	delete [] buffer;

	return shader_id;
}


GLuint ShaderSet::createShaderFromSource(GLenum shader_type, const char* source)
{
	if (source == NULL)
	{
		return 0;
	}

	switch (shader_type)
	{
		case GL_VERTEX_SHADER         : printf("vertex   shader ... "); break;
		case GL_FRAGMENT_SHADER       : printf("fragment shader ... "); break;
		case GL_GEOMETRY_SHADER       : printf("geometry shader ... "); break;
		case GL_TESS_CONTROL_SHADER   : printf("tesselation control shader    ... "); break;
		case GL_TESS_EVALUATION_SHADER: printf("tesselation evaluation shader ... "); break;
		case GL_COMPUTE_SHADER	      : printf("compute shader ... "); break;
		default                       : return 0;
	}

	GLuint shader_id = glCreateShader(shader_type);
	if (shader_id == 0)
	{
		return 0;
	}

	glShaderSource(shader_id, 1, &source, NULL);
	glCompileShader(shader_id);

	if (checkShaderCompileStatus(shader_id) != GL_TRUE)
	{
		printf("failed.\n");
		checkShaderInfoLog(shader_id);
		glDeleteShader(shader_id);
		return 0;
	}
	else
	{
		printf("successfull.\n");
		return shader_id;
	}
}

void ShaderSet::checkShaderInfoLog(GLuint shader_id)
{
	if (shader_id == 0)
	{
		return;
	}

	int log_length = 0;
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);

	if (log_length > 0)
	{
		char* buffer  = new char[log_length];
		int   written = 0;
		glGetShaderInfoLog(shader_id, log_length, &written, buffer);
		printf("%s\n", buffer);
		delete [] buffer;
	}
}


void ShaderSet::checkProgramInfoLog(GLuint program_id)
{
	if (program_id == 0)
	{
		return;
	}

	int log_length = 0;
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_length);

	if (log_length > 0)
	{
		char* buffer  = new char[log_length];
		int   written = 0;
		glGetProgramInfoLog(program_id, log_length, &written, buffer);
		printf("%s\n", buffer);
		delete [] buffer;
	}
}



GLint ShaderSet::checkShaderCompileStatus(GLuint shader_id)
{
	GLint status = GL_FALSE;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
	return status;
}

GLint ShaderSet::checkProgramLinkStatus(GLuint program_id)
{
	GLint status = GL_FALSE;
	glGetProgramiv(program_id, GL_LINK_STATUS, &status);
	return status;
}