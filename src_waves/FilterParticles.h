#ifndef __FILTER_PARTICLES_H__
#define __FILTER_PARTICLES_H__

#include "Drawable.h"
#include "myGL.h"

class GraphicsContext;
class FloatingObject;

class FilterParticles : public Drawable {
public:
	GLuint surfaceDeviation_FBO;
	
	uint filterKernelSize;

	void winResize(GraphicsContext& gc, int width, int height);
	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);	
	void preprocess(GraphicsContext& gc);
	void filterParticleTexture(GraphicsContext& gc, FloatingObject* floater);
};

#endif