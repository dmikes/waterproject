#include "Stage.h"

#include "Texture2D.h"
#include "Camera.h"
#include "ResourceList.h"

// Write measurement
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "../libs/dirent/dirent.h"

Stage::Stage()
{

}

void Stage::init(GraphicsContext& gc){
	for(DrawableContext& dc : drawables){
		gc.curResources = dc.resources;
		dc.drawable->init(gc);
	}
	
	int nStages = stageNames.size();
	query.resize(nStages);
	timerFrame.resize(nStages);
	timerTotalGPU.resize(nStages);
	timerTotalCPU.resize(nStages);
	std::fill(timerTotalGPU.begin(), timerTotalGPU.end(), 0.0);
	std::fill(timerTotalCPU.begin(), timerTotalCPU.end(), 0.0);

	genQueries();
};


void Stage::winResize( GraphicsContext& gc, int width, int height )
{
	for(Camera* cam : gc.cameras){
		if(cam->isWindowDependent){
			cam->resizeWindow(width, height);
		}
	}
	for(DrawableContext& dc : drawables){
		dc.drawable->winResize(gc, width, height);
	}
}


void Stage::preprocess(GraphicsContext& gc){
	prepareFrame(gc);
	for (DrawableContext& dc : drawables){
		gc.curResources = dc.resources;
		dc.drawable->preprocess(gc);
	}
}


void Stage::prepareFrame(GraphicsContext& gc){
	gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, gc.wireMode ? GL_LINE : GL_FILL);
}

void Stage::display(GraphicsContext& gc){
	prepareFrame(gc);

	for(DrawableContext& dc : drawables){
		gc.curResources = dc.resources;
		
		dc.drawable->display(gc);
		oglCheckError("Scene: after display ");
	}
	gc.frameCount++;
}


void Stage::update(GraphicsContext& gc){
	for(DrawableContext& dc : drawables){
		gc.curResources = dc.resources;
	
		dc.drawable->update(gc);
	}
}


void Stage::addDrawable(std::string stageName, Drawable* d, ResourceList* res)
{
	stageNames.push_back(stageName);
	drawables.push_back(DrawableContext(d, res));
}

void Stage::addDrawable(std::string stageName, Drawable* d)
{
	stageNames.push_back(stageName);
	drawables.push_back(DrawableContext(d, NULL));
}


void Stage::genQueries() {
	
	glGenQueries(2, &checkQuery[0]);
	glQueryCounter(checkQuery[queryFrontBuffer], GL_TIMESTAMP);
	
	for (int i = 0; i < query.size(); i++){
		glGenQueries(2, &query[i][0]);
		// Dummy query to prevent OpenGL errors from popping out
		glQueryCounter(query[i][queryFrontBuffer], GL_TIMESTAMP);
	}
}


void Stage::swapQueryBuffers() {

	if (queryBackBuffer) {
		queryBackBuffer = 0;
		queryFrontBuffer = 1;
	}
	else {
		queryBackBuffer = 1;
		queryFrontBuffer = 0;
	}
}


void Stage::measureUpdate(GraphicsContext& gc){
	
	int i = 0;
	for (DrawableContext& dc : drawables){
		gc.curResources = dc.resources;

		// with waiting. (replace with queryBackBuffer and uncoment swapQueryBuffers to measure interframe time without any waiting)
		glBeginQuery(GL_TIME_ELAPSED, query[i][queryFrontBuffer]); // queryBackBuffer
		double beforeTime = glfwGetTime();

		for (int j = 0; j < measureRepeat; j++){
			dc.drawable->update(gc);
		}
		double afterTime = glfwGetTime();
		glEndQuery(GL_TIME_ELAPSED);
		glGetQueryObjectui64v(query[i][queryFrontBuffer], GL_QUERY_RESULT, &timerFrame[i]);


		timerTotalGPU[i] += static_cast<double>(timerFrame[i] * 1e-6); // nanosec -> milisec	
		timerTotalCPU[i] += (afterTime - beforeTime) * 1e3;	// sec -> milisec
		i++;
	}
}

void Stage::measure(GraphicsContext& gc)
{
	
	prepareFrame(gc);

	int i = 0;
	for (DrawableContext& dc : drawables){
		gc.curResources = dc.resources;
		
		// with waiting. (replace with queryBackBuffer and uncoment swapQueryBuffers to measure interframe time without any waiting)
		glBeginQuery(GL_TIME_ELAPSED, query[i][queryFrontBuffer]); // queryBackBuffer
		double beforeTime = glfwGetTime();
		
		for (int j = 0; j < measureRepeat; j++){
			dc.drawable->display(gc);
		}
		//glFinish();
		double afterTime = glfwGetTime();
		glEndQuery(GL_TIME_ELAPSED);
		glGetQueryObjectui64v(query[i][queryFrontBuffer], GL_QUERY_RESULT, &timerFrame[i]);
		

		timerTotalGPU[i] += static_cast<double>(timerFrame[i] * 1e-6); // nanosec -> milisec	
		timerTotalCPU[i] += (afterTime - beforeTime)* 1e3;	// sec -> milisec
		i++;
	}
	gc.frameCount++;
	nMeasuredFrames++;

	// ----- end of the frame -----
	//swapQueryBuffers();
	//printf("Time spent on the GPU: %f ms\n", timerFrame[0] / 1000000.0);
}



void Stage::writeMeasurementResults(GraphicsContext& gc, std::string folder, std::string customInput){

	int fileCount = 0;

	// Count the number of files in the folder
	DIR *dpdf;
	struct dirent *epdf;
	dpdf = opendir(folder.c_str());
	if (dpdf != NULL){
		while (epdf = readdir(dpdf)){
			if (!strcmp(epdf->d_name, ".") || !strcmp(epdf->d_name, "..")){
				continue;    // skip self and parent
			}
			fileCount++;
		}
	}

	// Sum the time from different stages
	int nAddents = measureRepeat * nMeasuredFrames;
	double totalGPU = 0.0;
	double totalCPU = 0.0;
	for (int i = 0; i < timerTotalGPU.size(); i++){
		totalGPU += timerTotalGPU[i];
		totalCPU += timerTotalCPU[i];
	}

	// Store the test results into the file
	std::string file = folder + "test" + std::to_string(fileCount) + ".txt";
	std::ofstream saveFile(file.c_str());
	saveFile << gc.simConf->filename << " + " << customInput << std::endl;
	saveFile << "avgFPS: " << gc.avgFramesPerSec << std::endl;
	saveFile << "totalGPU: " << totalGPU << " (" << totalGPU / nAddents << ") " << 1000.0f / (totalGPU / nAddents) << " FPS" << std::endl;
	saveFile << "totalCPU: " << totalCPU << " (" << totalCPU / nAddents << ") " << 1000.0f / (totalCPU / nAddents) << " FPS" << std::endl;
	saveFile << "Frames: " << nMeasuredFrames << " Repeat: " << measureRepeat << std::endl;
	
	for (int i = 0; i < stageNames.size(); i++){
		//saveFile << stageNames[i] << ";" << (timerTotalGPU[i] / nAddents) << std::endl;
		//saveFile << stageNames[i] << ";" << (timerTotalGPU[i] / nAddents) /*<< ";" << (timerTotalCPU[i] / nAddents)*/ << std::endl;
		saveFile <<  (timerTotalGPU[i] / nAddents) << std::endl;
	}

	saveFile.close();
}



