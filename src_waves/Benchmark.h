#ifndef __BENCHMARK_H__
#define __BENCHMARK_H__

#include "Drawable.h"


class Benchmark : public Drawable {
	Drawable* meassuredEvent;

	void registerDrawable(Drawable* d);

	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);	
};

#endif // !__BENCHMARK_H__
