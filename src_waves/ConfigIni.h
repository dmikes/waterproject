#ifndef __CONFIG_INI_H__
#define __CONFIG_INI_H__

#include <iostream>

#include "utils.h"
#include "../libs/inih/cpp/INIReader.h"
#include "glm.h"

struct Config {
public:
	std::string filename;
protected:
	Config(std::string filename);
	bool fileExists(std::string filename);

	vec2 getVec2(INIReader& reader, const char* block);
	vec3 getVec3(INIReader& reader, const char* section );
	vec4 getVec4(INIReader& reader, const char* section );
	ivec2 getIVec2(INIReader& reader, const char* section );
	ivec3 getIVec3(INIReader& reader, const char* section );
	ivec4 getIVec4(INIReader& reader, const char* section );
};



// ===== Camera Config =====
struct CameraConfig : public Config {
	ivec2 winDim;
	vec3 position;
	vec3 rotation;
	bool isPerspective;
	bool hasInertia;
	bool isWindowDependent;
	float planeNear;
	float planeFar;
	struct {
		float left, right, bottom, top;
	} ortho;
	struct {
		float fov, aspectRatio;
	} persp;
	

	CameraConfig(std::string filename);
	void parse();
};



// ===== Simulation Config =====
struct SimulationConfig : public Config {
	uint simStepsPerSec = 60;	// number of frames
	int runUntil = -1;			// number of frames
	bool doRunUntil = false;

	struct Sim_WaveParticlesConfig {
		float speed;
		float waveDampening;
		float particleWidth;
		uint filterKernelSize;
		uint nMaxParticles;
		float minDispersionAngle;
	} mWaveParticle;

	struct Sim_NoiseConfig {
		float speed;
		float strength;
		float scale;
	} mNoise;

	struct Sim_TessellationConfig {
		bool useTessellation;
		int minTessLevel;
		int maxTessLevel;
		float minTessDistance;
		float maxTessDistance;
	} mTess;

	struct Sim_WaterSurface {
		float horizontalCoef;
		float verticalCoef;
	} m_Surface;

	bool useBoundaries;
	vec2 waveDepthReduce;

	ivec2 texSize; // render particles to texture

	ivec2 gridVertexCount;
	vec2 waterSize;
	vec3 waterOrigin;

	float texRayDistorition;
	float fresnelR0;
	float fresnelPow;
	float waterClarity;
	float reflectDarkening;
	bool useWaterHole;
	vec4 waterColor;

	SimulationConfig(std::string filename);
	void customSceneOverride(SimulationConfig& child);
};


// ===== Shader Config =====
struct ShaderConfig : public Config {
	bool useVertex;
	bool useGeometry;
	bool useFragment;
	bool useTessellation;
	bool useCompute;
	std::string description;
	std::string filenameVertex;
	std::string filenameGeometry;
	std::string filenameFragment;
	std::string filenameTessControl;
	std::string filenameTessEval;
	std::string filenameCompute;
	

	ShaderConfig(std::string filename);
};

// ===== Model Config =====
class TransformationConfig : public Config {
public:
	vec3 translation;
	vec3 rotation;
	vec3 scale;

	TransformationConfig(std::string filename);
};

// ===== Floating object Config =====
class FloatingObjectConfig : public Config {
public:
	int modelType;
	std::string modelNormal;
	std::string modelConvex;
	std::string transformation;
	std::string silhouetteCamera;
	std::string waveParticleCamera;

	float density;
	float swayReduce;
	float waveStrength;
	float floatSpeed;
	float moveStep;
	float dragCoef;
	float liftCoef;
	float meshProjectCoef;
	float volumeCumulationCoef;
	float rollRestore;
	float rollMax;
	float pitchMax;
	float pitchMaxSpeed;
	float maxWaveSlopeX;
	float maxWaveSlopeY;
	float rotationSpeed;
	vec3 rotSpeedVec;

	FloatingObjectConfig(std::string filename);
};


// ===== Camera Config =====
struct PathConfig : public Config {
	std::string images;
	std::string shaders;
	std::string models;

	std::string transforms;
	std::string cameraConfig;
	std::string shaderConfig;
	std::string sceneConfig;
	std::string floaterConfig;
	std::string waterSimulationConfig;



	PathConfig(std::string filename);
	void parse();
};

#endif