#ifndef __NOISE_GENERATOR__
#define __NOISE_GENERATOR__ 

#include "myGL.h"
#include "Drawable.h"
#include "Camera.h"
class GraphicsContext;

class NoiseGenerator {
	vec2 dirs[256];
	
public:	
	void preprocess(GraphicsContext& gc);

	float fBm(float x, float y, int per, int octs);
	float fBm(float x, float y, float z, int per, int octs);
	//void genTilableNoiseCPU(GraphicsContext& gc);
	void genTilableNoiseCPU(GraphicsContext& gc, int size = 64, float freq = 1 / 32.f, int octs = 5);
private:
	byte getPixelRepeated(std::vector<byte>& image, int x, int y, int width, int height);
	float getPixelRepeated(std::vector<glm::vec4>& image, int x, int y, int width, int height);
	glm::vec4 getNormal(std::vector<glm::vec4>& image, int x, int y, int width, int height);
};








#endif // __NOISE_GENERATOR__
