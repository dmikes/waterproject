#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <array>

#include "myGL.h"

#include "WaterSimApplication.h"

using namespace std;

// === Typedefs ===
typedef GraphicsContext GC;
// Function pointer types
typedef void (* TInitGLCallback)(void);
typedef void (* TDisplayCallback)(void);

// === File scope declaration ===
WaterSimApplication* app;
GLFWwindow* mainWindow;

// === Callbacks ===
/**
 * Recompile shaders callback
 * \param[in] clientData Arbitrary input data
 */
void TW_CALL cbCompileShaderProgram(void *clientData){
	app->compileShaders();
}

/**
 * Window resized callback
 * \param[in] window Window which is responcible for handling the user input
 * \param[in] width New width of the window
 * \param[in] height New heigh of the window
 */
void cbWindowSizeChanged(GLFWwindow* window, int width, int height) {
	app->windowSizeChanged(width, height);
	TwWindowSize(width, height);
}


/**
 * Keyboard changed its state callback
 * \param[in] window Window which is responcible for handling the user input
 * \param[in] key Key code
 * \param[in] scancode Scancode
 * \param[in] action Whether it was pressed or released
 * \param[in] mods Special key states.
 */
void cbKeyboardChanged(GLFWwindow* window, int key, int scancode, int action, int mods) {
	app->keyboardChanged(key, scancode, action, mods);
}


/**
 * A 'mouse position changed' callback.
 * \param[in] window Current GLFW window.
 * \param[in] x A mouse position x coordinate.
 * \param[in] y A mouse position y coordinate.
 */
void cbMousePositionChanged(GLFWwindow* window, double x, double y) {
	int ix = static_cast<int>(x);
	int iy = static_cast<int>(y);
	TwEventMousePosGLFW(ix, iy);
	app->mousePositionChanged(ix, iy);
}

/**
 * A 'mouse button changed' callback.
 * \param[in] window Current GLFW window.
 * \param[in] button Mouse button.
 * \param[in] action Whether the button was pressed or released.
 * \param[in] mods Special key states.
 */
void cbMouseButtonChanges(GLFWwindow* window, int button, int action, int mods){
	TwEventMouseButtonGLFW(button, action);
	app->mouseButtonChanged(button, action);
}


#ifdef _WIN32
void APIENTRY openglCallbackFunction(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam) {
#elif __linux__
void APIENTRY openglCallbackFunction(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	void* userParam) {
#endif

	std::cout << "---------------------opengl-callback-start------------" << std::endl;
	std::cout << "message: " << message << std::endl;
	std::cout << "type: ";
	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		std::cout << "ERROR";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		std::cout << "DEPRECATED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		std::cout << "UNDEFINED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		std::cout << "PORTABILITY";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		std::cout << "PERFORMANCE";
		break;
	case GL_DEBUG_TYPE_OTHER:
		std::cout << "OTHER";
		break;
	}
	std::cout << std::endl;

	std::cout << "id: " << id << std::endl;
	std::cout << "severity: ";
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:
		std::cout << "LOW";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		std::cout << "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_HIGH:
		std::cout << "HIGH";
		break;
	}
	std::cout << std::endl;
	//std::cout << "---------------------opengl-callback-end--------------" << std::endl;
}

/**
 * GLFW Error callback.
 * \param[out] error Erroc code.
 * \param[out] description Error description message.
 */
static void error_callback(int error, const char* description) {
	fputs(description, stderr);
}


int run(const char* window_title, 
		int* opengl_config,
        TInitGLCallback			cbUserInitGL,
        TDisplayCallback        cbUserDisplay,
        GLFWwindowsizefun		cbUserWindowSizeChanged,
        GLFWkeyfun				cbUserKeyboardChanged,
        GLFWmousebuttonfun		cbUserMouseButtonChanged,
        GLFWcursorposfun		cbUserMousePositionChanged,
		GLFWwindowclosefun		cbWindowClose
) {
    // Setup user callback functions
    assert(cbUserDisplay && cbUserInitGL);

    // Intialize GLFW   
    glfwInit();
    bool bDebugOutput = false;

	printf("GLEW \t\t %s\n", glewGetString(GLEW_VERSION));
	printf("GLFW \t\t %s\n", glfwGetVersionString());
	printf("AntTweakBar \t %d\n", TW_VERSION);
	printf("GLM \t\t %d\n", GLM_VERSION);
	//printf("Assimp \t\t %d.%d.%d\n", aiGetVersionMajor(), aiGetVersionMinor(), aiGetVersionRevision());

	// Get info about the main camera from the configuration file via WaterSimApplication
	CameraConfig mainCamConfig = CameraConfig(app->paths.cameraConfig + "PerspMain.ini");

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	mainWindow = glfwCreateWindow(mainCamConfig.winDim.x, mainCamConfig.winDim.y, window_title, NULL, NULL);
	if (!mainWindow) {
		printf("Error while creating new window\n");
		glfwTerminate();
	}
	glfwSetWindowPos(mainWindow, 600, 30);
	glfwMakeContextCurrent(mainWindow);

	cbWindowSizeChanged(mainWindow, mainCamConfig.winDim.x, mainCamConfig.winDim.y);
    
	GLuint err = glewInit();
	if (err != GLEW_OK) {
		printf("Unable to init GLEW: %s\n", glewGetErrorString(err));
	}

	if (glDebugMessageCallback) {
		std::cout << "OpenGL debug callback was successfully registered." << std::endl;
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(openglCallbackFunction, NULL);
		GLuint unusedIds = 0;
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, &unusedIds, true);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, false);
		//GLuint ignore[] = { 131185, 131186 };
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_PERFORMANCE, GL_DEBUG_SEVERITY_MEDIUM, 0, NULL, false);
	} else {
		std::cout << "glDebugMessageCallback not available" << std::endl;
	}

    // Init OGL
    if (cbUserInitGL){
        cbUserInitGL();
    }

    // Set GLFW event callbacks
    glfwSetWindowSizeCallback(mainWindow, cbUserWindowSizeChanged);
    glfwSetKeyCallback(mainWindow, cbUserKeyboardChanged);
    glfwSetMouseButtonCallback(mainWindow, cbUserMouseButtonChanged);
    glfwSetCursorPosCallback(mainWindow, cbUserMousePositionChanged);
	glfwSetWindowCloseCallback(mainWindow, cbWindowClose);
	// Main loop
	while (!glfwWindowShouldClose(mainWindow) && !app->quitProgram)
    {
        if (cbUserDisplay){
            cbUserDisplay();
        }

        TwDraw(); // Draw tweak bars
        glfwSwapBuffers(mainWindow); // Present frame buffer
		glfwPollEvents();
    }
 
    TwTerminate(); // Terminate AntTweakBar
    glfwTerminate();    // Terminate GLFW

    return 0;
} 

/**
 * Display callback
 */
void cbDisplay(){
	app->display(mainWindow);
}

/**
 * Initialization callback
 */
void cbInitGL(){
	app->init();
}

/**
 * Window closed routine callback
 * \param[in] window Current window
 */
void cbWindowClosed(GLFWwindow* window)
{}

/**
 * A main entry point of the application.
 * \param[int] argc Number of parameters
 * \param[out] argv Input parameters
 */
int main(int argc, char* argv[]) {
	app = new WaterSimApplication(argc, argv);
	app->setGUICompileShaders(cbCompileShaderProgram);


	run("Water Project", NULL, 
		cbInitGL, cbDisplay, cbWindowSizeChanged, cbKeyboardChanged, cbMouseButtonChanges, cbMousePositionChanged, cbWindowClosed);
	return 0;
}
