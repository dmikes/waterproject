#ifndef __MODEL_3D__
#define __MODEL_3D__

#include "glm.h"

#include "GraphicsContext.h"
#include "Drawable.h"
#include "MeshVBO.h"
#include <string>
#include "Transformation3D.h"

class Texture;


class Model3D : public Drawable {
private:
	bool fromFile;
	GLuint drawMode;
	MeshVBO* load();
public:
	bool worldBBPersistent; // world bounding box has been updated this frame
	std::string filename;
	std::string textureFolder;
	MeshVBO* mesh = NULL;
	MeshVBO* meshCentroids = NULL;
	bool doMipmap = false;
	std::vector<std::string> texNames;
	std::vector<Texture*> textures;
	std::vector<GLuint> meshVertixCount;
	uint nTex = 0;

	Texture* diffuseTex = NULL;
	vec3 modelOffset;
	//vec3 centerOfMass;
	//vec3 centerOfObject;
	vec3 position = vec3(0, 0, 0);
	vec3 rotation;
	bool displayBB = false;
	bool useCentoids = false;

	struct BoudingBox {
		vec3 mMin;
		vec3 mMax;
		vec3 mCenter;
		void transform(mat4 model);
	};
	BoudingBox modelBB;
	BoudingBox worldBB;
	
	mat4 modelMat;
	Transformation3D staticTransform;	// default model shift, etc..
	Transformation3D dynamicTransform;  // current position, etc..


	Model3D();
	Model3D(const Model3D& copy);
	Model3D(MeshVBO* mesh, bool loadCentoids = false);
	Model3D(std::string filename, bool loadCentoids = false);
	Model3D(std::string filename, const char* textureFolder, bool mipmap = false); // if string then string literals is preferable converted into bool
	Model3D(std::string filename, const char* textureFolder, bool mipmap, bool loadCentroids);
	~Model3D();

	//void copyMesh(Model3D& model);
	void updateWorldBB();

	void setDrawMode(GLuint drawMode);
	void setPosition(vec3 pos);
	void setDefaultTransform(Transformation3D trans);

	Texture* getDiffuseTex() const;
	void setDiffuseTex(Texture* val);
	
	void winResize(GraphicsContext& gc, int width, int height);
	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);	
	void preprocess(GraphicsContext& gc);
};


#endif