#include "Camera.h"
#include "FloatingObject.h"
#include "Model3D.h"

const float inertia = 0.03f; //mouse inertia
const float rotateSpeed = 0.2f; //mouse rotate speed (sensitivity)
const float walkSpeed = 5.0f; //walking speed (wasd)


//Camera::Camera(CameraConfig* config) : freeWalk(true) {
Camera::Camera(std::string configFile) : freeWalk(true) {
	config = new CameraConfig(configFile);
	resetView();
	filename = std::string(config->filename);
	type = config->isPerspective ? ProjectionType::PERSPECTIVE : ProjectionType::ORTHOGRAPHIC;
	isWindowDependent = config->isWindowDependent;
	planeNear = config->planeNear;
	planeFar = config->planeFar;
	hasInertia = config->hasInertia;
	if(!isWindowDependent){
		winDim = config->winDim;
	}

	if(type == ProjectionType::PERSPECTIVE){
		persp.fov = config->persp.fov;
		persp.aspectRatio = config->persp.aspectRatio;
	} else { // ORTHOGRAPHIC
		ortho.viewMin.x = 0.0f; // left
		ortho.viewMin.y = 0.0f; // right
		ortho.viewMax.x = config->ortho.right - config->ortho.left;
		ortho.viewMax.y = config->ortho.top   - config->ortho.bottom;
	}


	resizeWindow(config->winDim.x, config->winDim.y);
	rotate(0,0); // compute forward vector
	updateCameraViewMatrix(); // initial transformation
	updateProjection();
}


void Camera::resetView() {
	position = config->position;
	rotation = config->rotation;
	if(type == ProjectionType::ORTHOGRAPHIC){
		position -= - config->ortho.left;
		rotation -= - config->ortho.bottom;
	}
	positionLag = position;
	rotationLag = rotation;
}

void Camera::resizeWindow(int winWidth, int winHeight) {
	winDim = glm::vec2(winWidth, winHeight);
	updateProjection();
}

void Camera::updateProjection(){

	if(type == ProjectionType::PERSPECTIVE){
		if(isWindowDependent){
			projectionMatrix = glm::perspective(persp.fov, (float)winDim.x / winDim.y, planeNear, planeFar);
		} else {
			projectionMatrix = glm::perspective(persp.fov, persp.aspectRatio, planeNear, planeFar);
		}
	} else { // ORTHOGRAPHIC
		if(isWindowDependent){
			projectionMatrix = glm::ortho(0.0f, (float)winDim.x, 0.0f, (float)winDim.y, planeNear, planeFar);
		} else {
			projectionMatrix = glm::ortho(ortho.viewMin.x, ortho.viewMax.x, ortho.viewMin.y, ortho.viewMax.x, planeNear, planeFar);
		}
	}
}


void Camera::updateCameraViewMatrix() {
	//if(!freeWalk) {
	//	return;
	//}

	if (followee != NULL){
		//forwardView = followee->front;
		vec3 pos = followee->position - followDistance * followee->front;
		pos.z = followHeight;
		setPosition(pos);
		rotation.x = -90 + cos(followHeight / glm::distance(followee->position + followee->objNormal->worldBB.mMin, position));
		//rotation.x = cos(followHeight / glm::distance(followee->position + followee->objNormal->worldBB.mMin, position));
		rotation.z = 90 - followee->rotation.z;
	}
	
	//camera inertia
	if(hasInertia){
		positionLag += (position - positionLag) * inertia;
		rotationLag += (rotation - rotationLag) * inertia;
	} else {
		positionLag = position;
		rotationLag = rotation;
	}

	// view transform
	modelViewMatrix = glm::rotate(glm::mat4(1.0f), rotationLag.x, X_AXIS);
	modelViewMatrix = glm::rotate(modelViewMatrix, rotationLag.z, Z_AXIS);
	modelViewMatrix = glm::rotate(modelViewMatrix, rotationLag.y, Y_AXIS); // rotation by Up vector
	modelViewMatrix = glm::translate(modelViewMatrix, -positionLag);
}

void Camera::move(Move dir) {
	switch(dir) {
	case BACK:
		position -= forwardView * walkSpeed;
		break;
	case FORWARD:
		position += forwardView * walkSpeed;
		break;
	case LEFT:
		position -= rightView * walkSpeed;
		break;
	case RIGHT:
		position += rightView * walkSpeed;
		break;
	case UP:
		position -= Z_AXIS * walkSpeed;
		break;
	case DOWN:
		position += Z_AXIS * walkSpeed;
		break;
	}
}

void Camera::rotate(float diffX, float diffY) {
	rotation.x += diffY * rotateSpeed; 
	if(rotation.x < -180.0f) rotation.x = -180.0f;
	if(rotation.x > 0.0f) rotation.x = 0.0f;

	rotation.z += diffX * rotateSpeed; 
	//if(rotation.z > 360.0f) rotation.z -= 360.0f;
	//if(rotation.z < 0.0f)   rotation.z += 360.0f;

	// Rotation by Z axis
	forwardView.x = sin(glm::radians(rotation.z)); 
	forwardView.y = cos(glm::radians(rotation.z));
	// If flying would be added, forward vector wouldn't be normalized [normalization is not spherical].
	// So whole polar to XYZ conversion must be added

	// both vectors are already normalized
	rightView = glm::cross(forwardView, Z_AXIS);
}

void Camera::rotateUpVec(float angle) {
	rotation.y += angle * rotateSpeed;
}

void Camera::setCameraParams(const glm::mat3 &rot, const glm::vec3 &trans) {
	modelViewMatrix[3] = glm::vec4(trans, 1.0f);

	modelViewMatrix[0] = glm::vec4(rot[0], 0.0f);
	modelViewMatrix[1] = glm::vec4(rot[1], 0.0f);
	modelViewMatrix[2] = glm::vec4(rot[2], 0.0f);	
}

void Camera::applyViewport(){
	glViewport(0, 0, winDim.x, winDim.y);
}

void Camera::switchFreewalk() {
	freeWalk = !freeWalk;
}

bool Camera::isCameraStatic() const {
	return !freeWalk;
}

const glm::mat4& Camera::getModelViewMatrix() const {
	return modelViewMatrix;
}

const float* Camera::getModelViewMatrixPointer() const {
	return &modelViewMatrix[0][0];
}

const glm::mat4& Camera::getProjectionMatrix() const {
	return projectionMatrix;
}

const float* Camera::getProjectionMatrixPointer() const {
	return &projectionMatrix[0][0];
}

const glm::vec3& Camera::getPosition() const {
	return position;
}

const glm::vec3 Camera::getViewDirection() const {
	return glm::vec3(modelViewMatrix[0][2], modelViewMatrix[1][2], modelViewMatrix[2][2]);
}

const glm::ivec2& Camera::getWindowSize() const {
	return winDim;
}


void Camera::follow(FloatingObject* floater){
	followee = floater;
	
	if (floater == NULL){
		freeWalk = true;
		//forwardView = forwardViewPrevious;
	} else {
		freeWalk = false;
	}
	// in order to restore 
	//forwardViewPrevious = forwardView;
}

void Camera::setPosition(glm::vec3 val)
{
	position = val;
}
