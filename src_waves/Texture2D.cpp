#include "Texture2D.h"
#include "devil.h"

MeshVBO Texture::sprite = MeshVBO();
bool Texture::spriteInitialized = false;  // dont know if sprite has been initialized before (dont use sprite.isInitialized)
int Texture::nMaxTexUnits = -1;
int Texture::nTexUnits = 1; // reserve texture unit 0 for rebinding


Texture::Texture() : texID(0)
{ 
}

void Texture::init3D(GLenum target, GLuint minFilter, GLuint magFilter, GLuint wrap){
	this->target = target;
	textureUnit = 0;

	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_3D, texID);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, magFilter);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, wrap);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, wrap);
	
}

void Texture::allocate3D(int width, int height, int depth, GLint internalFormat,  GLenum inputFormat, GLenum dataType){
	this->width = width;
	this->height = height;
	this->depth = depth;
	glTexImage3D(target, 0, internalFormat, width, height, depth, 0, inputFormat, dataType, NULL);
}


void Texture::init(GLenum target, GLuint filter, GLuint wrap){
	if(texID) glDeleteTextures(1, &texID);
	
	this->target = target;
	textureUnit = 1;

	//mag filter: GL_NEAREST, GL_LINEAR
	//min filter: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR
	GLenum magFilter;
	if (filter == GL_NEAREST || filter == GL_NEAREST_MIPMAP_NEAREST || filter == GL_NEAREST_MIPMAP_LINEAR){
		magFilter = GL_NEAREST;
	} else {
		magFilter = GL_LINEAR;
	}

	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(target, texID);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter);
	
	if (target == GL_TEXTURE_1D){
		glTexParameteri(target, GL_TEXTURE_WRAP_S, wrap);
	}
	if (target == GL_TEXTURE_2D){ // TODO: this applies even for cubemap (change it)
		glTexParameteri(target, GL_TEXTURE_WRAP_S, wrap);
		glTexParameteri(target, GL_TEXTURE_WRAP_T, wrap);
	}
	if (target == GL_TEXTURE_CUBE_MAP || target == GL_TEXTURE_3D){
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrap);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrap);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrap);
	}
	
	if(!Texture::spriteInitialized){
		Texture::sprite.setVertices(4, (GLfloat*)unitPlaneVertices);
		Texture::sprite.setNormals((GLfloat*)unitPlaneNormals);
		Texture::sprite.setTexCoords((GLfloat*)unitPlaneTexCoords);
		// If it was in constructor, it would trigger before opengl initialization
		Texture::sprite.init();
		Texture::spriteInitialized = true;
	}
}

void Texture::loadImage(
	std::string filename, GLint internalFormat, GLenum inputFormat, GLenum dataType)
{
	byte* ptr = NULL;
	Texture::decodeImage(filename, width, height, ptr);

	bind(0);
	if (target == GL_TEXTURE_1D){
		glTexImage1D(target, 0, internalFormat, width, 0, inputFormat, dataType, ilGetData());//&image[0]);
	} else {
		glTexImage2D(target, 0, internalFormat, width, height, 0, inputFormat, dataType, ilGetData());//&image[0]);
	}
}

void Texture::loadImageCubemap(
	std::string path, std::array<std::string, 6> filenames, std::string ext, GLint internalFormat, GLenum inputFormat, GLenum dataType)
{
	for (int i = 0; i < 6; ++i){
		std::string filename = path + filenames[i] + ext;

		std::vector<byte> image;
		uint width = 0, height = 0; // out references

		byte* ptr = NULL;
		Texture::decodeImage(filename, width, height, ptr);
		// other cube map sides have continuos ordering:
		// POSITIVE_X, NEGATIVE_X, POSITIVE_Y, NEGATIVE_Y, POSITIVE_Z, NEGATIVE_Z
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, width, height, 0, inputFormat, dataType, ilGetData());
	}
}

void Texture::allocate(int width, int height, GLint internalFormat, GLenum inputFormat, GLenum dataType){
	allocate(ivec2(width, height), internalFormat, inputFormat, dataType);
}


void Texture::allocate(ivec2 size, GLenum internalFormat,  GLenum inputFormat, GLenum dataType)
{
	bind();
	this->width = size.x;
	this->height = size.y;
	glTexImage2D(target, 0, internalFormat, size.x, size.y, 0, inputFormat, dataType, NULL);
}

void Texture::allocateCubemap(ivec2 size, GLint internalFormat, GLenum inputFormat, GLenum dataType)
{
	width  = size.x;
	height = size.y;
	for(int i = 0; i < 6; ++i){
		// order: POSITIVE_X, NEGATIVE_X, POSITIVE_Y, NEGATIVE_Y, POSITIVE_Z, NEGATIVE_Z
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, width, height, 0, inputFormat, dataType, NULL);
	}
}

void Texture::display(GraphicsContext& gc, GLint x, GLint y, GLsizei wight, GLsizei height){
	mat4 modelView = gc.getModelViewMatrix();
	modelView = glm::translate(modelView, glm::vec3(x, y, 0));
	modelView = glm::scale(modelView, glm::vec3(wight, height, 0));
	gc.applyModelViewMatrix(&modelView[0][0]);
	gc.applyProjectionMatrix();
	
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(target, texID);
	glUniform1i(gc.getUniformLocation("u_Dimensions"), 2);
	glUniform1i(gc.getUniformLocation("u_InputTexture2D"), 1);
	//bind(textureUnit);
	Texture::sprite.draw_nonIndexed(GL_QUADS);
}

void Texture::display3D(GraphicsContext& gc, GLint x, GLint y, GLsizei wight, GLsizei height){
	mat4 modelView = gc.getModelViewMatrix();
	modelView = glm::translate(modelView, glm::vec3(x, y, 0));
	modelView = glm::scale(modelView, glm::vec3(wight, height, 0));
	gc.applyModelViewMatrix(&modelView[0][0]);
	gc.applyProjectionMatrix();

	glUniform1i(gc.getUniformLocation("u_Dimensions"), 3);
	glUniform1i(gc.getUniformLocation("u_InputTexture3D"), textureUnit);
	bind(textureUnit);
	Texture::sprite.draw_nonIndexed(GL_QUADS);
}

void Texture::bind(GLuint texUnit)
{
	textureUnit = texUnit;
	glActiveTexture(GL_TEXTURE0 + texUnit);
	glBindTexture(target, texID);
}

void Texture::bind()
{
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(target, texID);
}

void Texture::unbind(){
	glActiveTexture(GL_TEXTURE0);
}

uint Texture::getDim1()
{
	return width;
}

ivec2 Texture::getDim2()
{
	return ivec2(width, height);
}

ivec3 Texture::getDim3()
{
	return ivec3(width, height, depth);
}

void Texture::decodeImage(std::string filename, uint& width, uint& height, byte* dataPtr){
	uint ilImg;
	ilGenImages(1, &ilImg);

	ilBindImage(ilImg);
	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_UPPER_LEFT);
	ILboolean success = ilLoadImage((ILstring)filename.c_str());

	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

	width = ilGetInteger(IL_IMAGE_HEIGHT);
	height = ilGetInteger(IL_IMAGE_WIDTH);
	//dataPtr = ilGetData();
}