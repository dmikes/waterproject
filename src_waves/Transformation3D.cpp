#include "Transformation3D.h"

#include "ConfigIni.h"

Transformation3D::Transformation3D(){
};

Transformation3D::Transformation3D(std::string configFile){
	TransformationConfig config = TransformationConfig(configFile);
	filename = configFile;
	translation = config.translation;
	rotation = config.rotation;
	scale = config.scale;
};


Transformation3D::Transformation3D(TransformationConfig* config){
	filename	= std::string(config->filename);
	translation = config->translation;
	rotation	= config->rotation;
	scale		= config->scale;
};

void Transformation3D::merge(Transformation3D& other){
	translation += other.translation;
	rotation    += other.rotation;
	scale		*= other.scale;
}
	
void Transformation3D::applyTRSToMatrix(mat4& mat){
	mat = glm::translate(mat, translation);
	mat = glm::rotate(mat, rotation.x, X_AXIS);
	mat = glm::rotate(mat, rotation.y, Y_AXIS);
	mat = glm::rotate(mat, rotation.z, Z_AXIS);
	mat = glm::scale(mat, scale);
}

void Transformation3D::applySTRToMatrix(mat4& mat){
	mat = glm::scale(mat, scale);
	mat = glm::translate(mat, translation);
	mat = glm::rotate(mat, rotation.x, X_AXIS);
	mat = glm::rotate(mat, rotation.y, Y_AXIS);
	mat = glm::rotate(mat, rotation.z, Z_AXIS);
}

void Transformation3D::applySRTToMatrix(mat4& mat){
	mat = glm::scale(mat, scale);
	mat = glm::rotate(mat, rotation.x, X_AXIS);
	mat = glm::rotate(mat, rotation.y, Y_AXIS);
	mat = glm::rotate(mat, rotation.z, Z_AXIS);
	mat = glm::translate(mat, translation);
}

void Transformation3D::applySRzTToMatrix(mat4& mat){
	mat = glm::scale(mat, scale);
	mat = glm::rotate(mat, rotation.z, Z_AXIS);
	mat = glm::translate(mat, translation);
}


void Transformation3D::applyScaleToMatrix(mat4& mat){
	mat = glm::scale(mat, scale);
}

void Transformation3D::applyRotationToMatrix(mat4& mat){
	mat = glm::rotate(mat, rotation.x, X_AXIS);
	mat = glm::rotate(mat, rotation.y, Y_AXIS);
	mat = glm::rotate(mat, rotation.z, Z_AXIS);
}

void Transformation3D::applyRotationXToMatrix(mat4& mat){
	mat = glm::rotate(mat, rotation.x, X_AXIS);
}
void Transformation3D::applyRotationYToMatrix(mat4& mat){
	mat = glm::rotate(mat, rotation.y, Y_AXIS);
}
void Transformation3D::applyRotationZToMatrix(mat4& mat){
	mat = glm::rotate(mat, rotation.z, Z_AXIS);
}

void Transformation3D::applyTranslationToMatrix(mat4& mat){
	mat = glm::translate(mat, translation);
}

vec3 Transformation3D::getScale() const { 
	return scale; 
}

void Transformation3D::setScale(vec3 val) { 
	scale = val; 
}

void Transformation3D::setScale(vec2 val) {
	scale = vec3(val, 1.f);
}


vec3 Transformation3D::getTranslation() const { 
	return translation; 
}

void Transformation3D::setTranslation(vec3 val) {
	translation = val; 
}

vec3 Transformation3D::getRotation() const { 
	return rotation; 
}

void Transformation3D::setRotation(vec3 val) {
	rotation = val; 
}

void Transformation3D::setRotation(float val, E_Axis axis){
	rotation[axis] = val;
}