#ifndef __SHADERS_H__
#define __SHADERS_H__

#include <string>
#include <functional> //std::function
#include "glm.h"
#include "myGL.h"
#include "ConfigIni.h"

class ShaderSet {
public:
	static std::string filepath;
	GLint programId = 0;
	ShaderConfig* configuration;

	std::function<void(GLint)> beforeLinkFunc;
	ShaderSet(ShaderConfig* config);
	ShaderSet(std::string configFile);

	void applyModelViewMatrix(GLfloat* modelView) const;
	void applyProjectionMatrix(GLfloat* projectionIndex) const;
	void applyModelViewProjectionMatrix(GLfloat* mvp) const;

	// courtesy of David Ambro� from the course PGR2
	void compileShaderProgram();
	int createShaderFromFile(GLenum shader_type, const char* file_name);
	GLuint createShaderFromSource(GLenum shader_type, const char* source);
	void checkShaderInfoLog(GLuint shader_id);
	void checkProgramInfoLog(GLuint program_id);
	GLint checkShaderCompileStatus(GLuint shader_id);
	GLint checkProgramLinkStatus(GLuint program_id);
};

#endif