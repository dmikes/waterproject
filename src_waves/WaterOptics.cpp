#include "WaterOptics.h"
#include "GraphicsContext.h"
#include "Texture2D.h"
#include "Transformation3D.h"
#include "FloatingObject.h"
#include "Model3D.h"
#include "ResourceList.h"

typedef GraphicsContext GC;

void WaterOptics::init(GraphicsContext& gc)
{
	waterClarity = gc.simConf->waterClarity;
	reflectDarkening = gc.simConf->reflectDarkening;

	Drawable::init(gc);
	// prepare texture reflection map texture
	Texture* tex = gc.getTexture(GC::TEX_LOCAL_REFLECTION);
	glGenFramebuffers(1, &FBOwaterOptics);
	glBindFramebuffer(GL_FRAMEBUFFER, FBOwaterOptics);
	// Framebuffer texture will be set in draw

	glGenRenderbuffers(1, &RBOwaterOptics);
	glBindRenderbuffer(GL_RENDERBUFFER, RBOwaterOptics);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, tex->width, tex->height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, RBOwaterOptics);

	//GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	//assert(status == GL_FRAMEBUFFER_COMPLETE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void WaterOptics::winResize(GraphicsContext& gc, int width, int height)
{
	if (isInitialized){
		glBindFramebuffer(GL_FRAMEBUFFER, FBOwaterOptics);
		glBindRenderbuffer(GL_RENDERBUFFER, RBOwaterOptics);
		gc.getTexture(GC::TEX_LOCAL_REFLECTION)->allocate(width, height, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);
		gc.getTexture(GC::TEX_LOCAL_REFRACTION)->allocate(width, height, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
	}
}

void WaterOptics::update(GraphicsContext& gc)
{

}

void WaterOptics::display(GraphicsContext& gc)
{
	if (gc.curShader != gc.getShaders(GC::SHA_WATER_OPTICS)){
		gc.curShader = gc.getShaders(GC::SHA_WATER_OPTICS);
		glUseProgram(gc.curShader->programId);
	}

	if (gc.curCamera != gc.curResources->inputCamera){
		gc.curCamera = gc.windowCamera;
		gc.curCamera->updateCameraViewMatrix();
		glViewport(0, 0, gc.curCamera->winDim.x, gc.curCamera->winDim.y);
	}


	gc.applyProjectionMatrix();
	gc.applyModelViewMatrix();


	static GLenum RBObuffers[] = { GL_COLOR_ATTACHMENT0 };
	glBindFramebuffer(GL_FRAMEBUFFER, FBOwaterOptics);
	glDrawBuffers(1, RBObuffers);

	glUniform4fv(gc.getUniformLocation("u_DeepWaterColor"), 1, &gc.deepWaterColour.x);
	glUniform1f(gc.getUniformLocation("u_WaterClarity"), waterClarity);
	glUniform1f(gc.getUniformLocation("u_ReflectDarkening"), reflectDarkening);

	// --- Reflect ---
	// Important: draw reflection before refraction 
	// (still produces error but the reflection has larger influence especially on long distances)
	Texture* reflTex = gc.getTexture(GC::TEX_LOCAL_REFLECTION);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, reflTex->target, reflTex->texID, 0);
	glClearColor(0, 0, 0, 0.0f); // transparent background
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glUniform1i(gc.getUniformLocation("u_Refract"), false);
	
	int size = gc.curResources->floaters.size();
	for (FloatingObject* floater : gc.curResources->floaters){
		displayReflection(gc, floater, false);
	}
	// Display other terain objects
	for (Model3D* model : gc.staticObjects){
		displayReflection(gc, model);
	}
	
	// --- Refract ---
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	
	Texture* refrTex = gc.getTexture(GC::TEX_LOCAL_REFRACTION);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, refrTex->target, refrTex->texID, 0);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, refrTex->width, refrTex->height);

	glClearColor(vec3Elements(gc.deepWaterColour), 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUniform1i(gc.getUniformLocation("u_Refract"), true);

	for (FloatingObject* floater : gc.curResources->floaters){
		displayRefraction(gc, floater, false);
	}
	for (Model3D* model : gc.staticObjects){
		displayRefraction(gc, model);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0, 0, 0, 1.0); // just for sure
}


void WaterOptics::displayReflection(GraphicsContext& gc, FloatingObject* floater, bool isFirst){

	vec2 silCamScale = 1.0f / floater->silhouetteCamera->ortho.viewMax;
	vec4 heightTexToWorld = vec4(silCamScale, silCamScale * static_cast<vec2>(floater->silhouetteCamera->position));

	floater->texHeightfield.bind(1);
	glUniform1i(gc.getUniformLocation("u_WaterHeightTex"), floater->texHeightfield.textureUnit);
	glUniform4fv(gc.getUniformLocation("u_HeightTexToWorld"), 1, &heightTexToWorld.x);

	// --- Reflection ---	
	Model3D* ship = floater->objNormal;

	Transformation3D originalPose = ship->staticTransform;
	// Reflection transformation 
	Transformation3D reflectTf;
	reflectTf.setTranslation(vec3(0, 0, -2 * ship->position.z));
	reflectTf.setScale(vec3(1, 1, -1));
	ship->staticTransform.merge(reflectTf);

	ship->display(gc);
	ship->staticTransform = originalPose;

	glCullFace(GL_BACK);
	//gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
}


void WaterOptics::displayRefraction(GraphicsContext& gc, FloatingObject* floater, bool isFirst){

	vec2 silCamScale = 1.0f / floater->silhouetteCamera->ortho.viewMax;
	vec4 heightTexToWorld = vec4(silCamScale, silCamScale * static_cast<vec2>(floater->silhouetteCamera->position));

	floater->texHeightfield.bind(2);
	glUniform1i(gc.getUniformLocation("u_WaterHeightTex"), floater->texHeightfield.textureUnit);
	glUniform4fv(gc.getUniformLocation("u_HeightTexToWorld"), 1, &heightTexToWorld.x);

	// --- Refraction --
	//Model3D* ship = floater->objNormal;
	//Transformation3D originalPose = ship->staticTransform;

	// Refraction transformation 
	//Transformation3D refractTf;
	//refractTf.setScale(vec3(1, 1, 1.0 / 1.33)); // simulate water IOR
	//refractTf.setScale(vec3(1, 1, 1));
	//ship->staticTransform.merge(refractTf);

	floater->objNormal->display(gc);
	//ship->staticTransform = originalPose; // restore tranformation
}


void WaterOptics::displayRefraction(GraphicsContext& gc, Model3D* staticObject){
	// --- Refraction ---
	staticObject->display(gc);
}

void WaterOptics::displayReflection(GraphicsContext& gc, Model3D* staticObject){
	// --- Reflection ---
	Transformation3D originalPose = staticObject->staticTransform;
	// Reflection transformation 
	Transformation3D reflectTf;
	reflectTf.setTranslation(vec3(0, 0, -2 * (staticObject->position.z + staticObject->staticTransform.translation.z)));
	reflectTf.setScale(vec3(1, 1, -1));
	staticObject->staticTransform.merge(reflectTf);

	staticObject->display(gc);
	staticObject->staticTransform = originalPose;

	glCullFace(GL_BACK);
}

void WaterOptics::preprocess(GraphicsContext& gc)
{

}

