#ifndef __PYRAMIDE_PARTICLE_GEN_H__
#define __PYRAMIDE_PARTICLE_GEN_H__

#include "Drawable.h"
#include "myGL.h"
#include "MeshVBO.h"
#include "Texture2D.h"
//class Texture2D;

class ObjectToWaterCoupling : public Drawable {
private:
	Texture* objSilhouetteTex;
	Texture* waveEffectTex;
	Texture* silhouetteEdgeTex;
public: 
	Texture TMPtex;
	Texture finalTex;
	GLuint FBOfinal;
	GLuint depthSil;
	vec2 waveDepthReduce;

	float waterVolume;		///< volume under water
	GLuint FBOobjectSilhouette;

	void winResize(GraphicsContext& gc, int width, int height);
	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);	
	void preprocess(GraphicsContext& gc);

	float countVolume();
private:
	void pyramidTechnique(GraphicsContext& gc, FloatingObject* floater);
};

#endif