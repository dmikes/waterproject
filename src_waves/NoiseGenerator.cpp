#include "NoiseGenerator.h"

#include "GraphicsContext.h"
#include "Model3D.h"
#include "Texture2D.h"
#include "../libs/noise/noise1234.h"

typedef GraphicsContext GC;

void NoiseGenerator::preprocess(GraphicsContext& gc){
	for (int i = 0; i < 256; i++){
		dirs[i].x = cos(i * 2.0f * M_PI / 256);
		dirs[i].y = sin(i * 2.0f * M_PI / 256);
	}
}

// Brownian motion
float NoiseGenerator::fBm(float x, float y, float z, int per, int octs){
	float val = 0;
	for (int i = 0; i < octs; i++){
		float i2 = static_cast<float>( pow(2.0, i) ); // 2^i
		//val += pow(0.5, i) * noise(x * i2, y * i2, per * i2);
		val += static_cast<float>(pow(0.5, i)) * Noise1234::pnoise(x * i2, y * i2, z * i2, per * i2, per * i2, per * i2);
	}
	return val;
}

float NoiseGenerator::fBm(float x, float y, int per, int octs){
	float val = 0;
	for (int i = 0; i < octs; i++){
		float i2 = static_cast<float>( pow(2.0, i) ); // 2^i
		val += static_cast<float>(pow(0.5, i)) * Noise1234::pnoise(x * i2, y * i2, per * i2, per * i2);
	}
	return val;
}

float NoiseGenerator::getPixelRepeated(std::vector<glm::vec4>& image, int x, int y, int width, int height){
	// repeat texture
	if (x < 0)		 x = width - x;
	if (x >= width)	 x = x - width;
	if (y < 0)		 y = height - y;
	if (y >= height) y = y - height;
	
	int i = ((y * width) + x); 
	return image[i].x;
}

glm::vec4 NoiseGenerator::getNormal(std::vector<glm::vec4>& image, int x, int y, int width, int height){
	float h = 1.0f; // height modifier
	float cur   = getPixelRepeated(image, x  , y  , width, height);

	float right  = h * getPixelRepeated(image, x+1, y  , width, height);
	float left   = h * getPixelRepeated(image, x-1, y  , width, height);
	float top    = h * getPixelRepeated(image, x  , y+1, width, height);
	float bottom = h * getPixelRepeated(image, x  , y-1, width, height);
	
	//vec3 va = glm::normalize(vec3(2.0, 0.0, right - left));
	//vec3 vb = glm::normalize(vec3(0.0, 2.0, top - bottom));
	//vec4 normal = vec4(glm::cross(va, vb), cur);
	vec4 normal = vec4(right - left, top - bottom, 0, cur);
	return normal;
}

void NoiseGenerator::genTilableNoiseCPU(GraphicsContext& gc, int size, float freq, int octs){
	preprocess(gc);

	std::vector<glm::vec4> imageFloat;
	imageFloat.resize(size*size);

	std::vector<glm::vec4> imageNormals;
	imageNormals.resize(size*size);
	
	Texture* tex3D = gc.getTexture(GC::TEX_PERLIN_NOISE_3D);
	tex3D->init3D(GL_TEXTURE_3D, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_REPEAT);
	tex3D->bind(2);
	tex3D->allocate3D(size, size, size, GL_RGBA32F, GL_RGBA, GL_FLOAT);
	oglCheckError("After allocate 3D.");
	for (int z = 0; z < size; z++){
		//volume[z].resize(size*size);
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int i = ((((y * size) + x) * size) + z) * 4; // index
				int ifl = ((y * size) + x); // index float layer

				float val = fBm(x*freq, y*freq, z*freq, int(size*freq), octs);
				imageFloat[ifl].x = val;
				imageFloat[ifl].y = val;
				imageFloat[ifl].z = val;
				imageFloat[ifl].w = val;
			}
		}
		// after one layer has been made
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int ifl = ((y * size) + x);
				imageNormals[ifl] = getNormal(imageFloat, x, y, size, size);
			}
		}
	
		glTexSubImage3D(
			GL_TEXTURE_3D, 0,  // GLenum target,  GLint level,
			0, 0, z, size, size, 1, // GLint xoffset,  GLint yoffset,  GLint zoffset,  GLsizei width,  GLsizei height,  GLsizei depth, 
			GL_RGBA, GL_FLOAT, (GLvoid*) &imageNormals[0].x); // GLenum format,  GLenum type,  const GLvoid * data		
	}

	oglCheckError("Noise Gen after glTexSubImage.");
	glGenerateMipmap(GL_TEXTURE_3D);

}