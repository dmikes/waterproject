#include "wrappers.h"

#include <functional>
#include <array>
#include "DrawableWrapper.h"
#include "GraphicsContext.h"
#include "Texture2D.h"
#include "MeshVBO.h"
#include "Model3D.h"
#include "Transformation3D.h"
#include "FloatingObject.h"
#include "WaveParticleSimulation.h"
#include "ResourceList.h"

typedef GraphicsContext GC;

// Objects in Fixed Pipeline
DrawableWrapper dwObjectsFixPipe;
DrawableWrapper dwTexturePreview;
DrawableWrapper dwComputeShaderExample;

DrawableWrapper* getConvexModelPositionUpdate(){
	return &dwComputeShaderExample;
}
DrawableWrapper* getSceneBoxDW(){
	return &dwObjectsFixPipe;
}
DrawableWrapper* getTexturePreviewDW(){
	return &dwTexturePreview;
}


void setupSceneBoxDW(){

	dwObjectsFixPipe.updateFunc = [](GraphicsContext& gc){
		for (FloatingObject* floater : gc.curResources->floaters){
			floater->update(gc);
		}
	};

	dwObjectsFixPipe.displayFunc = [](GraphicsContext& gc){
		// --- Fixed pipeline shader ---
		// Drawing Box
		gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		gc.curCamera = gc.windowCamera; //gc.cameras[CameraSelect::PERSP]; // Switch to Perspective
		gc.curCamera->updateCameraViewMatrix();
		glViewport(0, 0, gc.curCamera->winDim.x, gc.curCamera->winDim.y);

		gc.curShader = gc.getShaders(gc.SHA_FIXED_PIPELINE);
		glUseProgram(gc.curShader->programId);
		gc.applyProjectionMatrix();
		
		mat4 modelView;
		static MeshVBO* cube = gc.getMesh(gc.MES_CUBE);

		// Draw Skybox
		Texture* skybox = gc.getTexture(GC::TEX_SKYBOX);
		skybox->bind(6);

		glDepthMask(GL_FALSE); // do not write into depth buffer

		glUniform1i(gc.getUniformLocation("u_CubeTexture"), skybox->textureUnit);
		glUniform1i(gc.getUniformLocation("u_Mode"), FixedPipelineShaderMode::CUBEMAP);
		modelView = gc.getModelViewMatrix();
		modelView = glm::translate(modelView, gc.getCamera(GC::CAM_PERSP)->position); // follow main camera
		gc.getTransform(GC::TRA_SKYBOX)->applyTRSToMatrix(modelView); // apply transforms 
		gc.applyModelViewMatrix(&modelView[0][0]);
		cube->display(GL_QUADS);

		glDepthMask(GL_TRUE);

		// Draw ship
		vec4 waterMVP = gc.getProjectionMatrix() * gc.getModelViewMatrix() * vec4(gc.simConf->waterOrigin, 1);
		glUniform4fv(gc.getUniformLocation("u_WaterPosScreen"), 1, &waterMVP.x);

		for (FloatingObject* floater : gc.curResources->floaters){
			Model3D* ship = floater->objNormal;
			ship->display(gc);
		}
		
		// Display non.moving objects
		for (Model3D* model : gc.staticObjects){
			model->display(gc);
		}
		
		//// Draw axis
		if(gc.drawAxis){
			glUniform1i(gc.getUniformLocation("u_Mode"), FixedPipelineShaderMode::UNIFORM_COLOR);

			gc.applyModelViewMatrix();

			GLint colorLoc = gc.getUniformLocation("u_Color");
			glLineWidth(5);
			glUniform3fv(colorLoc, 1, &X_AXIS[0]); 
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(500, 0, 0);
			glEnd();
			glUniform3fv(colorLoc, 1, &Y_AXIS[0]); 
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 500, 0);
			glEnd();
			glUniform3fv(colorLoc, 1, &Z_AXIS[0]); 
			glBegin(GL_LINES);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 0, 500);
			glEnd();
			glLineWidth(1);
		}
	};
}



static const int TEX_SIZE = 200;

void setupTexturePreviewDW(){

	dwTexturePreview.displayFunc = [](GraphicsContext& gc){
		#define KEEP_AS_IS 0
		#define NEGATIVE 1
		#define HIGLIGHT_COLORS 2
		#define HIGLIGHT_COLORS_NEGATIVE 3

		#define FINAL_WAVE_PARTICLE 11

		if(gc.displayTex){
			gc.curCamera = gc.getCamera(GC::CAM_ORTHO_TEXTURE);
			glViewport(0, 0, gc.curCamera->winDim.x, gc.curCamera->winDim.y);

			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gc.curShader = gc.getShaders(GC::SHA_DISPLAY_TEX); 
			glUseProgram(gc.curShader->programId);

			FloatingObject* floater = gc.activeFloater;

			int startX = 0;
			
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE);
			//gc.textures[TexSel::TEX_PERLIN_NOISE_3D]->display3D(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			//// local reflection
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS); 
			//gc.textures[TexSel::TEX_LOCAL_REFLECTION]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			//// local refraction
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TexSel::TEX_LOCAL_REFRACTION]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;
			
			//// compute example
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TexSel::TEX_COMPUTE]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;
			
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TEX_TEMP]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			//// noise 
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TexSel::TEX_PERLIN_NOISE]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			//// noise gradients
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TexSel::TEX_PERLIN_NOISE_GRADIENT]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			//// pool border
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TexSel::TEX_BORDERS]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			// particles 
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE);
			gc.activeFloater->particleRender.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			// particles filtered
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE);
			gc.activeFloater->particleFiltered.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			// Point tex
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), KEEP_AS_IS);
			//gc.textures[TexSel::TEX_PARTICLE_SHAPE]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			//// gradients filtered
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE); 
			//gc.textures[TexSel::TEX_PARTICLES_GRADIENT_FILTER_XY]->display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			// Buoyancy
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE);
			gc.activeFloater->texBuoyancy.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			// Water hole
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE);
			gc.activeFloater->texWaterHole.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			// Drag force
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), NEGATIVE);
			//gc.activeFloater->texWaterForces.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;

			glUniform1i(gc.getUniformLocation("u_DisplayMode"), NEGATIVE);
			gc.activeFloater->texLiftForces.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			// floater heightfield
			//glUniform1i(gc.getUniformLocation("u_DisplayMode"), NEGATIVE);
			//gc.activeFloater->texHeightfield.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			//startX += TEX_SIZE;
			
			// Result of object 2 water
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), FINAL_WAVE_PARTICLE);
			gc.activeFloater->texFinalWaveParticle.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			// Object sillhouette
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), HIGLIGHT_COLORS_NEGATIVE); 
			gc.activeFloater->texObjectSilhouette.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			glUniform1i(gc.getUniformLocation("u_DisplayMode"), NEGATIVE); 
			gc.activeFloater->texWaveEffect.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			glUniform1i(gc.getUniformLocation("u_LOD"), gc.displayTexLOD); 
			glUniform1i(gc.getUniformLocation("u_DisplayMode"), NEGATIVE); 
			gc.activeFloater->texSilhouetteEdge.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;

			glUniform1i(gc.getUniformLocation("u_DisplayMode"), NEGATIVE); 
			gc.activeFloater->texTemp.display(gc, startX, 0, TEX_SIZE, TEX_SIZE);
			startX += TEX_SIZE;
			
			oglCheckError("END OF TEX DISPLAY");
		}
	};
}


DrawableWrapper* getComputeShaderExample(){
	return &dwComputeShaderExample;
}

void setupComputeShaderExample()
{
	dwComputeShaderExample.displayFunc = [](GraphicsContext& gc){
		//oglCheckError("compute in");
		//ShaderSet* example = gc.shaderSets[ShaderSel::SHA_COMPUTE];
		//glUseProgram(example->programId);

		//static bool first = true;
		//Texture* comTex = gc.textures[TEX_COMPUTE];

		//oglCheckError("Before Active tex");

		//glActiveTexture(GL_TEXTURE0 + comTex->textureUnit);
		////glBindTexture(GL_TEXTURE_2D, comTex->texID);

		//glUniform1i(glGetUniformLocation(example->programId, "destTex"), comTex->textureUnit);
		//glBindImageTexture(comTex->textureUnit, comTex->texID, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);

		//oglCheckError("after glBindImageTexture");

		//static int i = 0;
		//i++;
		//
		//glUniform1f(glGetUniformLocation(example->programId, "roll"), (float)i*0.01f);
		//oglCheckError("Before dispatch");
		//glDispatchCompute(512 / 16, 512 / 16, 1); // 512^2 threads in blocks of 16^2
		//oglCheckError("Dispatch compute shader");
		//

		////glBindImageTexture(1, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);
		////glActiveTexture(GL_TEXTURE0);
		////glBindTexture(GL_TEXTURE_2D, 0);

		////glMemoryBarrier(GL_ALL_BARRIER_BITS);
	};
}

