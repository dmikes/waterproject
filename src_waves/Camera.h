#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "glm.h"
#include "ConfigIni.h"
#include "myGL.h"
#include <string>

class FloatingObject;

class Camera {
public:
	std::string filename;
	//Transformation matrixes
	glm::mat4 projectionMatrix; // Camera projection transformation
	glm::mat4 modelViewMatrix; // Camera view transformation

	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 positionLag;
	glm::vec3 rotationLag;

	glm::ivec2 winDim;

	bool freeWalk;
	bool hasInertia;
	bool isWindowDependent;
	float planeNear;
	float planeFar;
	
	struct OrthoSetting {
		vec2 viewMin;
		vec2 viewMax;
	} ortho;

	struct PerspSetting {
		float fov, aspectRatio;
	} persp;

	// TODO: config should only set (copy) properties and not being stored as member variable
	CameraConfig* config; // deletion is not responsibility of this class
	vec3 forwardView;
	vec3 forwardViewPrevious;
	vec3 rightView;	
	//vec3 upView;

	FloatingObject* followee = NULL;
	float followHeight = 80.f;
	float followDistance = 320.f;
	//float followHeight = 480.f;
	//float followDistance = 10.f;

	enum Move {
		FORWARD,
		BACK,
		LEFT,
		RIGHT,
		UP,
		DOWN
	};

	enum ProjectionType {
		PERSPECTIVE,
		ORTHOGRAPHIC
	};
	ProjectionType type;

	//Camera(CameraConfig* config);
	Camera(std::string configFile);

	void resetView();
	void resizeWindow(int winWidth, int winHeight);
	void updateCameraViewMatrix();
	void move(Move dir);
	void rotate(float diffX, float diffY);
	void rotateUpVec(float angle);
	void setCameraParams(const glm::mat3& rot, const glm::vec3& trans);
	void switchFreewalk();
	bool isCameraStatic() const;
	

	void setPosition(glm::vec3 val);
	const glm::mat4& getModelViewMatrix() const;
	const glm::mat4& getProjectionMatrix() const;
	const glm::vec3& getPosition() const;
	const glm::vec3 getViewDirection() const;
	const glm::ivec2& getWindowSize() const;
	const float* getModelViewMatrixPointer() const;
	const float* getProjectionMatrixPointer() const;
	void updateProjection(int winWidth, int winHeight);
	void updateProjection();
	void applyViewport();
	void follow(FloatingObject* floater);
};


#endif