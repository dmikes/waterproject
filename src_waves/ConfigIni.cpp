#include "ConfigIni.h"


vec2 Config::getVec2(INIReader& reader, const char* section )
{
	return vec2(	
		reader.GetFloat(section, "x", 0.0f), 
		reader.GetFloat(section, "y", 0.0f)
	);
}

vec3 Config::getVec3(INIReader& reader,  const char* section )
{
	return vec3(	
		reader.GetFloat(section, "x", 0.0f), 
		reader.GetFloat(section, "y", 0.0f),
		reader.GetFloat(section, "z", 0.0f)
	);
}

vec4 Config::getVec4(INIReader& reader,  const char* section )
{
	return vec4(	
		reader.GetFloat(section, "x", 0.0f), 
		reader.GetFloat(section, "y", 0.0f),
		reader.GetFloat(section, "z", 0.0f),
		reader.GetFloat(section, "w", 0.0f)
	);
}

ivec2 Config::getIVec2(INIReader& reader, const char* section )
{
	return ivec2(	
		reader.GetInt(section, "x", 0), 
		reader.GetInt(section, "y", 0)
	);
}

ivec3 Config::getIVec3(INIReader& reader,  const char* section )
{
	return ivec3(	
		reader.GetInt(section, "x", 0), 
		reader.GetInt(section, "y", 0),
		reader.GetInt(section, "z", 0)
	);
}

ivec4 Config::getIVec4(INIReader& reader,  const char* section )
{
	return ivec4(	
		reader.GetInt(section, "x", 0), 
		reader.GetInt(section, "y", 0),
		reader.GetInt(section, "z", 0),
		reader.GetInt(section, "w", 0)
	);
}

Config::Config(std::string filename) //: filename(filename)
{
	this->filename = filename;
}

bool Config::fileExists(std::string filename)
{
	if (FILE *file = fopen(filename.c_str(), "r")) {
		fclose(file);
		return true;
	} 
	return false;
}


// === Camera ===
CameraConfig::CameraConfig(std::string filename) : Config(filename) {
	parse();
};

void CameraConfig::parse() {
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", filename.c_str());
	}
	INIReader reader(filename);
	
	isPerspective		= reader.GetBoolean("", "isPerspective",	 false);
	isWindowDependent	= reader.GetBoolean("", "isWindowDependent", false);
	hasInertia			= reader.GetBoolean("", "movementInertia",   false);

	winDim   = getIVec2(reader, "winDim");
	position = getVec3(reader, "position");
	rotation = getVec3(reader, "rotation");
	
	if(isPerspective){
		persp.fov			= reader.GetFloat("perspective", "fov", 55.0f);
		persp.aspectRatio	= reader.GetFloat("perspective", "aspectRatio", 1.0f);
	} else {
		ortho.left   = reader.GetFloat("orthographic", "left"	, 0);
		ortho.right  = reader.GetFloat("orthographic", "right"	, 500);
		ortho.bottom = reader.GetFloat("orthographic", "bottom"	, 0);
		ortho.top    = reader.GetFloat("orthographic", "top"	, 500);
	}
	
	planeNear = reader.GetFloat("cuttingPlane", "near", 0.1f);
	planeFar  = reader.GetFloat("cuttingPlane", "far", 10000.0f);
}

// ===== Simulation Config =====
SimulationConfig::SimulationConfig(std::string filename) : Config(filename){
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", filename.c_str());
	}
	INIReader reader(filename);

	// --- tessellation and surface ---
	mTess.useTessellation = reader.GetBoolean("tessellation", "useTessellation", true);
	mTess.minTessLevel = reader.GetInt("tessellation", "minLevel", 1);
	mTess.maxTessLevel = reader.GetInt("tessellation", "maxLevel", 5);
	mTess.minTessDistance = reader.GetFloat("tessellation", "minDistance", 0.0f);
	mTess.maxTessDistance = reader.GetFloat("tessellation", "maxDistance", 1000.0f);
	if(mTess.useTessellation){
		gridVertexCount   = getIVec2(reader, "gridPatchCount");
	} else {
		gridVertexCount   = getIVec2(reader, "gridVertexCount");
	}
	waterSize   = getVec2(reader, "waterSurfSize");
	waterOrigin = getVec3(reader, "waterSurfOrigin");


	// --- wave particles ---
	mWaveParticle.speed			= reader.GetFloat("waveParticles", "speed", 1.0f);
	mWaveParticle.waveDampening  = reader.GetFloat("waveParticles", "waveDampening", 0);
	mWaveParticle.nMaxParticles  = reader.GetInt("waveParticles", "maxParticles", 1000);
	mWaveParticle.particleWidth  = reader.GetFloat("waveParticles", "particleWidth", 10.0f);
	mWaveParticle.filterKernelSize  = reader.GetInt("waveParticles", "FilterKernelSize", 10);
	mWaveParticle.minDispersionAngle = reader.GetFloat("waveParticles", "minDispersionAngle", 10.0f); // degrees

	// --- Noise ---
	mNoise.speed    = reader.GetFloat("noise", "speed", 1.0f);
	mNoise.strength = reader.GetFloat("noise", "strength", 1.f);
	mNoise.scale    = reader.GetFloat("noise", "scale", 100.f);

	// --- Surface ---
	m_Surface.horizontalCoef = reader.GetFloat("waterSurface", "horizontalCoef", 1.f);
	m_Surface.verticalCoef   = reader.GetFloat("waterSurface", "verticalCoef", 1.f);

	simStepsPerSec = reader.GetInt("timing", "stepsPerSec", 60);
	runUntil = reader.GetInt("timing", "runUntil", -1);
	doRunUntil = (runUntil > 0);

	texSize   = getVec2(reader, "renderTextureSize");

	texRayDistorition = reader.GetFloat("optical", "texRayDistorition", 1.0f);
	fresnelR0 = reader.GetFloat("optical", "fresnelR0", 1.0f);
	fresnelPow = reader.GetFloat("optical", "fresnelPow", 1.0f);
	waterClarity = reader.GetFloat("optical", "waterClarity", 1.0f);
	reflectDarkening = reader.GetFloat("optical", "reflectDarkening", 1.0f);
	useWaterHole = reader.GetBoolean("optical", "useWaterHole", true);

	waveDepthReduce = getVec2(reader, "waveDepthReduce");

	useBoundaries = reader.GetBoolean("boundary", "useBoundaries", false);

	waterColor = getVec4(reader, "waterColor");
}

void SimulationConfig::customSceneOverride( SimulationConfig& child )
{
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", child.filename.c_str());
	}
	INIReader reader(child.filename);

	if(reader.Has("noise", "speed")){
		mNoise = child.mNoise;
	}

	if(reader.Has("tessellation", "useTessellation")){
		mTess = child.mTess;
		//mTess.useTessellation = child.mTess.useTessellation;
		//mTess.minTessDistance = child.mTess.minTessDistance;
		//mTess.maxTessDistance = child.mTess.maxTessDistance;
		//mTess.minTessLevel = child.mTess.minTessLevel;
		//mTess.maxTessLevel = child.mTess.maxTessLevel;
		if(mTess.useTessellation){
			if(reader.Has("gridPatchCount", "x")){
				gridVertexCount = child.gridVertexCount;
			}
		} else {
			if(reader.Has("gridVertexCount", "x")){
				gridVertexCount = child.gridVertexCount;
			}
		}
	}
	if(reader.Has("waterSurfSize", "x")){
		waterSize = child.waterSize;
	}
	if(reader.Has("waterSurfOrigin", "x")){
		waterOrigin = child.waterOrigin;
	}
	if(reader.Has("waveParticles", "waveDampening")){
		mWaveParticle.waveDampening = child.mWaveParticle.waveDampening;
	}

	if(reader.Has("waveParticles", "maxParticles")){
		mWaveParticle.nMaxParticles = child.mWaveParticle.nMaxParticles;
	}
	if(reader.Has("waveParticles", "particleWidth")){
		mWaveParticle.particleWidth = child.mWaveParticle.particleWidth;
	}
	if(reader.Has("waveParticles", "FilterKernelSize")){
		mWaveParticle.filterKernelSize  = child.mWaveParticle.filterKernelSize;	
	}
	if(reader.Has("waveParticles", "speed")) {
		mWaveParticle.speed = child.mWaveParticle.speed;
	}
	if (reader.Has("waveParticles", "minDispersionAngle")) {
		mWaveParticle.minDispersionAngle = child.mWaveParticle.minDispersionAngle;
	}
	if (reader.Has("waveParticles", "minDispersionAngle")) {
		mWaveParticle.minDispersionAngle = child.mWaveParticle.minDispersionAngle;
	}
	if (reader.Has("waveParticles", "useBoundaries")) {
		mWaveParticle.minDispersionAngle = child.mWaveParticle.minDispersionAngle;
	}


	if(reader.Has("timing", "stepsPerSec")){
		simStepsPerSec = child.simStepsPerSec;
	}
	if(reader.Has("timing", "runUntil")){
		runUntil = child.runUntil;
		doRunUntil = true;
	}

	if(reader.Has("renderTextureSize", "x")){
		texSize = child.texSize;
	}

	if (reader.Has("optical", "texRayDistorition")){
		texRayDistorition = child.texRayDistorition;
	}
	if (reader.Has("optical", "fresnelR0")){
		fresnelR0 = child.fresnelR0;
	}
	if (reader.Has("optical", "fresnelPow")){
		fresnelPow = child.fresnelPow;
	}
	if (reader.Has("optical", "waterClarity")){
		waterClarity = child.waterClarity;
	}
	if (reader.Has("optical", "reflectDarkening")){
		reflectDarkening = child.reflectDarkening;
	}
	if (reader.Has("optical", "useWaterHole")){
		useWaterHole = child.useWaterHole;
	}
	if (reader.Has("waterColor", "x")){
		waterColor = child.waterColor;
	}
	if (reader.Has("waveDepthReduce", "x")){
		waveDepthReduce = child.waveDepthReduce;
	}
	if (reader.Has("boundary", "useBoundaries")){
		useBoundaries = child.useBoundaries;
	}
}



// ===== Shader Config =====
ShaderConfig::ShaderConfig(std::string filename) : Config(filename){
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", filename.c_str());
	}
	INIReader reader(filename);

	description = reader.Get("", "description", "");
	filenameVertex = reader.Get("programs", "vertex", "");
	useVertex = (filenameVertex != ""); // is not default value
	
	filenameGeometry = reader.Get("programs", "geometry", "");
	useGeometry = (filenameGeometry != "");

	filenameFragment = reader.Get("programs", "fragment", "");
	useFragment = (filenameFragment != "");

	filenameTessControl = reader.Get("programs", "tessellation_control", "");
	filenameTessEval	= reader.Get("programs", "tessellation_evaluation", "");
	useTessellation = (filenameTessControl != "" && filenameTessEval != "");

	filenameCompute = reader.Get("programs", "compute", "");
	useCompute = (filenameCompute != "");
}

// ===== Object Config =====
TransformationConfig::TransformationConfig(std::string filename) : Config(filename){
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", filename.c_str());
	}
	INIReader reader(filename);

	translation	= getVec3(reader, "translation");
	rotation	= getVec3(reader, "rotation");
	scale		= getVec3(reader, "scale");
}


// ===== Floater config =====
FloatingObjectConfig::FloatingObjectConfig(std::string filename) : Config(filename){
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", filename.c_str());
	}
	
	INIReader reader(filename);

	modelNormal = reader.Get("paths", "modelNormal", "");
	modelConvex = reader.Get("paths", "modelConvex", "");
	transformation = reader.Get("paths", "transformation", "");
	silhouetteCamera = reader.Get("paths", "silhouetteCamera", "");
	waveParticleCamera = reader.Get("paths", "waveParticleCamera", "");

	density			= reader.GetFloat("parameters", "density", 1.0f);
	swayReduce		= reader.GetFloat("parameters", "swayReduce", 10.0);
	waveStrength	= reader.GetFloat("parameters", "waveStrength", 0.25);
	floatSpeed		= reader.GetFloat("parameters", "floatSpeed", 1.3e6);
	moveStep		= reader.GetFloat("parameters", "moveStep", 1.0f);
	dragCoef		= reader.GetFloat("parameters", "dragCoef", 0.4);
	liftCoef		= reader.GetFloat("parameters", "liftCoef", 0.2);
	meshProjectCoef = reader.GetFloat("parameters", "meshProjectCoef", 0.5);
	volumeCumulationCoef = reader.GetFloat("parameters", "volumeCumulationCoef", 0.005);
	rollRestore			= reader.GetFloat("parameters", "rollRestore", 0.97);
	rollMax			= reader.GetFloat("parameters", "rollMax", 10.0);
	pitchMax		= reader.GetFloat("parameters", "pitchMax", 0.3);
	pitchMaxSpeed	= reader.GetFloat("parameters", "pitchMaxSpeed", 37.3);
	maxWaveSlopeX	= reader.GetFloat("parameters", "maxWaveSlopeX", 4000.0);
	maxWaveSlopeY	= reader.GetFloat("parameters", "maxWaveSlopeY", 4000.0);
	rotationSpeed	= reader.GetFloat("parameters", "rotationSpeed", 0.025);

	rotSpeedVec		= getVec3(reader, "rotSpeedVec");
}

PathConfig::PathConfig(std::string filename) : Config(filename){
	if (!fileExists(filename)){
		printf("ERROR: config file %s was not found!\n", filename.c_str());
	}
	INIReader reader(filename);

	images = reader.Get("folders", "images", "./images");
	shaders = reader.Get("folders", "shaders", "./shaders");
	models = reader.Get("folders", "models", "./models");
	transforms = reader.Get("folders", "transformations", "./transformations");
	sceneConfig = reader.Get("folders", "sceneConfig", "./scenes");
	cameraConfig = reader.Get("folders", "cameraConfig", "./cameras");
	floaterConfig = reader.Get("folders", "floaterConfig", "./floaters");
	shaderConfig = reader.Get("folders", "shaderConfig", "./shaders");
	waterSimulationConfig = reader.Get("folders", "waterSimulationConfig", "./water_simulation");
}