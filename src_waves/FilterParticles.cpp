#include "FilterParticles.h"
#include "GraphicsContext.h"
#include "Texture2D.h"
#include "Model3D.h"
#include "ResourceList.h"
#include "FloatingObject.h"

typedef GraphicsContext GC;

void FilterParticles::init(GraphicsContext& gc)
{
	if(isInitialized) return;
	isInitialized = true;

	filterKernelSize = gc.simConf->mWaveParticle.filterKernelSize;

	glGenFramebuffers(1, &surfaceDeviation_FBO);
	assert(surfaceDeviation_FBO > 0);

}

void FilterParticles::update(GraphicsContext& gc)
{
	
}

void FilterParticles::display(GraphicsContext& gc)
{
	for (FloatingObject* floater : gc.curResources->floaters){
		filterParticleTexture(gc, floater);
	}
}

void FilterParticles::filterParticleTexture(GraphicsContext& gc, FloatingObject* floater){
	vec2 invTexSize = 1.0f / (vec2)gc.simConf->texSize;

	gc.curCamera = gc.getCamera(GC::CAM_PLANE);

	ivec2 winDim = floater->waveParticleCamera->winDim;
	glViewport(0, 0, winDim.x, winDim.y);

	if (gc.curShader != gc.getShaders(GC::SHA_FILTER_PARTICLES)){
		gc.curShader = gc.getShaders(GC::SHA_FILTER_PARTICLES);
		glUseProgram(gc.curShader->programId);
	}
	gc.applyProjectionMatrix();

	static GLenum RBObuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	// Vertical deviation - Separable filter approximation
	// Vertical deviation in X direction
	glBindFramebuffer(GL_FRAMEBUFFER, surfaceDeviation_FBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->particleFilterByX.texID, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, floater->gradientFilterByX.texID, 0);
	glDrawBuffers(2, RBObuffers);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // not necessary

	floater->particleRender.bind(1);
	//gc.textures[TexSel::TEX_PARTICLES]->bind();
	//glUniform1f(gc.getUniformLocation("u_Alpha"), 0.2f);
	glUniform2fv(gc.getUniformLocation("u_TexSizeInv"), 1, &invTexSize.x);
	glUniform1i(gc.getUniformLocation("u_Axis"), 0); // 0 as X
	glUniform1i(gc.getUniformLocation("u_KernelSize"), filterKernelSize);
	glUniform1f(gc.getUniformLocation("u_InvKernelSize"), 1.0f / (float)filterKernelSize);
	glUniform1i(gc.getUniformLocation("u_InputTexture"), floater->particleRender.textureUnit);

	static MeshVBO* texPlane = gc.getMesh(GC::MES_PLANE);
	texPlane->display(GL_QUADS);

	// Vertical deviation in Y direction
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->particleFiltered.texID, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, floater->gradientFiltered.texID, 0);
	glDrawBuffers(2, RBObuffers);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // its re-painted by definition of filterting on shaders

	floater->particleFilterByX.bind(2);
	glUniform1i(gc.getUniformLocation("u_Axis"), 1); // 1 as Y
	glUniform1i(gc.getUniformLocation("u_InputTexture"), floater->particleFilterByX.textureUnit);
	// TODO: should it have two input textures? gradient

	texPlane->display(GL_QUADS);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FilterParticles::winResize( GraphicsContext& gc, int width, int height )
{

}

void FilterParticles::preprocess(GraphicsContext& gc)
{

}
