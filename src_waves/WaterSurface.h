#ifndef __WATER_SURFACE_H__
#define __WATER_SURFACE_H__

#define MAX_WAVES 8

#include "MeshVBO.h"
#include "ShaderSet.h"
#include "GraphicsContext.h"
#include "glm.h"
#include "utils.h"
#include "Drawable.h"

class WaterSurface : public Drawable {
public:
	float horizontalCoef = 0.0f;
	float verticalCoef = 0.0f;
	bool useWaterHole = true;

	struct TessellationParams {
		float minDistance;
		float maxDistance;
		float minLevel;
		float maxLevel;
	} m_Tess;

	MeshVBO* water;
	float mNoiseSpeed = 0.0f;
	float mNoiseTime = 0.0f;
	float mNoiseStrength = 0.0f;
	float mNoiseScale = 0.0f;

	float texRayDistortion = 0.12f;
	float fresnelR0 = 0.2f;
	float fresnelPow = 5.f;
	vec2 sunEffect = vec2(32.0f, 0.0f);

	void winResize(GraphicsContext& gc, int width, int height);
	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);
	void preprocess(GraphicsContext& gc);

};


#endif