#include "Model3D.h"
#include "ModelLoader.h"
#include "Texture2D.h"

typedef GraphicsContext GC;

// Bounding box
void Model3D::BoudingBox::transform(mat4 model){
	mMin = static_cast<vec3>(model * vec4(mMin, 1.f));
	mMax = static_cast<vec3>(model * vec4(mMax, 1.f));
	mCenter = static_cast<vec3>(model * vec4(mCenter, 1.f));
}

// Model
Model3D::Model3D(MeshVBO* mesh, bool loadCentroids) : 
mesh(mesh), fromFile(false), useCentoids(loadCentroids)
{
	setDrawMode(GL_QUADS);
}

Model3D::Model3D(std::string filename, bool loadCentroids) : filename(filename), 
mesh(NULL), fromFile(true), useCentoids(loadCentroids)
{
	setDrawMode(GL_TRIANGLES);
}


Model3D::Model3D(std::string filename, const char* textureFolder, bool mipmap) : filename(filename),
mesh(NULL), fromFile(true), useCentoids(false), doMipmap(mipmap)
{
	this->textureFolder = std::string(textureFolder);
	setDrawMode(GL_TRIANGLES);
}

Model3D::Model3D(std::string filename, const char* textureFolder, bool mipmap, bool loadCentroids) : filename(filename),
mesh(NULL), fromFile(true), useCentoids(loadCentroids), doMipmap(mipmap)
{
	this->textureFolder = std::string(textureFolder);
	setDrawMode(GL_TRIANGLES);
}


Model3D::Model3D()
{

}

Model3D::Model3D(const Model3D& copy)
{
	filename = copy.filename;
	fromFile = copy.fromFile;
	drawMode = copy.drawMode;
	modelBB = copy.modelBB;
	worldBB = copy.worldBB;
	worldBBPersistent = copy.worldBBPersistent;
	useCentoids = copy.useCentoids;
	isInitialized = copy.isInitialized;
	staticTransform = copy.staticTransform;

	mesh = copy.mesh; // copy pointer
	meshCentroids = copy.meshCentroids; // copy pointer
	//diffuseTex = copy.diffuseTex;
	textures = copy.textures;
	texNames = copy.texNames;
	nTex = copy.nTex;
	meshVertixCount = copy.meshVertixCount;
	textureFolder = copy.textureFolder;

}

Model3D::~Model3D() {
	if(mesh) {
		delete mesh;
	}
	if (useCentoids && meshCentroids){
		delete meshCentroids;
	}	
}


MeshVBO* Model3D::load()
{
	MeshVBO* meshPtr;

	std::vector<GLuint> indices;
	std::vector<vec3> vertices;
	std::vector<vec3> normals;
	std::vector<vec2> texCoords;

	//ModelLoader::countIndices(filename); // if used indexed geometry - choose whether use ushort or uint (index span)
	ModelLoader::importModel<GLuint>(filename, indices, vertices, normals, texCoords, texNames, meshVertixCount, modelOffset, modelBB.mMin, modelBB.mMax);
	ModelLoader::importTextures(textureFolder, texNames, textures);

	nTex = textures.size(); // after import (if texture trully is loaded)
	if (doMipmap){
		for (Texture* tex : textures){
			tex->bind(0);
			glTexParameteri(tex->target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(tex->target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glGenerateMipmap(tex->target);
		}
	}

	// update center of bounding box which will be used for the center of the object
	modelBB.mCenter = vec3(0, 0, 0); //== modelBB.mMin + ( (modelBB.mMax - modelBB.mMin) / 2.f ); 

	meshPtr = new MeshVBO();
	meshPtr->setVertices(vertices.size(), &vertices[0][0]);
	meshPtr->setNormals(&normals[0][0]);
	if(!texCoords.empty()){
		meshPtr->setTexCoords(&texCoords[0][0]); // dimension 2
	}
	meshPtr->init(); // must be inited here. because vertices are temporary variable

	updateWorldBB();
	//----
	if (useCentoids){
		uint nIndex = indices.size();
		std::vector<vec3> centroids;
		std::vector<vec4> faceNormals;
		centroids.reserve(nIndex / 3);
		faceNormals.reserve(nIndex / 3);

		mat4 model;
		staticTransform.applyScaleToMatrix(model);
		mat3 scale = static_cast<mat3>(model);
		
		for (uint i = 0; i < nIndex; i += 3){
			glm::vec3 first = vertices[indices[i]];
			glm::vec3 second = vertices[indices[i + 1]];
			glm::vec3 third = vertices[indices[i + 2]];
			glm::vec3 centroid = (first + second + third) / 3.0f;

			glm::vec3 edge1 = scale * (second - first);
			glm::vec3 edge2 = scale * (third - first);
			glm::vec3 edge1x2 = glm::cross(edge1, edge2);
			float A = glm::length(edge1x2) / 2.f; // Area of triangle = length of cross
			glm::vec4 normal_A = vec4(glm::normalize(edge1x2), A); // face normal


			centroids.push_back(centroid);
			faceNormals.push_back(normal_A);
		}

		meshCentroids = new MeshVBO();
		meshCentroids->setNormalFormat(4); // 4D = pack extra information into normals
		meshCentroids->setVertices(centroids.size(), &centroids[0].x);
		meshCentroids->setNormals(&faceNormals[0].x);
		meshCentroids->init();
	}
	return meshPtr;
}

void Model3D::setPosition(vec3 pos){
	position = pos;
	worldBBPersistent = false;
}

void Model3D::setDefaultTransform(Transformation3D trans){
	staticTransform = trans;
}


void Model3D::init(GraphicsContext& gc){
	isInitialized = true;

	if(mesh == NULL){
		mesh = load();
	}
	assert(mesh->isInitialized());
}

void Model3D::updateWorldBB(){
	mat4 modelView;
	
	modelView = glm::translate(modelView, position);
	staticTransform.applyTRSToMatrix(modelView);
	
	
	worldBB.mMin = static_cast<vec3>(modelView * vec4(modelBB.mMin, 1.f));
	worldBB.mMax = static_cast<vec3>(modelView * vec4(modelBB.mMax, 1.f));
	worldBB.mCenter = static_cast<vec3>(modelView * vec4(modelBB.mCenter, 1.f));
}

Texture* Model3D::getDiffuseTex() const { 
	return diffuseTex; 
}

void Model3D::setDiffuseTex(Texture* val) {
	diffuseTex = val; 
}

void Model3D::update(GraphicsContext& gc)
{
	if (!worldBBPersistent){
		updateWorldBB();
		worldBBPersistent = true;
	}
}

void Model3D::display(GraphicsContext& gc)
{
	modelMat = glm::translate(mat4(), position);
	staticTransform.applyTRSToMatrix(modelMat);
	modelMat = glm::rotate(modelMat, rotation.z, Z_AXIS);
	modelMat = glm::rotate(modelMat, rotation.y, Y_AXIS);
	modelMat = glm::rotate(modelMat, rotation.x, X_AXIS);

	glUniformMatrix4fv(gc.getUniformLocation("u_ModelMatrix"), 1, GL_FALSE, &modelMat[0][0]);

	mat4 modelView = gc.getModelViewMatrix() * modelMat;
	gc.applyModelViewMatrix(&modelView[0][0]);

	// Only for non-index geometry
	if (nTex > 0){
		int offset = 0;
		glUniform1i(gc.getUniformLocation("u_ModelDiffuse"), 0); // will be binded to zero
		glUniform1i(gc.getUniformLocation("u_Mode"), FixedPipelineShaderMode::TEXTURE_COLOR);

		for (int m = 0; m < meshVertixCount.size(); m++)
		{
			textures[m]->bind(0);
			mesh->draw_nonIndexed(drawMode, offset, meshVertixCount[m]);
			offset += meshVertixCount[m];
		}
	}
	else {
		glUniform1i(gc.getUniformLocation("u_Mode"), FixedPipelineShaderMode::UNIFORM_COLOR);
		mesh->display(drawMode);
	}

	// Do display bounding box
	if (displayBB){
		modelView = gc.getModelViewMatrix();
		vec3 wordBBsize = worldBB.mMax - worldBB.mMin;
		modelView = glm::translate(modelView, worldBB.mMin);
		modelView = glm::scale(modelView, wordBBsize);

		//staticTransform.applyToMatrix(modelView);
		gc.applyModelViewMatrix(&modelView[0][0]);

		MeshVBO* mesh = gc.getMesh(GC::MES_CUBE_LINE);
		mesh->display(GL_LINES);
	}
}


void Model3D::setDrawMode(GLuint drawMode)
{
	this->drawMode = drawMode;
}

void Model3D::winResize( GraphicsContext& gc, int width, int height )
{

}

void Model3D::preprocess(GraphicsContext& gc)
{

}

