#ifndef __GRAPHICS_CONTEXT__
#define __GRAPHICS_CONTEXT__

#include "global_preprocesor.h"
#include <vector>
#include "myGL.h"
#include <assert.h>

// All in-app resources
#include "Camera.h"
#include "ShaderSet.h"
#include "Light.h"

class MeshVBO;
class Transformation3D;
class FloatingObject;

enum DrawStage {
	DS_DISP_SAME_AS_PREVIOUS = 0,
	DS_DISP_ORTHO_CAM,
	DS_DISP_MAIN_CAM,
	DS_DISP_OBJECT_ORTHO_CAM
};

enum UpdateStage {
	US_UNDEFINED = 0,
	US_WAVE_SIMULATION,
	US_POSITIONING
};

class Texture;
class Model3D;
class Object3D;
class ResourceList;

class GraphicsContext {
public:
	// Graphic Resources!
	std::vector<Camera*> cameras;
	std::vector<ShaderSet*> shaderSets;
	std::vector<Texture*> textures;
	std::vector<Light*> lights;
	std::vector<Model3D*> models;
	std::vector<Model3D*> staticObjects;
	std::vector<FloatingObject*> floaters;
	std::vector<MeshVBO*> meshes;
	std::vector<Transformation3D*> transformations;
	std::vector<std::string> paths;

	enum ShaderSel{
		SHA_WAVE_SURFACE = 0,
		SHA_WAVE_PARTICLE_SIM,
		SHA_FIXED_PIPELINE,
		SHA_RENDER_PARTICLES,
		SHA_FILTER_PARTICLES,
		SHA_DISPLAY_TEX,
		SHA_OBJECT_PARTICLE_GEN,
		SHA_WATER_TO_OBJECT,
		SHA_WATER_OPTICS,
		SHA_COMPUTE,
		SHA_REDUCTION,
		SHA_REDUCTION4,
		SHA_FLOATER_DEVIATION,
		SHA__END // keep it last
	};
	enum CameraSel{
		CAM_PERSP = 0,
		CAM_ORTHO,
		CAM_ORTHO_TEXTURE,
		CAM_OBJECT_SILHOUETTE,
		CAM_CUBE_MAP_RENDER,
		CAM_NOISE_RENDER,
		CAM_PLANE,
		CAM__END // keep it last
	};
	enum TexSel{
		TEX_PARTICLES = 0,
		TEX_PARTICLE_SHAPE,
		TEX_BORDERS,
		TEX_SKYBOX,
		TEX_LOCAL_REFRACTION,
		TEX_LOCAL_REFLECTION,
		TEX_FRESNEL_LOOKUP,
		TEX_PERLIN_NOISE_3D,
		TEX_BUOYANCY,
		TEX_COMPUTE,
		TEX_TEMP,
		TEX__END // keep it last
	};
	enum LightSel {
		LIG_MAIN = 0,
		LIG__END // keep it last
	};
	enum ModelSel {
		MOD_SHIP_01 = 0,
		MOD_SHIP_01_CONVEX,
		MOD_SPHERE,
		MOD_LIGHTHOUSE,
		MOD_TERRAIN1,
		MOD_TERRAIN2,
		MOD_TERRAIN3,
		MOD_HOUSE_POOL,
		MOD_POOL,
		MOD_OBSTACLE,
		SHIP_DUMMY,
		MOD_SHIP_SHALLOP,
		MOD__END // keep it last
	};
	enum FloaterSel {
		FLO_SHIP_1 = 0,
		FLO_SHIP_2
	};
	enum MeshSel {
		MES_CUBE = 0,
		MES_CUBE_LINE,
		MES_PLANE,
		MES_PLANE_UNIT,
		MES__END
	};
	enum TransformSel {
		TRA_SHIP = 0,
		TRA_SKYBOX,
		TRA__END
	};

	Camera* curCamera;
	ShaderSet* curShader;
	
	DrawStage curDrawStage;
	UpdateStage curUpdateStage;

	FloatingObject* activeFloater;

	Camera* mainCamera;
	Camera* windowCamera;
	float motionFreq = 0.1f;
	bool runFromConsole		= false;
	bool testRun			= false;
	bool displayParticles	= true;
	bool displayTex			= false;
	bool pauseAnimation		= false;
	bool pauseWorldForces   = false;
	bool pauseForces		= false;
	bool pauseDemonstration = true;
	bool faceCulling		= false;
	bool wireMode			= false;
	bool motionParticles	= true;
	bool moveShip			= false;
	bool drawAxis			= false;
	int displayTexLOD = 0;
	
	int frameCount = 0;
	float deltaTime = 0.f;

	vec4 deepWaterColour = vec4(0.16862, 0.26666, 0.25688, 1.0f);
	vec4 shallowWaterColour;

	uint framesPerSec = 0; 
	uint avgFramesPerSec = 0;
	float simulationSpeed = 1 / 60.f; // = 1 / simulation steps per second (in float)
	
	SimulationConfig* simConf;
	ResourceList* curResources;

	double beforeTimer = 0.0; // s

	GraphicsContext();
	~GraphicsContext();

	void addShaderSet(ShaderSel shaderId, ShaderSet* shaders);
	void addCamera(CameraSel camId, Camera* camera);
	void addTexture(TexSel textureOrdinal, Texture* texture);
	void addLight(LightSel lightId, Light* light);
	void addFloater(FloaterSel modelId, FloatingObject* model);
	void addMesh(MeshSel meshId, MeshVBO* mesh);
	void addTransform(TransformSel transId, Transformation3D* trans);
	void addModel(Model3D* model);

	ShaderSet* getShaders(ShaderSel shaId) const;
	Camera* getCamera(CameraSel camId) const;
	Texture* getTexture(TexSel texId) const;
	Light* getLight(LightSel lightId) const;
	FloatingObject* getFloater(FloaterSel floatId) const;
	MeshVBO* getMesh(MeshSel meshId) const;
	Transformation3D* getTransform(TransformSel transId) const;

	inline void applyModelViewMatrix() const;
	inline void applyProjectionMatrix() const;
	inline void applyModelViewProjectionMatrix() const;
	inline void applyModelViewMatrix(GLfloat* modelView) const;
	inline void applyProjectionMatrix(GLfloat* projection) const;
	inline void applyModelViewProjectionMatrix(GLfloat* mvp) const;
	inline GLint getUniformLocation(const char* uniformVariable) const;
	inline mat4 getProjectionMatrix();
	inline mat4 getModelViewMatrix();

	std::string curLabel;
	void startTimer(std::string label);
	void endTimer();

	void cleanup();
};

// --- inline function definition ---
mat4 GraphicsContext::getModelViewMatrix(){
	return curCamera->getModelViewMatrix();
}

mat4 GraphicsContext::getProjectionMatrix(){
	return curCamera->getProjectionMatrix();
}

void GraphicsContext::applyModelViewMatrix( GLfloat* modelView ) const
{
	curShader->applyModelViewMatrix(modelView);
}

void GraphicsContext::applyModelViewMatrix() const
{
	curShader->applyModelViewMatrix((GLfloat*)curCamera->getModelViewMatrixPointer());
}

void GraphicsContext::applyProjectionMatrix( GLfloat* projection ) const
{
	curShader->applyProjectionMatrix(projection);
}

void GraphicsContext::applyProjectionMatrix() const
{
	curShader->applyProjectionMatrix((GLfloat*)curCamera->getProjectionMatrixPointer());
}

void GraphicsContext::applyModelViewProjectionMatrix( GLfloat* mvp ) const
{
	curShader->applyModelViewProjectionMatrix(mvp);
}


// NOTE: since it is inline function (1 copying)
void GraphicsContext::applyModelViewProjectionMatrix() const
{
	mat4 MVP = curCamera->getProjectionMatrix() * curCamera->getModelViewMatrix();
	curShader->applyModelViewProjectionMatrix(&MVP[0][0]);
	//glUniformMatrix4fv(getUniformLocation("u_MVP"), 1, GL_FALSE, &MVP[0][0]);	
}

GLint GraphicsContext::getUniformLocation( const char* uniformVariable ) const
{
	GLint location = glGetUniformLocation(curShader->programId, uniformVariable);
	#ifdef PRINT_WARNINGS_UNIFORM_LOCATION
	if(location == -1){
		printf("Warning: GraphicContext.h uniform location not found (program id = %d, variable = %s)\n", curShader->programId, uniformVariable);
	}
	#endif
	return location;
}

#endif