#include "ObjectToWaterCoupling.h"
#include "utils.h"

#include "GraphicsContext.h"
#include "Texture2D.h"
#include "Model3D.h"
#include "ResourceList.h"
#include "FloatingObject.h"

typedef GraphicsContext GC;


void ObjectToWaterCoupling::init(GraphicsContext& gc)
{
	if (isInitialized) return;
	isInitialized = true;

	waveDepthReduce = gc.simConf->waveDepthReduce;

	glGenFramebuffers(1, &FBOobjectSilhouette);
	assert(FBOobjectSilhouette > 0);
	glBindFramebuffer(GL_FRAMEBUFFER, FBOobjectSilhouette);
	//glFramebufferTexture2D will be set in draw

	//FloatingObject* floater = gc.floater[FloaterSel::FLO_SHIP_1];
	glGenRenderbuffers(1, &depthSil);
	glBindRenderbuffer(GL_RENDERBUFFER, depthSil);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, floater->texObjectSilhouette.width, floater->texObjectSilhouette.height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthSil);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}

void ObjectToWaterCoupling::update(GraphicsContext& gc)
{

}


/*
* 
* Requires resources: 
*/
void ObjectToWaterCoupling::display(GraphicsContext& gc)
{
	oglCheckError("O2W: before display");

	if (gc.curShader != gc.getShaders(GC::SHA_OBJECT_PARTICLE_GEN)){
		gc.curShader = gc.getShaders(GC::SHA_OBJECT_PARTICLE_GEN); // ShaderSel::SHA_OBJECT_PARTICLE_GEN
		glUseProgram(gc.curShader->programId);
	}
	
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (FloatingObject* floater : gc.curResources->floaters){
		pyramidTechnique(gc, floater);
	}
	gc.faceCulling ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
}


void ObjectToWaterCoupling::pyramidTechnique(GraphicsContext& gc, FloatingObject* floater){
	static GLenum colorAttachment0[1] = {GL_COLOR_ATTACHMENT0};

	gc.curCamera = floater->silhouetteCamera; // CameraSel::CAM_OBJECT_SILHOUETTE
	gc.curCamera->applyViewport();

	// --- 1. Step ---
	glUniform1i(gc.getUniformLocation("u_Step"), 1);
	// Low resolution silhouette of the object from ortho top view
	// Individual color channels can be found in shader file
	Model3D* convexObj = floater->objConvex;

	gc.applyModelViewMatrix();
	gc.applyProjectionMatrix();

	// screen space: z coordinate (transformed z=0)  would be enough
	vec4 waterMVP = gc.getProjectionMatrix() * gc.getModelViewMatrix() * vec4(gc.simConf->waterOrigin, 1);
	glUniform4fv(gc.getUniformLocation("u_WaterPosScreen"), 1, &waterMVP.x);

	oglCheckError("O2W: before frame buff init");
	glBindFramebuffer(GL_FRAMEBUFFER, FBOobjectSilhouette); // it is faster to bind one FBO and change the texture than changing FBO
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texObjectSilhouette.texID, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, depthSil);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, floater->texObjectSilhouette.width, floater->texObjectSilhouette.height);
	
	glDrawBuffers(1, colorAttachment0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	convexObj->display(gc);
	oglCheckError("O2W: after step 1 disp");

	// --- 2. Step ---
	// Draw each face as a point, writing the wave effect of the face
	// Separe channels (viz shader) to distinquish direct and undirect wave effect
	glUniform1i(gc.getUniformLocation("u_Step"), 2);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texWaveEffect.texID, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND); // blend individual points
	glDisable(GL_DEPTH_TEST);
	glPointSize(1); // Points represent Faces (one per pixels so no information duplicity occur)

	floater->texObjectSilhouette.bind(2);
	glUniform1i(gc.getUniformLocation("u_SilhouetteTexture"), floater->texObjectSilhouette.textureUnit);
	mat4 rotVelocity;
	rotVelocity = glm::rotate(rotVelocity, -floater->rotation.z, Z_AXIS);
	// TODO: informace rovne x+ a dozadu x- staci. (Rotuju to zpet: vec(1,0) -> front -> vec(1,0))
	vec4 dir = rotVelocity * vec4(floater->direction, 1.f);
	//vec4 dir = vec4(floater->direction, 1.f);
	//vec4 dir = vec4(1, 0, 0, 1);

	glUniform1f(gc.getUniformLocation("u_ObjSpeed"), floater->speed);
	glUniform3fv(gc.getUniformLocation("u_ObjDirection"), 1, &dir.x);
	glUniform2fv(gc.getUniformLocation("u_WaveDepthReduce"), 1, &waveDepthReduce.x);
	
	convexObj->meshCentroids->draw_nonIndexed(GL_POINTS);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	// --- 3. Step ---
	glUniform1i(gc.getUniformLocation("u_Step"), 3);
	floater->texWaveEffect.bind(3);
	glUniform1i(gc.getUniformLocation("u_WaveEffectTexture"), floater->texWaveEffect.textureUnit);

	static vec2 invTexSize = 1.0f / (glm::vec2)gc.curCamera->winDim;
	glUniform2fv(gc.getUniformLocation("u_InvTexSize"), 1, &invTexSize.x);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texSilhouetteEdge.texID, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set quad to the camera
	mat4 modelView = gc.getModelViewMatrix();
	modelView = glm::translate(modelView, vec3(convexObj->position.x, convexObj->position.y, 0)); // translate to current camera position
	modelView = glm::scale(modelView, glm::vec3(gc.curCamera->ortho.viewMax.x / 2.f, gc.curCamera->ortho.viewMax.y / 2.f, 1.0f));
	gc.applyModelViewMatrix(&modelView[0][0]);

	static MeshVBO* texPlane = gc.getMesh(GC::MES_PLANE);
	texPlane->display(GL_QUADS);

	// --- 4. step ---
	glUniform1i(gc.getUniformLocation("u_Step"), 4);

	ivec2 baseLevelSize = ivec2(gc.curCamera->winDim.x, gc.curCamera->winDim.y);
	// max mipmap level (dependend on the base texture size)
	int nLevels = (int)round(log2(baseLevelSize.x));

	for (int srcLevel = 0; srcLevel < nLevels; ++srcLevel)
	{ // level = 1 : we are not drawing to mipmap level 0
		glUniform1i(gc.getUniformLocation("u_SourceMipmapLevel"), srcLevel);
		int dstLevel = srcLevel + 1;
		int dstLevelSize = static_cast<int>(pow(2, nLevels - dstLevel));
		int srcLevelSize = dstLevelSize * 2;

		// --- copy stage ---
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texSilhouetteEdge.texID, srcLevel);
		// bind the destination texture
		floater->texTemp.bind(4);
		// copy from FBO to the bound texture
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, srcLevelSize, srcLevelSize);

		// --- RTT stage ---
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texSilhouetteEdge.texID, dstLevel);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUniform1i(gc.getUniformLocation("u_SilhouetteEdgeTex"), floater->texTemp.textureUnit);
		glViewport(0, 0, dstLevelSize, dstLevelSize);

		vec2 invTexSizeMipmap = 1.0f / vec2(srcLevelSize, srcLevelSize);
		glUniform2fv(gc.getUniformLocation("u_InvTexSize"), 1, &invTexSizeMipmap.x);

		texPlane->display(GL_QUADS);
	}

	// --- 5. step ---
	glUniform1i(gc.getUniformLocation("u_Step"), 5);
	glUniform1i(gc.getUniformLocation("u_nMipmapLevels"), nLevels);

	floater->texSilhouetteEdge.bind(5);
	glUniform1i(gc.getUniformLocation("u_SilhouetteEdgeTex"), floater->texSilhouetteEdge.textureUnit);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->texFinalWaveParticle.texID, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, baseLevelSize.x, baseLevelSize.y);

	texPlane->display(GL_QUADS);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void ObjectToWaterCoupling::winResize( GraphicsContext& gc, int width, int height )
{

}

void ObjectToWaterCoupling::preprocess(GraphicsContext& gc)
{

}

