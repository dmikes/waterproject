#include "WaveParticleSimulation.h"

#include "Texture2D.h"
#include "Model3D.h"
#include "FloatingObject.h"
#include "ResourceList.h"

typedef GraphicsContext GC;

WaveParticleSimulation::WaveParticleSimulation() : curVBO(0), curTFB(1), stopSim(false), nSimSteps(0)
{

}


WaveParticleSimulation::~WaveParticleSimulation(void) {
	glDeleteBuffers(1, &newParticleVBO);
	glDeleteBuffers(1, &particleGpuVBO);
	glDeleteVertexArrays(1, &newParticleVAO);
	glDeleteVertexArrays(1, &particleGpuVAO);
}


void WaveParticleSimulation::init(GraphicsContext& gc)
{
	if(isInitialized) return;
	isInitialized = true;

	startupSpeed		= gc.simConf->mWaveParticle.speed;
	startupHalfSize		= gc.simConf->mWaveParticle.particleWidth;
	thresholdDisp		= startupHalfSize;
	minDisperAngle		= gc.simConf->mWaveParticle.minDispersionAngle;
	waveDampening		= gc.simConf->mWaveParticle.waveDampening;
	nMaxParticleBound	= gc.simConf->mWaveParticle.nMaxParticles;

	useBoundaries = gc.simConf->useBoundaries;
	
	nVertices = 0;


	glGenVertexArrays(1, &newParticleVAO); 
	glGenBuffers(1, &newParticleVBO);
	prepareVBO(newParticleVAO, newParticleVBO);
	
	// Buffers for texture to particle generation
	glGenVertexArrays(1, &particleGpuVAO);
	glGenBuffers(1, &particleGpuVBO);
	
	glBindVertexArray(particleGpuVAO);
	glBindBuffer(GL_ARRAY_BUFFER, particleGpuVBO);
	glEnableVertexAttribArray(WPV_LOC_POSITION_ORIGIN);
	glVertexAttribPointer(WPV_LOC_POSITION_ORIGIN, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glBindVertexArray(0);
	oglCheckError("WPsim: before floater");

	for (FloatingObject* floater : gc.curResources->floaters){
		glGenVertexArrays(2, floater->particleVAOs);
		glGenBuffers(2, floater->particleVBOs);
		glGenTransformFeedbacks(2, floater->particleTFBs);

		glGenQueries(1, &floater->queryTF);

		for (unsigned int i = 0; i < 2; i++)
		{
			glBindVertexArray(floater->particleVAOs[i]);
			prepareVBO(floater->particleVAOs[i], floater->particleVBOs[i]);

			// Buffer size: Three times more than maximum!
			glBufferData(GL_ARRAY_BUFFER, nMaxParticleBound * sizeof(WaveParticle), NULL, GL_STATIC_DRAW);

			glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, floater->particleTFBs[i]);
			glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, floater->particleVBOs[i]);
			glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, floater->particleVBOs[i], 0, nMaxParticleBound * sizeof(WaveParticle));

		}
	}
	oglCheckError("WPsim: after floater");


	// Frame buffer init
	if(particleRender_FBO) glDeleteFramebuffers(1, &particleRender_FBO);
	
	glGenFramebuffers(1, &particleRender_FBO);
	assert(particleRender_FBO > 0);
		
	oglCheckError("WPsim: before ordering VBO ");
	// point order (so different sized textures (square and power of two) can use one buffer)
	// 0  1  4   9
	// 2  3  5  10
	// 6  7  8  11
	// 12 13 14 15
	int maxW = 512; // maximal width (square textures)
	std::vector<glm::vec4> vertices;
	vertices.resize(maxW*maxW);
	for (int i = 0; i < maxW; i++){
		for (int j = 0; j < maxW; j++){
			int maxx = glm::max(i, j);
			int minn = glm::min(i, j);
			int index = pow(maxx, 2); // base
			if (maxx == i){
				index += maxx;
			}
			index += minn;
			//---
			//printf("%d [%d %d]\n", index, i, j);
			vertices[index] = vec4(static_cast<float>(i), static_cast<float>(j), 0.f, 0.f);
		}
	}
	glBindVertexArray(particleGpuVAO);
	glBindBuffer(GL_ARRAY_BUFFER, particleGpuVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec4), &vertices[0][0], GL_STATIC_DRAW);
}

void WaveParticleSimulation::prepareVBO(GLuint VAO, GLuint VBO){
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	int offset = 0;
	int size;
	size = 4;
	glEnableVertexAttribArray(WPV_LOC_POSITION_ORIGIN);
	glVertexAttribPointer(WPV_LOC_POSITION_ORIGIN, size, GL_FLOAT, GL_FALSE, sizeof(WaveParticle), BUFFER_OFFSET(0));
	offset += size;

	size = 4;
	glEnableVertexAttribArray(WPV_LOC_AMP_PROPAG_DISP);
	glVertexAttribPointer(WPV_LOC_AMP_PROPAG_DISP, size, GL_FLOAT, GL_FALSE, sizeof(WaveParticle), BUFFER_OFFSET(sizeof(GLfloat)*offset));
	offset += size;
}

void WaveParticleSimulation::display(GraphicsContext& gc){
	oglCheckError("WPS: before display");
	
	glDisable(GL_CULL_FACE); // particles represented as quads
	
	if (gc.curShader != gc.getShaders(GC::SHA_RENDER_PARTICLES)){
		gc.curShader = gc.shaderSets[GC::SHA_RENDER_PARTICLES];
		glUseProgram(gc.curShader->programId);
	}

	if(gc.curResources->inputCamera == gc.getCamera(GC::CAM_PERSP)){
		for (FloatingObject* floater : gc.curResources->floaters){
			displayToCam(gc, floater);
		}
	} else {
		// Is wave particle simulation
		glBindFramebuffer(GL_FRAMEBUFFER, particleRender_FBO);
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);

		for (FloatingObject* floater : gc.curResources->floaters){
			displayToTexture(gc, floater);
		}

		glDisable(GL_BLEND);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	oglCheckError("WPS: after display");
}


void WaveParticleSimulation::displayToTexture(GraphicsContext&gc, FloatingObject* floater){
	static GLenum RBObuffers[1] = { GL_COLOR_ATTACHMENT0 };
	
	// --- Wave particle render shader ---
	// Drawing wave particles to textures
	// Viewport will be probably the same for each floater but do it anyway
	gc.curCamera = floater->waveParticleCamera; // Switch to Ortho
	glViewport(0, 0, gc.curCamera->winDim.x, gc.curCamera->winDim.y);

	gc.applyProjectionMatrix();
	gc.applyModelViewMatrix();
	
	// !Important. Particle texture of each floater must have same resolution. ( Otherwise other FBO must be bound )
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, floater->particleRender.texID, NULL);
	glDrawBuffers(1, RBObuffers); // 1 = number of render targets (only first one)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gc.getTexture(GC::TEX_PARTICLE_SHAPE)->bind(1);
	glUniform1f(gc.getUniformLocation("u_ParticleHalfSize"), startupHalfSize);
	glUniform1i(gc.getUniformLocation("u_PointTexture"), gc.getTexture(GC::TEX_PARTICLE_SHAPE)->textureUnit);
	
	displayVBO(gc, floater);
}

void WaveParticleSimulation::displayToCam(GraphicsContext& gc, FloatingObject* floater){
	if (gc.displayParticles){
		mat4 modelView = gc.getModelViewMatrix();
		modelView = glm::translate(modelView, glm::vec3(0, 0, 120));
		gc.applyModelViewMatrix(&modelView[0][0]);
		gc.applyProjectionMatrix();

		gc.getTexture(GC::TEX_PARTICLE_SHAPE)->bind(1);
		glUniform1i(gc.getUniformLocation("u_PointTexture"), gc.getTexture(GC::TEX_PARTICLE_SHAPE)->textureUnit);

		displayVBO(gc, floater);
	}
}

void WaveParticleSimulation::displayVBO(GraphicsContext& gc, FloatingObject* floater){
	if (floater->particleCounter > 0){
		glBindVertexArray(floater->particleVAOs[curVBO]);

		glDrawTransformFeedback(GL_POINTS, floater->particleTFBs[curVBO]); // intentionally the second one to update
		glBindVertexArray(0);
	}
	
}


void WaveParticleSimulation::insertSingleWaveParticle(vec2 position){
	WaveParticle particle = WaveParticle();
	particle.setPosition(position);
	particle.setOrigin(position);

	particle.setAmplitude(1.0f);
	particle.setPropagationAngle(0.f);
	particle.setDispersionAngle(2.f * M_PI);
	particle.setSize(startupHalfSize);
	particle.setSpeed(startupSpeed);

	nNewParticles = 1;

	glBindBuffer(GL_ARRAY_BUFFER, newParticleVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(WaveParticle), &particle, GL_STATIC_DRAW);
}

void WaveParticleSimulation::insertWaveParticles(vec2 position, int nParticles){
	WaveParticle particle = WaveParticle();
	particle.setPosition(position);
	particle.setOrigin(position);

	particle.setAmplitude(1.0f);
	particle.setPropagationAngle(0.f);
	particle.setDispersionAngle(2.f * M_PI);
	particle.setSize(startupHalfSize);
	particle.setSpeed(startupSpeed);

	nNewParticles = nParticles;

	std::vector<WaveParticle> wps;
	wps.resize(nParticles);
	for (WaveParticle& wp : wps){
		wp = particle;
		wp.setPosition(vec2(rand() % 2000, rand() % 2000));
	}

	glBindBuffer(GL_ARRAY_BUFFER, newParticleVBO);
	glBufferData(GL_ARRAY_BUFFER, nParticles*sizeof(WaveParticle), &wps[0], GL_STATIC_DRAW);
}


void WaveParticleSimulation::clearParticles(GraphicsContext& gc){
	gc.activeFloater->isFirstIteration = true; // This alone works for cleaning
};


void WaveParticleSimulation::insertWaveParticlesFromTextureGPU(GraphicsContext& gc, FloatingObject* floater){
	// It is impossible to use another shader program while Transform feedback is turned on
	// This is the reason why different shader mode has to be used.
	glUniform1i(gc.getUniformLocation("u_Mode"), 2);

	// No need for camera setting (no fragment shader)
	Camera* cam = floater->silhouetteCamera; // gc.cameras[CameraSel::CAM_OBJECT_SILHOUETTE];
	Texture* tex = &floater->texFinalWaveParticle;
	vec2 texScale = cam->ortho.viewMax / vec2(tex->width, tex->height);
	vec2 invTexSize = 1.0f / vec2(tex->width, tex->height);

	mat4 model; // particleTex [0,1) to world coordinates
	model = glm::translate(model, floater->position);
	model = glm::rotate(model, floater->rotation.z, Z_AXIS);
	model = glm::translate(model, floater->waveDistance * (cam->getPosition() - floater->position));
	model = glm::scale(model, vec3(floater->waveDistance * texScale, 1));

	mat4 dirRotate = glm::rotate(mat4(), floater->rotation.z, Z_AXIS);
	glUniformMatrix4fv(gc.getUniformLocation("u_DirRotation"), 1, GL_FALSE, &dirRotate[0][0]); // rotate generated angles (instead of rotating objConvex image)
	glUniformMatrix4fv(gc.getUniformLocation("u_ModelMatrix"), 1, GL_FALSE, &model[0][0]);


	tex->bind(1);
	glUniform1i(gc.getUniformLocation("u_Mode"), 2); // Set mode to FromTexture
	glUniform1i(gc.getUniformLocation("u_ParticleTexture"), tex->textureUnit);
	glUniform2fv(gc.getUniformLocation("u_ParticleTexInvSize"), 1, &invTexSize.x);
	glUniform1f(gc.getUniformLocation("u_MinDispersionAngle"), glm::radians(minDisperAngle)); 
	glUniform1f(gc.getUniformLocation("u_SimSpeed"), startupSpeed);
	glUniform1f(gc.getUniformLocation("u_CumulativeMotion"), floater->cumulativeWaveMotion);
	glUniform1f(gc.getUniformLocation("u_ParticleHalfSize"), startupHalfSize);

	// send the whole grid to the gpu and delete empty pixels (empty particles)
	glDrawArrays(GL_POINTS, 0, tex->width * tex->height);

	floater->cumulativeWaveMotion = 0.0f;

	glBindVertexArray(0);
	glUniform1i(gc.getUniformLocation("u_Mode"), 0);
}

void WaveParticleSimulation::swapBuffers(){
	curVBO = curTFB;
    curTFB = (curTFB + 1) & 0x1; // & 0x1 <==> % 2
}


void WaveParticleSimulation::update(GraphicsContext& gc){
	//if(gc.curResources->inputCamera == gc.windowCamera){
	if(gc.curResources->inputCamera == gc.getCamera(GC::CAM_PERSP)){
		// Other cam means that we are not in simulation step but in a display step
		return;
	}

	if (gc.curShader != gc.getShaders(GC::SHA_WAVE_PARTICLE_SIM)){
		gc.curShader = gc.getShaders(GC::SHA_WAVE_PARTICLE_SIM);
		glUseProgram(gc.curShader->programId);
	}
	glUniform1f(gc.getUniformLocation("u_DispersionThreshold"), thresholdDisp);
	glUniform1f(gc.getUniformLocation("u_Dampening"), waveDampening);
	glUniform1f(gc.getUniformLocation("u_SimSpeed"), startupSpeed);
	glUniform1i(gc.getUniformLocation("u_StopSim"), (int)!stopSim);
	glUniform1i(gc.getUniformLocation("u_UseBoundaries"), useBoundaries);

	for (FloatingObject* floater : gc.curResources->floaters){
		updateParticles(gc, floater);
	}

	swapBuffers();
}

void WaveParticleSimulation::updateParticles(GraphicsContext& gc, FloatingObject* floater){
	static vec2 world2texShift = (glm::vec2)gc.simConf->waterOrigin; // will be instanced by floater
	static vec2 world2texScale = 1.0f / (glm::vec2)gc.simConf->waterSize;

	gc.getTexture(GC::TEX_BORDERS)->bind(1);
	// Don't need camera for simulation
	glUniform1i(gc.getUniformLocation("u_CollisionTexture"), gc.getTexture(GC::TEX_BORDERS)->textureUnit);
	glUniform2fv(gc.getUniformLocation("u_CollisionTexInvSize"), 1, &world2texScale.x);
	glUniform2fv(gc.getUniformLocation("u_CollisionTexShift"), 1, &world2texShift.x);

	glUniform1ui(gc.getUniformLocation("u_VertexCount"), particleCounter); // from the previous frame
	glUniform1ui(gc.getUniformLocation("u_MaxParticleBound"), nMaxParticleBound);

	// === Start of the Feedback stuff ===
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, floater->particleTFBs[curTFB]);
	glBeginTransformFeedback(GL_POINTS);

	glBindVertexArray(floater->particleVAOs[curVBO]);
	glEnable(GL_RASTERIZER_DISCARD);

	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, floater->queryTF);

	if (floater->isFirstIteration) {
		glDrawArrays(GL_POINTS, 0, nVertices); // mode, first, count
		floater->isFirstIteration = false;
	} else {
		//glDrawArrays(GL_POINTS, 0, counter);
		glDrawTransformFeedback(GL_POINTS, floater->particleTFBs[curVBO]);
	}

	if (nNewParticles > 0){
		glBindVertexArray(newParticleVAO);
		glDrawArrays(GL_POINTS, 0, nNewParticles);
		nNewParticles = 0;
	}

	if (floater->createParticles){
		glBindVertexArray(particleGpuVAO);
		insertWaveParticlesFromTextureGPU(gc, floater);
		floater->createParticles = false;
		floater->particlesGenerated(gc);
	}

	glBindVertexArray(0);
	//printTransformFeedback(1, 10);

	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);

	//glEndQuery(GL_PRIMITIVES_GENERATED);
	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
	glGetQueryObjectiv(floater->queryTF, GL_QUERY_RESULT, &floater->particleCounter);
}

void WaveParticleSimulation::printTransformFeedback(int nParticles, int offset){
	WaveParticle* feedbackBuff = new WaveParticle[nParticles]; // not optimal (used for debugging)

	glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, offset*sizeof(WaveParticle), nParticles*sizeof(WaveParticle), feedbackBuff);
	if(!stopSim){
		for(int i = 0; i < nParticles; ++i){
			WaveParticle wp = feedbackBuff[i];
			glm::vec2 pos  = wp.getPosition();
			glm::vec2 orig = wp.getOrigin();
			printf("Position: %f %f \nOrigin: %f %f\n", pos.x, pos.y, orig.x, orig.y);
			printf("A PA DA: %f %.3f %.3f \n", wp.getAmplitude(), glm::degrees(wp.getPropagationAngle()), glm::degrees(wp.getDispersionAngle()));
			if (i < nParticles - 1) {
				printf("------\n");
			}
		}
		printf("------------------------------------------\n");
	}
	delete[] feedbackBuff;
}

void WaveParticleSimulation::winResize( GraphicsContext& gc, int width, int height )
{

}

void WaveParticleSimulation::preprocess(GraphicsContext& gc)
{

}
