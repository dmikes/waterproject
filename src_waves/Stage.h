#ifndef __SCENE_H__
#define __SCENE_H__



#include <vector>

#include "myGL.h"
#include "utils.h"
#include "glm.h"
#include "Drawable.h"

class Texture;
class Camera;
 
#include "ShaderSet.h"
#include "MeshVBO.h"

#include "GraphicsContext.h"
#include "Light.h"




class Stage {
public:

	struct DrawableContext {
		DrawableContext(Drawable* d, ResourceList* res) : drawable(d), resources(res)
		{ }

		Drawable* drawable;
		ResourceList* resources;
		//uint order;
	};

	// two query buffers: front and back
	
	glm::uvec2 checkQuery;
	//GLuint64 checkTimer = 0;
	GLint64 timer1;
	GLuint64 timer2;
	double checkTotalGPU = 0.0;
	double checkTotalCPU = 0.0;

	uint queryBackBuffer = 0, queryFrontBuffer = 1;
	std::vector<std::string> stageNames;
	std::vector<glm::uvec2> query;
	std::vector<GLuint64> timerFrame;
	std::vector<double> timerTotalGPU;
	std::vector<double> timerTotalCPU;
	int nMeasuredFrames = 0;
	int measureRepeat = 1;

	std::vector<DrawableContext> drawables; 
	Stage();
	
	void addDrawable(std::string stageName, Drawable* d);
	void addDrawable(std::string stageName, Drawable* d, ResourceList* res);
	void winResize(GraphicsContext& gc, int width, int height);
	void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);
	void preprocess(GraphicsContext& gc);

	void measureUpdate(GraphicsContext& gc);
	void measure(GraphicsContext& gc);

	void displayStageChange(GraphicsContext& gc, DrawableContext& dc);
	void prepareFrame(GraphicsContext& gc);
	void swapQueryBuffers();
	void genQueries();

	void writeMeasurementResults(GraphicsContext& gc, std::string folder, std::string customInput);
};

#endif