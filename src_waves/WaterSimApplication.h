#pragma once

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <array>

#include "myGL.h"
#include "glm.h"
#include "GraphicsContext.h"
#include "Input.h"
#include "GUI.h"
#include "Stage.h"

typedef GraphicsContext GC;

class WaterOptics;
class WaterSurface;
class FilterParticles;
class WaveParticleSimulation;
class ObjectToWaterCoupling;
class WaterToObjectCoupling;
class NoiseGenerator;

class WaterSimApplication {
public:
	// Initialized first!
	PathConfig paths = PathConfig("../resources/paths.ini");
	bool quitProgram = false;

	// Default simulation configuration file, ...
	std::string defaultSimFile;
	// Which can be overriden by specified input config (loaded from command line)
	std::string inputConfig; 

	// Input
	Mouse mouse;
	Keyboard keyboard;

	// Stages
	WaterOptics* waterOptics;
	WaterSurface* waterSurface;
	FilterParticles* filterParticles;
	WaveParticleSimulation* waveParticles;
	ObjectToWaterCoupling* object2water;
	WaterToObjectCoupling* water2object;
	NoiseGenerator* perlin;

	// Context which is passsed into single stage
	GraphicsContext gc;
	// Scene "graph"
	Stage stage;
	GUI Gui;

	enum E_Scene{
		SCE_OCEAN = 0,
		SCE_OCEAN_SHIPS = 1,
		SCE_LIGHTHOUSE = 2,
		SCE_HOUSE_POOL = 3,
		SCE_POOL = 4,
		SCE_POOL_OBSTACLE = 5,
		SCE_PARTICLE_TEST = 6,
		SCE_MESH_TEST = 7
	};
	//E_Scene curScene = SCE_OCEAN_SHIPS;
	//E_Scene curScene = SCE_OCEAN;
	E_Scene curScene = SCE_LIGHTHOUSE;
	//E_Scene curScene = SCE_HOUSE_POOL;
	//E_Scene curScene = SCE_POOL_OBSTACLE;
	//E_Scene curScene = SCE_PARTICLE_TEST;


	// For SCE_LIGHTHOUSE == number of ships
	// For SCE_OCEAN_SHIPS == number of ships
	// For SCE_MESH_TEST == model number (different number of faces)
	// For SCE_PARTICLE_TEST == number of particles
	int sceneParam = 0;

	// Call back to compile shaders (passed into GUI or input method)
	void (TW_CALL *guiCompileShaders)(void *clientData);
public:
	WaterSimApplication(int argc, char* argv[]);
	~WaterSimApplication();

	void cleanup();
	void exitSimulation(int errcode);
	void checkRequirements();
	
	void enableState();
	
	void display(GLFWwindow* mainWindow);
	void init();

	void initShaders();
	void initFloaters();
	void initCameras();
	void initTextures();
	void initPrimitives();
	void initModels();
	void InitGUI(GraphicsContext& gc);
	
	void prepareStages();
	void prepareScene();

	void compileShaders();
	void windowSizeChanged(int width, int height);
	void keyboardChanged(int key, int scancode, int action, int mods);
	void mousePositionChanged(int ix, int iy);
	void mouseButtonChanged(int button, int action);
	void setGUICompileShaders(void (TW_CALL *function)(void *clientData));
};


