#include "WaveParticle.h"


glm::vec2 WaveParticle::getPosition() const {
	return vec2(mPos2_Orig_Size.x, mPos2_Orig_Size.y);
}

void WaveParticle::setPosition(glm::vec2 val) {
	mPos2_Orig_Size.x = val.x;
	mPos2_Orig_Size.y = val.y;
}

glm::vec2 WaveParticle::getOrigin() const {
	vec2 position = getPosition();
	float propag = getPropagationAngle();
	vec2 origin = position - ( mPos2_Orig_Size.z * vec2(cos(propag), sin(propag)) );
	return origin;
}

void WaveParticle::setOrigin(glm::vec2 val) {
	mPos2_Orig_Size.z = glm::distance(getPosition(), val);
}

glm::vec4 WaveParticle::getColor() const {
	return vec4(0, 0, 0, 0);
	//return mColor;
}

void WaveParticle::setColor(glm::vec4 val) {
	//mColor = val;
}

float WaveParticle::getAmplitude() const {
	return mAmp_Prop_Disp_Spee.x;
}

void WaveParticle::setAmplitude(float val) {
	mAmp_Prop_Disp_Spee.x = val;
	float magnitude = glm::abs(mAmp_Prop_Disp_Spee.w);
	// We encode amplitude sign as a speed sign (speed sign not used)
	if (mAmp_Prop_Disp_Spee.x >= 0){
		mAmp_Prop_Disp_Spee.w = magnitude;
	} else {
		mAmp_Prop_Disp_Spee.w = -magnitude;
	}
}
float WaveParticle::getPropagationAngle() const {
	return mAmp_Prop_Disp_Spee.y;
}

void WaveParticle::setPropagationAngle(float val) {
	mAmp_Prop_Disp_Spee.y = val;
}
float WaveParticle::getDispersionAngle() const {
	return mAmp_Prop_Disp_Spee.z;
}

void WaveParticle::setDispersionAngle(float val) {
	mAmp_Prop_Disp_Spee.z = val;
}

float WaveParticle::getSize() const {
	return mPos2_Orig_Size.w;
}

void WaveParticle::setSize(float val) {
	if (val > 65536.0f){
		printf("Error: cannot exceed 65536 (in shader specification)\n");
	}
	mPos2_Orig_Size.w = val;
}

float WaveParticle::getSpeed() const {
	return mAmp_Prop_Disp_Spee.w;
}

void WaveParticle::setSpeed(float val){
	// We encode amplitude sign as a speed sign (speed sign not used)
	if (mAmp_Prop_Disp_Spee.x >= 0){
		mAmp_Prop_Disp_Spee.w = val;
	} else {
		mAmp_Prop_Disp_Spee.w = -val;
	}
}

//#define getDeleteFlag(info) info & 1
//#define setDeleteFlag(info) info |= 1
//#define clearDeleteFlag(info) info &= ~1
//
//// number 2 = (1 << 1)
//#define getPositiveFlag(info) info & 2
//#define setPositiveFlag(info) info |= 2
//#define clearPositiveFlag(info) info &= ~2
//
//// Number of Consequtive Frames Behing the Border
//// number 65280 = (255 << 8)
//#define getBorderFrames(info) info & 65280
//#define setBorderFrames(info,val) info |= (val << 8)
//
//#define getBorderFrames(info) info & (65535 << 16)
//#define setBorderFrames(info,val) info |= (val << 16)