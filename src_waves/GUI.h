#ifndef __GUI_H__
#define __GUI_H__

class GraphicsContext;

#include "../libs/AntTweakBar/include/AntTweakBar.h"


class GUI{
	typedef unsigned int uint;
private:
	TwBar* controlBar;
	TwBar* motionBar;

	// Water surface stuff
	float* minTessDistance;
	float* maxTessDistance;
	float* minTessLevel;
	float* maxTessLevel;
	float* noiseSpeed;
	float* noiseStrength;
	float* noiseScale;
	float* texRayDistortion;
	float* horizontalCoef;
	float* verticalCoef;
	float* fresnelR0;
	float* fresnelPow;
	float* sunEffectPow;
	float* sunEffectStrength;
	bool* useWaterHole;
	// Wave particles stuff
	bool* useBoundaries;
	float* startupSpeed;
	float* startupHalfSize;
	float* thresholdDisp;
	float* minDisperAngle;
	float* waveDampening;
	// Filtering
	uint* filterKernelSize;
	// Optics
	float* waterClarity;
	float* reflectDarkening;
	// Object to Water
	float* waveDepthReduce;
	float* waveDepthReducePow;
	
	
	void (TW_CALL *compileShaders)(void *clientData);
public:
	static GraphicsContext* gc;
	
	GUI();
	void setShaderCompileFunc(void (TW_CALL *func)(void *clientData));
	

	void init(GraphicsContext& gc);
	void renewGUI(GraphicsContext& gc);
	void addControlVars(GraphicsContext& gc);
	void addMotionVars(GraphicsContext& gc);

	void setMinTessDistance(float* value);
	void setMaxTessDistance(float* value);
	void setMinTessLevel(float* value);
	void setMaxTessLevel(float* value);
	void setNoiseSpeed(float* value);
	void setNoiseStrength(float* value);
	void setNoiseScale(float* value);
	void setTexRayDistortion(float* value);
	void setHorizontalCoef(float* value);
	void setVerticalCoef(float* value);
	void setFresnelR0(float* value);
	void setFresnelPow(float* value);
	void setSunEffectPow(float* value);
	void setSunEffectStrength(float* value);
	void setUseWaterHole(bool* value);
	void setUseBoundaries(bool* value);
	void setStartupSpeed(float* value);
	void setStartupHalfSize(float* value);
	void setThresholdDisp(float* value);
	void setMinDisperAngle(float* value);
	void setWaveDampening(float* value);
	void setFilterKernelSize(uint* value);
	void setWaterClarity(float* value);
	void setReflectDarkening(float* value);
	void setWaveDepthReduce(float* value);
	void setWaveDepthReducePow(float* value);
};

#endif