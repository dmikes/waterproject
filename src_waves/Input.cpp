#include "Input.h"
#include "GraphicsContext.h"

#include "WaveParticleSimulation.h"
#include "FloatingObject.h"
#include "GUI.h"


void Mouse::mousePositionChanged(GraphicsContext& gc, int x, int y){
	if (rightClicked){
		gc.windowCamera->rotate(static_cast<float>(x - lastFramePos.x), static_cast<float>(y - lastFramePos.y));
	}
	lastFramePos.x = x;
	lastFramePos.y = y;
}

void Mouse::mouseButtonChanged(int button, int action){
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS){
		rightClicked = true;
	}
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE){
		rightClicked = false;
	}
}


//////////////////////////////////////////////////////////////////////////
void Keyboard::keyboardChanged(GraphicsContext& gc, int key, int scancode, int action, int mods){
	// Cleaner way, there should be another layer which screens out the implementation of each key press
	if (action == GLFW_RELEASE){
		// propagate keypress to antweakbar lib
		TwKeyPressed(key, action);

		switch (key) {
		case GLFW_KEY_O:
			//gc.windowCamera = (gc.windowCamera == &mainCam) ? &particleRenderCam : &mainCam;
			gc.windowCamera = (gc.windowCamera == gc.mainCamera) ? gc.activeFloater->waveParticleCamera : gc.mainCamera;
			break;
		case GLFW_KEY_U:
			gc.displayParticles = !gc.displayParticles;
			break;
		case GLFW_KEY_P: // Pause
			waveParticles->stopSim = !waveParticles->stopSim;
			break;
		case GLFW_KEY_I:
			gc.displayTex = !gc.displayTex;
			break;
		case GLFW_KEY_L: {
			gc.pauseAnimation = !gc.pauseAnimation;
			break;
		}
		case GLFW_KEY_1: {
			waveParticles->insertSingleWaveParticle(vec2(250, 250));
			break;
		}
		case GLFW_KEY_2:{
			gc.activeFloater->createParticles = true;
			break;
		}
		case GLFW_KEY_3:{
			waveParticles->insertWaveParticles(vec2(250, 250), 1000);
			break;
		}
		case GLFW_KEY_N:{
			waveParticles->clearParticles(gc);
			break;
		}
		case GLFW_KEY_G:{ // Gde je model
			printf("model position: %f %f %f\n", vec3Elements(gc.activeFloater->position));
			break;
		}
		case GLFW_KEY_C: { // Camera report
			printf("=== Camera ===\n");
			Camera* cam = gc.windowCamera;
			printf("Position: %f %f %f \n", vec3Elements(cam->position));
			printf("Orientation: %f %f %f \n", vec3Elements(cam->rotation));
			printf("WinDim: %d %d \n", vec2Elements(cam->winDim));
			if (cam->type == Camera::ProjectionType::ORTHOGRAPHIC){
				printf("Ortho: min: %f %f  max: %f %f \n", vec2Elements(cam->ortho.viewMin), vec2Elements(cam->ortho.viewMax));
			}
			break;
		}
		case GLFW_KEY_R: // Ride a boat
			gc.moveShip = !gc.moveShip;
			break;
		case GLFW_KEY_T: // stop Time
			if (mods == GLFW_MOD_SHIFT){
				gc.pauseWorldForces = !gc.pauseWorldForces;
			}
			else {
				gc.pauseForces = !gc.pauseForces;
			}
			break;
		case GLFW_KEY_V:{ // Velocity reset
			gc.activeFloater->velocity = vec3(0, 0, 0);
			break;
		}
		case GLFW_KEY_K:{ // Knoze! :)
			//gc.activeFloater->setPosition(vec3(300, 150, 0));
			gc.activeFloater->setPosition(vec3(86, 86, 0));
			gc.activeFloater->rotation.z = 0;
			break;
		}
		case GLFW_KEY_B:{ // Build
			compileShaders(NULL);
			break;
		}
		case GLFW_KEY_J:{ // Jet
			gc.activeFloater->velocity = vec3();
			gc.activeFloater->rotation.z = 180;
			gc.activeFloater->setPosition(vec3(2140, 1350, 0));
			//gc.activeFloater->moveImpuls = vec3(10e7f, 0, 0);
			break;
		}
		case GLFW_KEY_M: // toggle Motion particle generation
			gc.motionParticles = !gc.motionParticles;
			break;
		case GLFW_KEY_Z:{ // Zmena floateru
			static int curF = 0;
			curF = (curF + 1) % gc.floaters.size();
			//printf("current floater: %d\n", curF);
			gc.activeFloater = gc.floaters[curF];
			if (gc.windowCamera->followee != NULL){
				gc.windowCamera->follow(gc.activeFloater);
			}

			Gui->renewGUI(gc);
			break;
		}
		case GLFW_KEY_F:{ // Follow
			if (gc.windowCamera->followee == NULL){
				gc.windowCamera->follow(gc.activeFloater);
				gc.moveShip = true;
			}
			else {
				gc.windowCamera->follow(NULL);
				gc.windowCamera->rotate(0, 0); // reset camera front
				gc.moveShip = false;
			}
			break;
		}
		case GLFW_KEY_H: {
			gc.pauseDemonstration = !gc.pauseDemonstration;
			gc.frameCount = 0;
			break;
		}
		}
	}
}
void Keyboard::keyboardCheckState(GraphicsContext& gc, GLFWwindow* window, float deltaTime){
	if (gc.moveShip){
		if (glfwGetKey(window, GLFW_KEY_W)){
			gc.activeFloater->move(gc, FloatingObject::FORWARD);
		}
		if (glfwGetKey(window, GLFW_KEY_S)){
			gc.activeFloater->move(gc, FloatingObject::BACK);
		}
		if (glfwGetKey(window, GLFW_KEY_D)){
			gc.activeFloater->move(gc, FloatingObject::RIGHT);
		}
		if (glfwGetKey(window, GLFW_KEY_A)){
			gc.activeFloater->move(gc, FloatingObject::LEFT);
		}
		if (glfwGetKey(window, GLFW_KEY_Q)){
			gc.activeFloater->move(gc, FloatingObject::UP);
		}
		if (glfwGetKey(window, GLFW_KEY_E)){
			gc.activeFloater->move(gc, FloatingObject::DOWN);
		}
	}
	else {
		if (glfwGetKey(window, GLFW_KEY_W)){
			gc.windowCamera->move(Camera::FORWARD);
		}
		if (glfwGetKey(window, GLFW_KEY_S)){
			gc.windowCamera->move(Camera::BACK);
		}
		if (glfwGetKey(window, GLFW_KEY_D)){
			gc.windowCamera->move(Camera::RIGHT);
		}
		if (glfwGetKey(window, GLFW_KEY_A)){
			gc.windowCamera->move(Camera::LEFT);
		}
		if (glfwGetKey(window, GLFW_KEY_Q)){
			gc.windowCamera->move(Camera::UP);
		}
		if (glfwGetKey(window, GLFW_KEY_E)){
			gc.windowCamera->move(Camera::DOWN);
		}
	}
	if (glfwGetKey(window, GLFW_KEY_4)){
		for (int i = 0; i < 500; i++){
			waveParticles->insertSingleWaveParticle(vec2(250 + rand() % 100, 250 + rand() % 100));
		}
	}

}

void Keyboard::setShaderCompileFunc(void (TW_CALL *func)(void *clientData))
{
	compileShaders = func;
}
