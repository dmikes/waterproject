#ifndef __WAVE_PARTICLE_VBO_H__
#define __WAVE_PARTICLE_VBO_H__

#include "myGL.h"
#include "utils.h"
#include "WaveParticle.h"
#include "GraphicsContext.h"
#include "Drawable.h"


#define BUFFER_OFFSET(y) ((char*) NULL + (y))

class WaveParticleSimulation : public Drawable {
private:
	Texture* texInsertParticleGPU;
	bool insertParticlesGPU;

	GLuint nVertices = 0;
	GLuint nMaxParticleBound = 0;
	
	GLuint newParticleVAO;
	GLuint newParticleVBO;
	GLuint particleGpuVAO;
	GLuint particleGpuVBO;
	bool inited = false;

	GLuint particleRender_FBO;
	int curTFB = 0;
	int curVBO = 0;
	uint nNewParticles = 0;
public: 
	// Simulation info
	uint nSimSteps = 0;
	bool stopSim = false;
	bool useBoundaries = false;
	
	GLint particleCounter = 0;
	uint queryTF;
	//GLint counter = 0;
	//uint query;

	float startupSpeed = 0.f;
	float startupHalfSize = 0.f;
	float thresholdDisp = 0.f;
	float minDisperAngle = 0.f;
	float waveDampening = 0.f;
	

	WaveParticleSimulation();
	~WaveParticleSimulation(void);

	void winResize(GraphicsContext& gc, int width, int height);
	virtual void init(GraphicsContext& gc);
	void update(GraphicsContext& gc);
	void display(GraphicsContext& gc);
	void preprocess(GraphicsContext& gc);
	
	void displayVBO(GraphicsContext& gc);
	void displayVBO(GraphicsContext& gc, FloatingObject* floater);
	void swapBuffers();

	void insertSingleWaveParticle(vec2 position);
	void insertWaveParticlesFromTextureGPU(GraphicsContext& gc, FloatingObject* floater);
	void prepareFromTexture();
	void clearParticles(GraphicsContext& gc);
	void clearParticles(GraphicsContext& gc, FloatingObject* floater);
	void printTransformFeedback(int nParticles);
	void printTransformFeedback(int nParticles, int offset);

	void prepareVBO(GLuint VAO, GLuint VBO);
	void displayToTexture(GraphicsContext&gc, FloatingObject* floater);
	void displayToCam(GraphicsContext& gc, FloatingObject* floater);
	void updateParticles(GraphicsContext& gc, FloatingObject* floater);
	void insertWaveParticles(vec2 position, int nParticles);
};

#endif