#ifndef __GLM_REF_H__
#define __GLM_REF_H__

#include "../libs/glm/glm.hpp"
#include "../libs/glm/gtc/matrix_transform.hpp"

const glm::vec3 X_AXIS = glm::vec3(1.0f, 0.0f, 0.0f);
const glm::vec3 Y_AXIS = glm::vec3(0.0f, 1.0f, 0.0f);
const glm::vec3 Z_AXIS = glm::vec3(0.0f, 0.0f, 1.0f);
const glm::vec3 GREY = glm::vec3(0.5f, 0.5f, 0.5f);

const glm::mat4 IDENTITY_4;

typedef glm::vec2 vec2;
typedef glm::vec3 vec3;
typedef glm::vec4 vec4;

typedef glm::ivec2 ivec2;
typedef glm::ivec3 ivec3;
typedef glm::ivec4 ivec4;

typedef glm::mat3 mat3;
typedef glm::mat4 mat4;

#define vec2Elements(vec) vec.x, vec.y
#define vec3Elements(vec) vec.x, vec.y, vec.z
#define vec4Elements(vec) vec.x, vec.y, vec.z, vec.w

#define isNANvec(v) v.x != v.x || v.y != v.y || v.z != v.z

#endif